Source: inhomog
Section: science
Priority: optional
Maintainer: Debian Astro Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Boud Roukema <boud-debian@cosmo.torun.pl>
Build-Depends: debhelper (>= 11),
               libgsl-dev (>= 2.3),
               perl
Standards-Version: 4.3.0
Homepage: https://bitbucket.org/broukema/inhomog
Vcs-Git: https://salsa.debian.org/debian-astro-team/inhomog.git
Vcs-Browser: https://salsa.debian.org/debian-astro-team/inhomog

Package: inhomog
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: kinematical backreaction and average scale factor evolution
 The inhomog library calculates average cosmological expansion.
 This particular package contains an example front end program that uses the
 biscale_partition routines of the inhomog library, illustrating
 effective scale factor evolution in a universe with a T^3 spatial
 section that is divided into two complementary domains. The inhomog
 library provides Raychaudhuri integration of cosmological
 domain-wise average scale factor evolution using an analytical formula for
 kinematical backreaction Q_D evolution. The library routine
 lib/Omega_D_precalc.c is callable by RAMSES using ramses-scalav.
 .
 You may use this front-end program for command-line investigation
 of the role of virialisation as a potential replacement for dark
 energy (see Roukema 2017, arXiv:1706.06179).

Package: libinhomog0
Architecture: any
Section: libs
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: shared library for kin.backreaction/average scale factor
 The inhomog library calculates average cosmological expansion.
 The library provides Raychaudhuri integration of cosmological
 domain-wise average scale factor evolution using an analytical formula for
 kinematical backreaction Q_D evolution. The inhomog main program illustrates
 biscale examples. The library routine lib/Omega_D_precalc.c is callable by
 RAMSES using ramses-scalav (see Roukema 2018 A&A 610, A51, arXiv:1706.06179).
 .
 This package contains inhomog's shared libraries. To compile your
 own programs with inhomog, you need to install libinhomog-dev.

Package: libinhomog-dev
Section: libdevel
Architecture: any
Depends: libinhomog0 (= ${binary:Version}), ${misc:Depends}
Description: static library for kin.backreaction/average scale factor
 The inhomog library calculates average cosmological expansion.
 The library provides Raychaudhuri integration of cosmological
 domain-wise average scale factor evolution using an analytical formula for
 kinematical backreaction Q_D evolution. The inhomog main program illustrates
 biscale examples. The library routine lib/Omega_D_precalc.c is callable by
 RAMSES using ramses-scalav (see Roukema 2018 A&A 610, A51, arXiv:1706.06179).
 .
 This package contains the static libraries and header files that
 you will need if you wish to compile a program that uses inhomog
 as a library.
