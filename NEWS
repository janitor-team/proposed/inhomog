inhomog NEWS                                         -*- outline -*-

* Main changes in releases 0.1.9.2 (2019-01-24) [alpha]

Benchmarks that can be turned on with #define macros have been
added to most of the lib/test_*.c unit tests. Growth functions
have been sped up by factors of about 4, 3, 10 for the 0th, 1st,
2nd derivatives for the non-EdS flat FLRW (e.g. LCDM) case by
using Kasai (2010 arXiv:1012.2671) Eq (5), and the flat FLRW 2nd
scale factor second time derivative function, a_ddot_flat_FLRW, has
been added, with an extension of the appropriate unit test.
Some test unit memory leaks were removed.
Minor: casting safety changes and portable printf formats.
Version 0.1.9.1 passes all unit and integration regression tests.

* Main changes in releases 0.1.8.1 (2018-12-22) [alpha]

The Omega_D_precalc interface has been modified, with backward
compatibility, so calls to the old version should not (yet) fail.
Optionally definable preprocessor macros have been made more
standard (DISABLE_Q, DISABLE_LAMBDA), had added functionality
(PRECALC_PRINT_ALL), or been introduced (INHOM_LCDM, INHOM_EDS,
OMEGA_GLOBAL_NORMALISE, INHOM_TURNAROUND_IS_COLLAPSE). The unit
tests have been strengthened so that NaNs do not lead to
incorrectly accepting erroneous results.


* Main changes in releases 0.1.7 (2017-09-12) [alpha]

This portability fix for (test_|)alloc_big_array.*() should
hopefully fix Debian builds on the powerpc and sh4 architectures.
(Version 0.1.6.1 built correctly for i386.) See
http://tracker.debian.org/inhomog for studying Debian warnings
and errors in the debianised version of inhomog; fixes should
preferably be done upstream at bitbucket. TODO has been updated
to refer to debianisation.

* Main changes in releases 0.1.[56] (2017-09-10) [alpha]

These are the first two versions tested in Debian. Build failures on
i386 and sh4 architectures for v0.1.5 were probably due to passing
preprocessor constants directly to the alloc_big_array_*() routines
without casting them to the required type (int64_t); v0.1.6 should
correct this.

* Main changes in releases 0.1.[34] (2017-08-17) [alpha]

The upstream version is now being prepared for debianisation.  Changes
in these two version include a dpkg-configure .pc.in file and
Makefile.am changes; use of libtool for making both shared and static
libraries; installation of include files in a subdirectory of
include/; removing the ancient (2006) system.h include file of
portability hacks; adding a doc/examples/ demo C program + README.

* Main changes in release 0.1.2 (2017-08-10) [alpha]

Integers input to the Omega_D_precalc interface are now all int64_t,
to make it easier for a calling program with -fdefault-integer-8 to
reduce the chance of occurrence of bugs related to the 2^31 maximum
value of int32_t. A bug in alloc_big_array.c, related to checking
available RAM when the kernel is linux 2.3.48 or later, was fixed, and
Omega_D_precalc now reacts to the alloc_big_array return values
if there is insufficient memory. Some minor changes improve the match
to ISO C standards.

* Main changes in release 0.1.1 (2017-07-30) [alpha]

Allow I, II, III command line inputs in the biscale_partition
which is called by the front end (main program);
add reference to main article, arxiv:1706.06179;
include first draft of doxygen documentation.

* Main changes in release 0.1.0 (2017-06-18) [alpha]

First public release of inhomog package.
