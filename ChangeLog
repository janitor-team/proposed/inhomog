2019-01-24 boud
	* v0.1.9.2 - avoid adding dD_da_func to shared library

	* v0.1.9.1 - configure.ac (C-A).A.R GNU revision numbering update

	* v0.1.9 - many small improvements:

	Benchmarks have been added to most of the lib/test_*.c functions.
	Some options need turning on with #define's since they are only
	for manual testing. The non-EdS growth function slowed down
	LCDM calculations versus EdS by a factor of about 20-30.

	Growth function speedups and adding a_ddot_flat_FLRW()
	to FLRW_background.c:

	- growth_function.c: Introduce Kasai (2010 arXiv:1012.2671) Eq (5)
	to speed up non-EdS flat FLRW growth factor calculations by
	factors of about 4, 3, 10, for the zeroth, first, second
	derivatives, respectively;

	- growth_function.c: Introduce missing return line for EdS case
	for second derivative of the growth function - this should speed
	up the EdS case a little. The old calculation was correct but
	did a lot more work than needed;

	- FLRW_background.c: Add flat FLRW (non-EdS) second derivative of
	scale factor, a_ddot_flat_FLRW.

	- make check: Extended test_growth_function.c, test_scale_factor.c
	for checking second derivatives between EdS and nearly-EdS flat
	FLRW.

	more safety in casting of memory sizes in lib/alloc_big_array.c,
	unsigned 64-bit types in lib/Omega_D_precalc.c,
	integer casting for gsl_monte_vegas in lib/inhomog.h, lib/sigma*.c;

	portable printf formats for uint64_t and size_t;

	slightly safer check for forgotten initialisation in
	lib/power_spectrum_flatspace.c;

        remove some memory leaks in lib/test_gsl_multifit.c,
	lib/test_sigma_invariants.c;

	All seven default (fast) 'make check' tests pass on amd64;
	test_Omega_D should be faster than before; full tests with 'time
	(./configure --enable-devmode && make clean && make -j ${NCPU}
	check)' for NCPU cpus runs in about 65 seconds real time on
	an amd64 intel core i7-4770K with NCPU=8.

2018-12-22 boud
	* v0.1.8.2 - the upstream version should build a static library by
	default; debian will override this to create a shared library by default

	* v0.1.8.1 - return to gcc 6.3.0/8.2.0 preferences for %lu type formats
	in printf statements

	* v0.1.8 - The Omega_D_precalc interface has been modified: (i) to
	require three numbers of elements in the scalar averaging `cube`:
	scal_av_n_gridsize(1|2|3), in order to allow bidomain averaging;
	and (ii) to include aD_norm, the multiplicative normalisation of
	the individual domains, i.e. the cube root of their initial
	volumes (the global normalisation cancels).  The old
	omega_d_precalc_ is retained for backward compatibility, but it is
	deprecated; you should avoid using it. The version info given
	to libtool was updated to 1:0:1, which translates into
	libinhomog0.1.0 according to libtool's numbering system.

	Unit tests: isfinite() tests have been added so that infinities
	and NaNs are treated as errors. This is necessary because
	(fabs(NaN) > error_threshold) gives `false` instead of detecting a
	likely error.

	In lib/scale_factor_D_(Ham|Ray).c - faster Lambda + more standard
	DISABLE_(Q|LAMBDA).  There's no need to recalculate Lambda - a
	constant in LCDM - from the H0^2 normalised version Omega_Lambda0
	- 100000 or so times for every element of the spline. Calculating
	it once remove about a million calculations per call to
	scale_factor_D_(Ham|Ray).  The DISABLE_Q and DISABLE_LAMBDA_IN_ODE
	define options are now shifted to a more standard style: if
	undefined then there's a double negative, as would be expected:
	the Q and Lambda lines are 'not (not (compiled and executed))',
	i.e. they are compiled and executed normally.  This commit yields
	success (pass = 0) for test_scale_factor_D_Ray and
	test_scale_factor_D_Ham.

	In lib/Omega_D_precalc: some changes were made that increase the
	amount of ascii output if PRECALC_PRINT_ALL is defined.

	In lib/Omega_D_precalc.c, optional defines INHOM_LCDM and
	INHOM_EDS were added, and Planck2015 sigma_8 is now used.  The
	Omega_D_precalc function is (initially at least) intended to be
	called by RAMSES via ramses-scalav, but the ramses-scalav routines
	are not normally intended for LCDM, since the whole point of
	scalar averaging is to see if LCDM is emergent from EdS rather
	than put in by hand as a fudge to match observations.  For some
	purposes, checking LCDM consistency may still be useful, which is
	why inhomog in general can handle LCDM. For the RAMSES inhomog
	interaction via Omega_D_precalc, INHOM_LCDM and INHOM_EDS are
	introduced here in order to run a calculation for precalculated
	initial conditions for one (normally not both; INHOM_LCDM_EDS
	gives both) of the models. The default is EdS.  The sigma_8
	normalisation for EdS (shouldn't affect the calculations) is
	corrected to be CMB-normalised; and the LCDM values are updated to
	Planck 2015.

	In lib/Omega_D_precalc.c: outputs - introduce
	OMEGA_GLOBAL_NORMALISE and INHOM_TURNAROUND_IS_COLLAPSE.  It
	generally makes more sense to show Omega values normalised by
	H_D_global, as in Figs 10 & 11 of arXiv:1706.06179. This is set as
	a default now, with OMEGA_GLOBAL_NORMALISE defined to 1.  The
	description of PRECALC_PRINT_ALL is also slightly modified - this
	can be useful for small simulations (or rather, small numbers of
	domains).  Defining the preprocessor macro
	INHOM_TURNAROUND_IS_COLLAPSE is intended to allow easy study of
	what happens prior to turnaround, by pretending
	(from a software point of view) that it's the moment of
	gravitational `collapse`. This macro is by default undefined; and
	is documented in lib/inhomog.h ; and its effect occurs in the
	function find_collapse_time() in lib/scale_factor_D.c .

	In lib/scale_factor_D.c - correct documentation: default is
	Raychaudhuri.  A few small wording changes to the description of
	the scale_factor_D wrapper to the two individual
	integrators. Correction to the description of the default, which
	is presently the Raychaudhuri integrator. The Hamiltonian
	integrator is only used if INHOM_ENABLE_HAMILTON_INTEGRATOR is
	defined, e.g. using -DINHOM_ENABLE_HAMILTON_INTEGRATOR when
	compiling, or by editing the inhomog.h file.

	Bibliometric references are updated: Roukema 2018 A&A 610, A51,
	arXiv:1706.06179 and Sahni & Starobinsky peer-reviewed journal
	reference.

	Omega_D_precalc: backward compatibility with old gsl lacking
	gsl_sort2

2017-09-12 boud
	* v0.1.7.1 - the private routine report_request_too_much_mem
	was incorrectly exported in 0.1.7; a libtool Makefile.am
	option now prevents the export
	* v0.1.7 portability: casting protection (size_t) in
	alloc_big_array.*() for parameters given to malloc();
	test_alloc_big_array: if allocation fails, do not assign to the
	unallocated array.

2017-09-10 boud
	* v0.1.6.1 include the NEWS file update for v0.1.6
	* v0.1.6 test_alloc_big_array, Omega_D_precalc: cast preprocessor
	constants before passing them as arguments to functions;
	library: 0.1.0 because of minor (non-interface) change in 	Omega_D_precalc

2017-08-18 boud
	* v0.1.5 version ready for Debian GNU/Linux distribution

2017-08-17 boud
	* v0.1.4.5 remove inhomog_shared binary from top Makefile.am
	* v0.1.4.4 reorganise minor tests to reduce unnecessary symbols
	in .h files
	* v0.1.4.3 run TESTS_SLOW only with ./configure --enable-devmode
	* v0.1.4.2 remove inclusion of fftw3.h in lib/test_sigma_invariants.c
	* v0.1.4.1 add file "compile"
	* v0.1.4 dpkg-configure .pc.in file and Makefile.am changes;
	preparing for debian version

2017-08-16 boud
	* v0.1.3 libtoolize - both shared and static libraries;
	install include files in subdirectory of include/;
	remove obsolete system.h include file;
	doc/examples/ demo C program + README

2017-08-10 boud
	* v0.1.2 int64_t in Omega_D_precalc input parameters;
	bug fix in alloc_big_array free RAM check for linux;
	Omega_D_precalc exits politely if out of memory.

2017-07-30 boud
	* v0.1.1 main program: allow I II III command line inputs;
	add reference to arxiv:1706.06179; first draft of
	doxygen documentation.

2017-06-18 boud
	* v0.1.0 first public release @bitbucket

2017-06-16 boud
	* v0.0.70 update configure.ac
	* v0.0.69 inhomog.c front end -> biscale front end; man page

2017-06-15 boud
	* v0.0.68 +(test_)biscale_partition

2017-05-25 boud
	* v0.0.67 revert 0.0.66; enable DETECT_SUDDEN_COLLAPSE in find_collapse_time()
	* v0.0.66 Omega_D_precalc: consider !isnormal a_D to be the collapsed case
	* v0.0.65 Omega_D_precalc: print uncollapsed count

2017-05-03 boud
	* v0.0.64 minor: +lib/c_gsl_wrap.c for f90 front ends

2017-03-17 boud
	* v0.0.63 scale_factor_D_(Ray|Ham).c - bug fix: do not leave N_T_SPLINE'th value undefined
	* v0.0.62 scale_factor_D_(Ray|Ham).c - bug fix: DISABLE_Q block for EdS background

2017-02-09 boud
	* v0.0.60 scale_factor_D_Ray.c - remove obsolete DISABLE_Q block
	* v0.0.61 update test_power_spectrum_flatspace.c, test_Omega_D.c to test unnormalised case

2016-12-18 boud
	* v0.0.59 scale_factor_D_(Ray|Ham).c - allow either Q or Lambda to
	be disabled in ODE integration

2016-12-02 boud
	* v0.0.58 Omega_D_example - disable Giga-printing want_verbose option

2016-12-01 boud
	* v0.0.57 Omega_D_example - high mass end - 50-90 Mpc/h

2016-12-01 boud
	* v0.0.56 Omega_D_precalc - tiny corrections - LCDM Planck 2015

2016-11-20 boud
	* v0.0.55 allow -DDISABLE_Q=1 for scale_factor_D_Ray.c
	- Omega_D_precalc - redefine a_D_linear as section-weighted mean

2016-11-14 boud
	* v0.0.54 separate print loop for sqrt(E[sigma(I)])
	* v0.0.53.1  N_INITCOND_KBS 32768
	* v0.0.53 rza_integrand_params -> pointers at all intermediate levels
	* v0.0.52 Omega_D_example - normal chimera-level numbers
	* v0.0.51 experimental - calculate invariants only once per R

2016-11-11 boud
	* v0.0.50 "unphysical" array in case user gives unsafe parameters;
	- Omega_D_precalc seems OK now;
	- _SCALE_FACTOR_ normalisations no longer hardwired
	- some front end programs still need _SCALE_FACTOR_ updating
	- make check -> 8 passes

2016-11-09 boud
	* v0.0.49 *power_spectrum_flatspace* updated, numerically safer
	  + omega_d_precalc_  as routine callable from fortran

2016-11-05 boud
	* v0.0.48 Omega_D_example - solve detached HEAD/branch confusion
	* v0.0.47 Omega_D_example - fix missing [i_R] bug in find_collapse_time call

2016-11-02 boud
	* v0.0.46 Omega_D_example - N_INITCOND_KBS: s/8/512/; print_a_few_omegas = 1

2016-11-01 boud
	* v0.0.45 inhomog.h: revert to standard scale factor normalisation

2016-10-31 boud
	* v0.0.44 modularise collapse detection: find_collapse_time();
	+Omega_D_precalc
	+find_collapse_time in Omega_D_example - bug correction, should
	find more collapses

2016-10-26 boud
	* v0.0.43 Omega_D_example: linear R intervals;
	+structure precalculated_invariants_s

2016-10-25 boud
	* v0.0.42 Omega_D_example: EdS case only;
	- power_spectrum_flatspace.h normalisation 1.0;
	- sigma8 normalisation;
	- clearer naming of init condition strategies

2016-06-05 boud
	* v0.0.41 Omega_D_example: EdS case only; power_spectrum_flatspace.h normalisation 0.5

2016-06-02 boud
	* v0.0.40 Omega_D_example: LCDM case only (N_COSM_(I|1))

2016-02-09 boud
	* v0.0.39 Omega_D_example: i_KBS case 7: linear theory+spherical

2016-01-27 boud
	* v0.0.38 N_RZA2 512, n_calls_invariants = 300000

2015-10-12 boud
	* v0.0.37 Omega_D_example:  N_RZA2 256, n_calls_invariants = 100000; scripts/plot_OmD_small*
	* Omega_D_example: z_c unnormalised, clearer calculation; small N's for small computer

2015-10-03 boud
	* v0.0.36 Omega_D_example: dynamically alloc and free multiD arrays
	* v0.0.35 Omega_D_example:  N_RZA2 256, n_calls_invariants = 100000
	* Omega_D_example: I_KBS_CASE and N_SIGMA_SIGMA as compile level or default defines
	* Omega_D_example - setting up for chimera run (unfinished): modified i_KBS cases
	* lib/inhomog.h #defines for init cond and integration choices; minor cleanups
	* scale_factor_D_Ray: added missing a_initial factor; growth_function.c updates; Omega_D_BAO OK
	* Hamiltonian init conditions; integrator switched to Raychaudhuri; based on 1RJS work session
	* scale_factor_Ham_one_try: restart from beginning when re-estimating t_neg_sqrt
	* test_gsl_multifit for least-squares fitting of some non-linear functions

2015-02-16 boud
	* v0.0.34
	* Omega_D_example minor bug: replace A_D_NULL by SCALE_FACTOR_A_D_NEARLY_ZERO and shift to inhomog.h
	* scale_factor_D: Ham enabled

2015-02-16 boud
	* v0.0.32.1 [31 was committed but reverted]
	* new front ends: Omega_D_BAO, Omega_D_effWEC, Omega_D_example
	* curvature_backreaction( ) written
	* scale_factor_D forks to either scale_factor_Ray or scale_factor_Ham
	* want_planar parameter introduced for all main work functions
	* Omega_D_example illustrates redshift of collapse distributions
	* Omega_D_example: high values of n_calls_invariants, N_RZA2 for supercomputer check

2013-11-11 boud
	* v0.0.30: test_Omega_D: make check - unused test commented out

2013-11-08 boud
	* v0.0.29: test_Omega_D: new RZA2-based check values - make check
	should give pass = 0

2013-11-08 boud
	* v0.0.28: delta_tilde_integrals: DEFN 0 of (C22): one replacement
	s/k_mod_3_sq/k_mod_3_4th/

2013-10-28  boud
	* v0.0.27: oscillatory EisHu98 power_spectrum survives tests;
	its test function should be OK with make check
	* v0.0.27.1: same as 0.0.27 but includes updated configure*

2013-10-20  boud
	* v0.0.26: test_sigma_invariants: better openmp usage (parallel
	for, schedule(dynamic))

2013-10-18  boud
	* v0.0.25: non-oscillatory EisHu98 power_spectrum seems OK

2013-10-17  boud
	* v0.0.24: test_sigma_invariants gsl_rng handling improved
	(RANLUX/ranlxd2; GSL_RNG_SEED read once and further seeds generated
	internally)

2013-10-13  boud
	* v0.0.21: test_sigma_invariants rewritten; III OK.
	* v0.0.22: malloc for possibly big arrays -> avoid seg viol.
	* v0.0.23: test_sigma_invariants anisotropic i,j,k_arrays: III still OK :).

2013-10-12  boud
	* v0.0.20: delta_tilde_interp(): discretise to
	avoid interpolation subtleties in a complex
	discrete Fourier spectrum (series) - linear 3D
	rectangular interpolation loses power

2013-10-10  boud
	* v0.0.18: fftw3 for test_sigma_invariants delta realisations
	* v0.0.19: C9, C20 bug corrections

2013-10-09  boud
	* v0.0.17: valgrind clean of test_sigma_invariants:
	free gsl_rng, fftw3 allocations

2013-10-08  boud
	* v0.0.16: fftw3 for test_sigma_invariants

2013-10-07  boud
	* v0.0.13: version for BKS Appendix C testing: Invs I, II
	* v0.0.14: inhomog.c: corrected header include
	* v0.0.15: sigma_square_invariant_III.c + delta_tilde_integrands.c bug correction in III:
	three dot products had not been squared;
	parameters chosen for 32-core multi-hour calculation

2013-10-04  boud
	* v0.0.12: scale_factor_D, Omega_D: flat Lambda case seems OK

2013-10-02  boud
	* v0.0.11: seven checks now run with make checks

2013-10-01  boud
	* v0.0.10: Omega_D; scale factor normalisations to inhomog.h;
	new normalisation of \xi (avoids dimensional violation);
	kinematic_backreaction.c: bug removed: inv_II wasn't squared in Q_D denominator;
	spherical test RZA2 V.B.3 in kinematic_backreaction;
	all FLRW scale factor functions shifted to FLRW_background;
	rough first draft of spatial Ricci curvature code;
	growth_function;
	unit testing: make check should generate five different test passes.

2013-09-24  boud
	* v0.0.9.2: with tag in git (hopefully)
	* v0.0.9.1: with tag in git (hopefully)
	* v0.0.9: scale_factor_D.c: looks consistent with
	a_D/a for RZA2 Fig 2.left top, bottom curves without
	modifying parameters; values about 10% lower than
	in RZA2 Fig 2 - not yet "precise cosmology" ;)

2013-02-16  boud
	* v0.0.8: kinematic_backreaction.c: rza_Q_D from (49) of
	Buchert et al RZA2 20120722 preprint; testing difficult
	until a_D is programmed;
	* inhomog.h: switching to INHOM_INV_III_DEFN 0 for comparison
	with existing calculations

2013-02-15  boud
	* v0.0.7.1: bug fix: power_spectrum_flatspace.c - static
	should't be used in functions called by competing openmp
	threads

2013-02-15  boud
	* v0.0.7: optional shell window function instead of sphere;
	dependence on astromisc-0.1.18.20 for parse_noempty_strtox

2013-02-14  boud
	* v0.0.6.1: new formula for sigma_sq_invariant_III hidden
	temporarily while analytical calculation is checked

2013-02-09  boud
	* v0.0.6: openmp to parallelise  test_sigma_sq_invariants()
	* v0.0.5: sigma_sq_invariant_III: new analytical formula - choose
	new vs old in inhomog.h

2013-02-05  boud
	* v0.0.4.1: factor of 8 was not validated in 0.0.3, normalisation
	wrt BKS00 Fig 13 now 201^{-3};
	sigma_sq_invariant_II: matches BKS00 Fig 13 main curve;
	sigma_sq_invariant_III: matches BKS00 claim to order of magnitude
	if sign is reversed.

2013-02-05  boud
	* v0.0.3: sigma_sq_invariant_I matches BKS00 Fig 13 main curve
	with 1e-6 renormalisation, calculation t = 5 s; script provided.

2013-02-04  boud
	* v0.0.2: sigma_sq_invariant_I programmed, z=201 vs z=0 issues buggy,
	BKS00 Fig 13 not reproduced; individual tests via -t option.

2012-11-08  boud
	* example of Monte Carlog integration

2012-10-29  Boud Roukema - boud cosmo.torun.pl

	* inhomog: initial version 0.0.1. Buggy.
