BASE=OmD_examp_20151011
EV=evRay
SIG=sig1.0

for R in 0.00746269 0.0160779 0.0346387; do
    OUTFILE=${BASE}_${R}_all_tmp.dat
    rm -f ${OUTFILE}
    touch ${OUTFILE}
    for BKS in D U; do
	case ${BKS} in
	    "D")
		CURVS="1"
		;;
	    *)
		CURVS="0 1"
		;;
	esac
	     
	for CURV in ${CURVS}; do
	    for ((KBS=0; KBS<7; KBS++)); do
		grep "freq.*${R}" ${BASE}_BKS${BKS}_CURV${CURV}_${EV}_i_kbs${KBS}_${SIG}.log | \
		    awk '{print $6,$7}' |sed -e "s/^/${BKS} ${CURV} ${KBS} /" | \
		    sed -e 's/D/1/' |sed -e 's/U/0/' >> ${OUTFILE}
		#echo "" >> ${OUTFILE}
	    done
	done
    done # for BKS
    ls -l ${OUTFILE}
done
