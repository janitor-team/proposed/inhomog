/*
   sigma_square_invariant_III - functions for BKS00 (C20-C22)

   Copyright (C) 2013 Jan Ostrowski, Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html

*/

/*! \file sigma_square_invariant_III.c */

#include <stdio.h>
#include <sys/types.h>
#include <inttypes.h>
#include "config.h"
#include <math.h>
#include <gsl/gsl_rng.h>
/* #include <gsl/gsl_math.h> */
#include <gsl/gsl_monte.h>
#include <gsl/gsl_monte_plain.h>
#include <gsl/gsl_monte_vegas.h>

/* for malloc_usable_size if available */
#ifdef __GNUC__
#include <malloc.h>
#endif

#include "lib/inhomog.h"
#include "lib/power_spectrum_flatspace.h"

#define DEBUG0 1
#define DEBUG1 1
#define K_ARRAY_CAREFUL 1

#undef DEBUG0
#undef DEBUG1
/* #undef K_ARRAY_CAREFUL */


double flat_dot_product_3_6params
( double a1, double a2, double a3,
  double b1, double b2, double b3){
  return (a1*b1 + a2*b2 + a3*b3);
}



double flat_dot_product_3(double *k_array,
                          int j_1,  /* which vector?
                                      j_1 \in 1, 2, 3 */
                          int j_2   /* which vector?
                                      j_2 \in 1, 2, 3 */
                          ){
  int j_1_offset;
  int j_2_offset;
  double dd;

#ifdef K_ARRAY_CAREFUL
#ifdef __GNUC__
  int j_max;
#endif
#endif

  /* GNU extension, not portable:  check correct usage of the
     function
  */
#ifdef K_ARRAY_CAREFUL
#ifdef __GNUC__
  j_max=j_1;
  if(j_2>j_max) j_max=j_2;

  if(malloc_usable_size(k_array) < (unsigned int)(3*j_max) *sizeof(double)){
    printf("flat_dot_product_3: allocation Error. k_array = %p,",
           (void *)k_array);
    printf("  malloc_usable_size, (3*j_max) *sizeof(double) = %zu %zu",
           (size_t)((long)malloc_usable_size(k_array)),
           (size_t)(3*j_max) *sizeof(double));
    exit(1);
  };
#endif
#endif

  /* by how many addresses should we offset a vector start? */
  j_1_offset = 3*(j_1-1);
  j_2_offset = 3*(j_2-1);
  dd = k_array[0+j_1_offset] * k_array[0+j_2_offset]
    +  k_array[1+j_1_offset] * k_array[1+j_2_offset]
    +  k_array[2+j_1_offset] * k_array[2+j_2_offset] ;

  return dd;
}


/* BKS00 (C20-C22) */
/*! \brief Calculates the integrand function for \f$ \sigma_{III}^2 (R)
 * \f$ calculation.
 *
 * Prepares the integrand function by calling \ref window_R_k (or
 * \ref window_R1_R2_k, depending on \a w_type value) and
 * \ref power_spectrum_flatspace functions. For necessary parameters,
 * such as the window function type or the power spectrum calculation
 * method, uses rza_integrand_params_s structure.
 *
 * The equation used may come from (C20)-(C22) in Buchert et al. 2000
 * \latexonly (\href{https://arxiv.org/abs/astro-ph/9912347}
 * {arXiv:astro-ph \textbackslash 9912347v2}) \endlatexonly ; however,
 * this formula is wrong. If \b INHOM_INV_III_DEFN is set to 1, the
 * calculation happens according to [TODO: future paper reference].
 *
 * \param [in] k_array pointer to the array of possible mode values
 * \param [in] dim [TODO]
 * \param [in] params a pointer to the void parameter
 */
double sigma_sq_invariant_III_integrand(double * k_array,
                                        size_t dim,
                                        void * params){

  struct rza_integrand_params_s rza_integrand_params;
  double k_mod_1, k_mod_2, k_mod_3; /* 3D mod, 3D mod, 3D mod,
                                       respectively */
  double k_mod_1_sq, k_mod_2_sq, k_mod_3_sq; /* squares */
  double k_mod_1_4th, k_mod_2_4th; /*, k_mod_3_4th;*/ /* 4th power */
  double k_123_sq, k_123_4th;  /* product of all three, ^2 and ^4 */
  double k_sum[DIM_BKS00];          /* vec(k_1) + vec(k_2) + vec(k_3) */
  double k_sum_mod;
  double dot_12, dot_13, dot_23; /* dot products */

  double window_1, window_sum;
  /*   double R_domain; */

  /* BKS00 (C20)-(C22) */
  double G1_term1, G1_term2, G1;
  double G2_term1, G2_term4, G2_term5, G2;

  /* calculated directly */
  double factor_1, factor_2;

  /* any approach */
  double factor_main;

  double the_integrand;

  const double two_thirds = 2.0/3.0;
  const double one_sixth = 1.0/6.0;

  if(dim!=9){
    printf("sigma_sq_invariant_III_integrand: dim = %d != 9\n",
           (int)dim);
    exit(1);
  };

  k_mod_1 = flat_length_3(k_array);
  k_mod_2 = flat_length_3( &(k_array[3]) );
  k_mod_3 = flat_length_3( &(k_array[6]) );
  k_sum[0]= k_array[0] + k_array[3] + k_array[6];
  k_sum[1]= k_array[1] + k_array[4] + k_array[7];
  k_sum[2]= k_array[2] + k_array[5] + k_array[8];
  k_sum_mod = flat_length_3(k_sum);
#ifdef DEBUG0
  printf("sigma_sq_invariant_I_integrand: k_mod_1 = %g\n",k_mod_1);
#endif

  if(k_mod_1 < TOL_LENGTH_INV_III ||
     k_mod_2 < TOL_LENGTH_INV_III || k_mod_3 < TOL_LENGTH_INV_III ){
    the_integrand = 0.0; /* set to zero if k too close to negative */
  }else{

    rza_integrand_params = *((struct rza_integrand_params_s *)params);

    /* window function: use physical units at initial epoch directly */

    switch(rza_integrand_params.w_type)
      {
      case 1:
      default:
        /* window function: use physical units at initial epoch directly */
        if(0==INHOM_INV_III_DEFN){
          window_1 = window_R_k(rza_integrand_params.R_domain, k_mod_1);
        };
        window_sum = window_R_k(rza_integrand_params.R_domain, k_sum_mod);
        break;

      case 2:
        if(0==INHOM_INV_III_DEFN){
          window_1 = window_R1_R2_k(rza_integrand_params.R_domain_1,
                                    rza_integrand_params.R_domain_2,
                                    k_mod_1);
        };
        window_sum = window_R1_R2_k(rza_integrand_params.R_domain_1,
                                    rza_integrand_params.R_domain_2,
                                    k_sum_mod);
        break;
      }; /* switch(rza_integrand_params.w_type) */


    k_mod_1_sq = k_mod_1*k_mod_1;
    k_mod_2_sq = k_mod_2*k_mod_2;
    k_mod_3_sq = k_mod_3*k_mod_3;

    k_mod_1_4th = k_mod_1_sq*k_mod_1_sq;
    k_mod_2_4th = k_mod_2_sq*k_mod_2_sq;
    /* k_mod_3_4th = k_mod_3_sq*k_mod_3_sq; */

    dot_12 = flat_dot_product_3(k_array, 1, 2);
    dot_13 = flat_dot_product_3(k_array, 1, 3);
    dot_23 = flat_dot_product_3(k_array, 2, 3);

#ifdef DEBUG0
    /*    if(! ( -1e300 < dot_12 && dot_12 < 1e300 ))*/
    {
      printf("sIII debug: dot_12 = %g, k_array = \n",dot_12);
      printf(" %g %g %g\n %g %g %g\n %g %g %g\n",
             k_array[0],k_array[1],k_array[2],
             k_array[3],k_array[4],k_array[5],
             k_array[6],k_array[7],k_array[8]);
    };
#endif

    if(0==INHOM_INV_III_DEFN){
      /*
         This is the peer-reviewed, wrong version of the IIIrd
         invariant formula. arXiv:astro-ph/9912347v2 gives the
         identical, wrong formula.
      */
      /* (C21); small k_mod_1, k_mod_2, k_mod_3 checked above */
      G1_term1 = dot_12 * dot_13 /(k_mod_1_sq * k_mod_2 *k_mod_3);
      G1_term1 = -2.0 *G1_term1 *G1_term1;

      G1_term2 = dot_12 /(k_mod_1 * k_mod_2);
      G1_term2 = 3.25 *G1_term2 *G1_term2;

      G1 = G1_term1 + G1_term2 - 1.25;

      /* (C22); small k_mod_1, k_mod_2, k_mod_3 checked above */
      G2_term1 = dot_12 * dot_13 * dot_23 / (k_mod_1_sq * k_mod_2_sq * k_mod_3_sq);
      G2_term1 = two_thirds *G2_term1 *G2_term1 ;

      G2_term4 = dot_12/(k_mod_1*k_mod_2);
      G2_term4 *= G2_term4;
      G2_term5 = - G2_term4;
      G2_term4 *= G2_term4;
      G2_term4 *= two_thirds;

      G2 = G2_term1
      - 2.0 *dot_12 *dot_12 *dot_12  *dot_13  *dot_23 /
      (k_mod_1_4th *k_mod_2_4th *k_mod_3_sq)
      + two_thirds * dot_12 *dot_13  *dot_23 /
      (k_mod_1_sq *k_mod_2_sq *k_mod_3_sq)
      + G2_term4
      + G2_term5
      + one_sixth;

      factor_main = (window_1 * window_1 * G1 +
       window_sum *window_sum * G2);

    }else if(1==INHOM_INV_III_DEFN){
      /*
         This correct version (1==INHOM_INV_III_DEFN) of the IIIrd
         invariant formula is to appear soon in a peer-reviewed
         publication by authors including Roukema and Ostrowski. It
         can be obtained from BKS00 using (C9), (C10) + octave +
         maxima for the generalisation of (C17).
      */

      k_123_sq = k_mod_1_sq * k_mod_2_sq * k_mod_3_sq;
      k_123_4th = k_123_sq*k_123_sq;

      factor_1 = k_123_sq
        - 3.0 * dot_23 * dot_23 * k_mod_1_sq
        + 2.0 * dot_12 * dot_13 * dot_23;
      factor_2 = k_123_sq
        - dot_12*dot_12 * k_mod_3_sq
        - dot_13*dot_13 * k_mod_2_sq
        - dot_23*dot_23 * k_mod_1_sq
        + 2.0 * dot_12 * dot_13 * dot_23;

      factor_main = one_sixth * factor_1 * factor_2
        * window_sum * window_sum / k_123_4th;
    };


    /* power spectrum: both physical units and initial epoch are needed */
    the_integrand =  power_spectrum_flatspace
      (k_mod_1,
       rza_integrand_params.background_cosm_params.inhomog_a_scale_factor_initial,
       rza_integrand_params.pow_spec_type,
       rza_integrand_params.background_cosm_params
       ) *
      power_spectrum_flatspace
      (k_mod_2,
       rza_integrand_params.background_cosm_params.inhomog_a_scale_factor_initial,
       rza_integrand_params.pow_spec_type,
       rza_integrand_params.background_cosm_params
       ) *
      power_spectrum_flatspace
      (k_mod_3,
       rza_integrand_params.background_cosm_params.inhomog_a_scale_factor_initial,
       rza_integrand_params.pow_spec_type,
       rza_integrand_params.background_cosm_params
       ) *
      factor_main;

#ifdef DEBUG0
  printf("sigma_sq_invariant_I_integrand: window, the_integrand = %g\n",
         window,the_integrand);
#endif

  };

  return the_integrand;
}

/*! \brief Calculates \f$ \sigma_{III}^2 (R) \f$  and its error.
 *
 * Uses Monte Carlo alogrithm through GSL libraries to find the value of
 * the first invariant together with its error. It's possible to use
 * 'plain' version of the algorithm, but the VEGAS method is used by
 * default to reduce errors.
 *
 * If \a want_verbose is specified, prints out the domain radius R
 * together with the results (integral value and its error).
 *
 * \param [in] rza_integrand_params_s structure containing parameters
 * necessary for the ODE integration
 * \param [in] n_calls number of times to evaluate the function
 * \param [in] want_verbose control parameter; defined in
 * biscale_partition.c
 * \param [out] the_integral pointer to the result
 * \param [out] integ_error pointer to the result error
 */
int sigma_sq_invariant_III(/* INPUTS; */
                           struct rza_integrand_params_s rza_integrand_params,
                           long   n_calls,  /* number of times to evaluate the function */
                           int want_verbose,
                           double *the_integral, /* OUTPUTS: */
                           double *integ_error
                           )
{
  const gsl_rng_type * T_gsl;
  gsl_rng * r_gsl;

  static unsigned long int local_gsl_seed=0;

  /*  gsl_monte_plain_state * working_space; */
  gsl_monte_vegas_state * working_space;
#define INHOMOG_VEGAS_SMALL_III ((size_t)100000)
#define INHOMOG_TRY_VEGAS_MAX 10
  int i_try_vegas=0;

  /* a meta-function as per  gsl_monte_function_struct  in gsl_monte.h */
  gsl_monte_function our_meta_function;

  double x_lower[3*DIM_BKS00];
  double x_upper[3*DIM_BKS00];
  int i;

  /* definition used in BKS00 because of error in (C20)--(C22) */
  if(2==INHOM_INV_III_DEFN){
    sigma_sq_invariant_I(rza_integrand_params, (int)(n_calls/20),
                         want_verbose,
                         the_integral,
                         integ_error );
    *the_integral = exp(3.0*log(*the_integral))/27.0;
    /* *integ_error  not modified */
    return 0;
  };


  gsl_rng_env_setup();
  T_gsl = gsl_rng_default;
  gsl_rng_default_seed += 4003 + local_gsl_seed;
  local_gsl_seed = gsl_rng_default_seed;
  r_gsl = gsl_rng_alloc (T_gsl); /* this gets reallocated each time the
                                    function gets called */

  /* meta set up our function (defined in detail above) */
  our_meta_function.f = &sigma_sq_invariant_III_integrand;
  our_meta_function.dim = 3*DIM_BKS00;
  our_meta_function.params = &rza_integrand_params;

  /* gsl_monte_plain_state * gsl_monte_plain_alloc (size_t DIM) */
  /*  working_space =  gsl_monte_plain_alloc (DIM_BKS00); */
  working_space =  gsl_monte_vegas_alloc (3*DIM_BKS00);

  /*  int gsl_monte_plain_integrate (gsl_monte_function * F,
      const double XL[], const double XU[], size_t DIM, size_t
      CALLS, gsl_rng * R, gsl_monte_plain_state * S, double *
      RESULT, double * ABSERR);

      VEGAS is a more intelligent algorithm.
  */

  /* define the cartesian region of integration */
  for (i=0; (unsigned)i<3*DIM_BKS00; i++){
    x_lower[i] = K_LOWER_BKS00;
    x_upper[i] = K_UPPER_BKS00;
  };

  /*
  gsl_monte_plain_integrate ( &our_meta_function,
                              x_lower,
                              x_upper,
                              (3*DIM_BKS00),
                              n_calls,
                              r_gsl,
                              working_space,
                              the_integral,
                              integ_error
                              );
  */

  /* warm up: */
  gsl_monte_vegas_integrate ( &our_meta_function,
                              x_lower,
                              x_upper,
                              (3*DIM_BKS00),
                              INHOMOG_VEGAS_SMALL_III,
                              r_gsl,
                              working_space,
                              the_integral,
                              integ_error
                              );

  /* try 5-tuple repetitions at least a third of the maximum n tries,
     and keep going if the accuracy is still low, but not infinitely */
  while(i_try_vegas < INHOMOG_TRY_VEGAS_MAX &&
        (i_try_vegas < INHOMOG_TRY_VEGAS_MAX/3 ||
         fabs(gsl_monte_vegas_chisq(working_space) -1.0) > 0.5) ) {
    gsl_monte_vegas_integrate ( &our_meta_function,
                                x_lower,
                                x_upper,
                                (3*DIM_BKS00),
                                (size_t)((long)(n_calls/5)),
                                r_gsl,
                                working_space,
                                the_integral,  /* already a pointer */
                                integ_error    /* already a pointer */
                                );
    i_try_vegas++;  /* don't risk an unending bad run at VEGAS */
    /* printf("debug vegas: i_try_VEGAS = %d\n",i_try_vegas); */
  };

  *the_integral *= 64.0 *exp(6.0*log(M_PI));
  *integ_error *= 64.0 *exp(6.0*log(M_PI));

  if(want_verbose){
    printf("sigma_square_invariant_I: R_domain");
      switch(rza_integrand_params.w_type)
      {
      case 1:
      default:
        printf("= %g, the_integral = %g, err = %g\n",
               rza_integrand_params.R_domain, *the_integral, *integ_error);
        break;

      case 2:
        printf("= %g, the_integral = %g, err = %g\n",
               rza_integrand_params.R_domain_1, *the_integral, *integ_error);
        break;
      };
  };

  gsl_rng_free(r_gsl);
  /*  gsl_monte_plain_free (working_space); */
  gsl_monte_vegas_free (working_space);

  return 0;
}
