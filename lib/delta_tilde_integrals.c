/* 
   delta_tilde_integrals - interpolations and integrands for BKS 2000 App C

   Copyright (C) 2013 Boud Roukema, Jan Ostrowski

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

   See also http://www.gnu.org/licenses/gpl.html
   
*/

#include <stdio.h>
#include <sys/types.h>
#include "config.h"
#include <math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_monte.h>
#include <gsl/gsl_monte_plain.h>
#include <gsl/gsl_monte_vegas.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_complex_math.h>

#include "lib/inhomog.h"
#include "lib/power_spectrum_flatspace.h"
#include "lib/test_sigma_invariants.h"




/* 
   integral of (C7) squared, i.e. for independent k_1, k_2
*/
double C7_sq_integral(struct delta_tilde_struct_s *delta_tilde_struct){

  double kmod_1,kmod_2; /* moduli of k vectors */
  double W1,W2;
  const double R_domain = TEST_INVAR_R_domain;

  gsl_complex delta_tilde_1, delta_tilde_2;
  gsl_complex the_integral;

  const int N_k = TEST_INVAR_N_k;
  int i_1=0, j_1=0, k_1;
  int i_2=0, j_2=0, k_2;

  the_integral = gsl_complex_rect(0.0,0.0);

#ifndef TEST_INVAR_TEST1D
  for(i_1=0; i_1<N_k; i_1++){
    for(j_1=0; j_1<N_k; j_1++){
#endif
      for(k_1=0; k_1<N_k; k_1++){
#ifndef TEST_INVAR_TEST1D
        for(i_2=0; i_2<N_k; i_2++){
          for(j_2=0; j_2<N_k; j_2++){
#endif
            for(k_2=0; k_2<N_k; k_2++){
    
#ifndef TEST_INVAR_TEST1D
              kmod_1 = flat_length_3_3params
                ((delta_tilde_struct->i_array)[i_1],
                 (delta_tilde_struct->j_array)[j_1],
                 (delta_tilde_struct->k_array)[k_1]);
              kmod_2 = flat_length_3_3params
                ((delta_tilde_struct->i_array)[i_2],
                 (delta_tilde_struct->j_array)[j_2],
                 (delta_tilde_struct->k_array)[k_2]);
#else
              kmod_1 = fabs((delta_tilde_struct->k_array)[k_1]);
              kmod_2 = fabs((delta_tilde_struct->k_array)[k_2]);
#endif

              delta_tilde_1 = 
                (delta_tilde_struct->delta_tilde)
                [ index3D(i_1,j_1,k_1, N_k) ];
              delta_tilde_2 = 
                (delta_tilde_struct->delta_tilde)
                [ index3D(i_2,j_2,k_2, N_k) ];

              W1 = window_R_k(R_domain, kmod_1);
              W2 = window_R_k(R_domain, kmod_2);
              /* W1= 1.0; W2 = 1.0; */

              the_integral = gsl_complex_add
                (the_integral,
                 (gsl_complex_mul_real
                  (gsl_complex_mul(delta_tilde_1, 
                                   delta_tilde_2),
                   (W1*W2))
                  )
                 );
            };
#ifndef TEST_INVAR_TEST1D
          };
        };
#endif
      };
#ifndef TEST_INVAR_TEST1D
    };
  };
#endif
  return gsl_complex_abs(the_integral);
}



/* 
   integral of (C14) 
*/
double C14_integral(struct delta_tilde_struct_s * delta_tilde_struct){

  double kmod_1; /* moduli of k vectors */
  double W1;
  const double R_domain = TEST_INVAR_R_domain;

  double the_integral;

  const int N_k = TEST_INVAR_N_k;
  int i_1=0, j_1=0, k_1;

  the_integral = 0.0;

#ifndef TEST_INVAR_TEST1D
  for(i_1=0; i_1<N_k; i_1++){
    for(j_1=0; j_1<N_k; j_1++){
#endif
      for(k_1=0; k_1<N_k; k_1++){ 
#ifndef TEST_INVAR_TEST1D
        kmod_1 = flat_length_3_3params
          (delta_tilde_struct->i_array[i_1],
           delta_tilde_struct->j_array[j_1],
           delta_tilde_struct->k_array[k_1]);
#else
        kmod_1 = fabs(delta_tilde_struct->k_array[k_1]);
#endif

        W1 = window_R_k(R_domain, kmod_1);
        /* W1 = 1.0; */

        the_integral += power_spec3D( kmod_1 )
          *(W1*W1);
      };
#ifndef TEST_INVAR_TEST1D
    };
  };
#endif
  return the_integral;
}



/* 
   integral of (C8) squared, i.e. for independent k_1, k_2, k_3, k_4
*/
double C8_sq_integral(struct delta_tilde_struct_s *delta_tilde_struct){

  double kmod_1,kmod_2; /* moduli of k vectors */
  double kmod_3,kmod_4; /* moduli of k vectors */
  double dot_12; /* dot product of k_1 and k_2 vectors */
  double dot_34; /* dot product of k_3 and k_4 vectors */
  double sum_k12[3]; /* sums of vectors */
  double sum_k34[3];

  double W1,W2;

  const double R_domain = TEST_INVAR_R_domain;

  gsl_complex delta_tilde_1, delta_tilde_2;
  gsl_complex delta_tilde_3, delta_tilde_4;

  gsl_complex delta_tilde_factor;
  double      real_factor;

  gsl_complex the_integral;

  const int N_k = TEST_INVAR_N_k;
  int i_1, j_1, k_1;
  int i_2, j_2, k_2;
  int i_3, j_3, k_3;
  int i_4, j_4, k_4;

  the_integral = gsl_complex_rect(0.0,0.0);

  for(i_1=0; i_1<N_k; i_1++){
    for(j_1=0; j_1<N_k; j_1++){
      for(k_1=0; k_1<N_k; k_1++){

        for(i_2=0; i_2<N_k; i_2++){
          for(j_2=0; j_2<N_k; j_2++){
            for(k_2=0; k_2<N_k; k_2++){
    
              for(i_3=0; i_3<N_k; i_3++){
                for(j_3=0; j_3<N_k; j_3++){
                  for(k_3=0; k_3<N_k; k_3++){

                    for(i_4=0; i_4<N_k; i_4++){
                      for(j_4=0; j_4<N_k; j_4++){
                        for(k_4=0; k_4<N_k; k_4++){
    
                          delta_tilde_1 = 
                            (delta_tilde_struct->delta_tilde)
                            [ index3D(i_1,j_1,k_1, N_k) ];
                          delta_tilde_2 = 
                            (delta_tilde_struct->delta_tilde)
                            [ index3D(i_2,j_2,k_2, N_k) ];
                          delta_tilde_3 = 
                            (delta_tilde_struct->delta_tilde)
                            [ index3D(i_3,j_3,k_3, N_k) ];
                          delta_tilde_4 = 
                            (delta_tilde_struct->delta_tilde)
                            [ index3D(i_4,j_4,k_4, N_k) ];

                          sum_k12[0] = 
                            (delta_tilde_struct->i_array)[i_1] +
                            (delta_tilde_struct->i_array)[i_2];
                          sum_k12[1] = 
                            (delta_tilde_struct->j_array)[j_1] +
                            (delta_tilde_struct->j_array)[j_2];
                          sum_k12[2] = 
                            (delta_tilde_struct->k_array)[k_1] +
                            (delta_tilde_struct->k_array)[k_2];
                                                          
                          sum_k34[0] = 
                            (delta_tilde_struct->i_array)[i_3] +
                            (delta_tilde_struct->i_array)[i_4];
                          sum_k34[1] = 
                            (delta_tilde_struct->j_array)[j_3] +
                            (delta_tilde_struct->j_array)[j_4];
                          sum_k34[2] = 
                            (delta_tilde_struct->k_array)[k_3] +
                            (delta_tilde_struct->k_array)[k_4];
                                                          
                          W1 = window_R_k(R_domain, 
                                          flat_length_3(sum_k12) );
                          W2 = window_R_k(R_domain, 
                                          flat_length_3(sum_k34) );

                          /* W1= 1.0; W2 = 1.0; */
                                                          
                          kmod_1 = flat_length_3_3params
                            ((delta_tilde_struct->i_array)[i_1],
                             (delta_tilde_struct->j_array)[j_1],
                             (delta_tilde_struct->k_array)[k_1]);
                          kmod_2 = flat_length_3_3params
                            ((delta_tilde_struct->i_array)[i_2],
                             (delta_tilde_struct->j_array)[j_2],
                             (delta_tilde_struct->k_array)[k_2]);

                          kmod_3 = flat_length_3_3params
                            ((delta_tilde_struct->i_array)[i_3],
                             (delta_tilde_struct->j_array)[j_3],
                             (delta_tilde_struct->k_array)[k_3]);
                          kmod_4 = flat_length_3_3params
                            ((delta_tilde_struct->i_array)[i_4],
                             (delta_tilde_struct->j_array)[j_4],
                             (delta_tilde_struct->k_array)[k_4]);

                          dot_12 = flat_dot_product_3_6params
                            ((delta_tilde_struct->i_array)[i_1],
                             (delta_tilde_struct->j_array)[j_1],
                             (delta_tilde_struct->k_array)[k_1],
                             (delta_tilde_struct->i_array)[i_2],
                             (delta_tilde_struct->j_array)[j_2],
                             (delta_tilde_struct->k_array)[k_2]);
                          dot_34 = flat_dot_product_3_6params
                            ((delta_tilde_struct->i_array)[i_3],
                             (delta_tilde_struct->j_array)[j_3],
                             (delta_tilde_struct->k_array)[k_3],
                             (delta_tilde_struct->i_array)[i_4],
                             (delta_tilde_struct->j_array)[j_4],
                             (delta_tilde_struct->k_array)[k_4]);
                          
                            
                          delta_tilde_factor =
                            gsl_complex_mul
                            (gsl_complex_mul
                             (gsl_complex_mul
                              (delta_tilde_1, delta_tilde_2),
                              delta_tilde_3),
                             delta_tilde_4);
                          
                          real_factor = 
                            W1*W2 * 
                            (1 - dot_12*dot_12/
                             (kmod_1*kmod_1 * kmod_2*kmod_2)) *
                            (1 - dot_34*dot_34/
                             (kmod_3*kmod_3 * kmod_4*kmod_4));    

                          the_integral = 
                            gsl_complex_add
                            (the_integral,
                             gsl_complex_mul_real
                             ( delta_tilde_factor, 
                               real_factor ));
                        };
                      };
                    }; /* for(i_4=0; i_4<N_k; i_4++) */
                  };
                };
              }; /* for(i_3=0; i_3<N_k; i_3++) */
            };
          };
        }; /* for(i_2=0; i_2<N_k; i_2++) */
      };
    };
  };  /* for(i_1=0; i_1<N_k; i_1++) */

  return gsl_complex_abs(the_integral);
}



/* 
   integral of (C18) 
*/
double C18_integral(struct delta_tilde_struct_s * delta_tilde_struct){

  double kmod_1; /* moduli of k vectors */
  double kmod_2; /* moduli of k vectors */
  double W1;
  const double R_domain = TEST_INVAR_R_domain;

  double dot_12; /* dot product of k_1 and k_2 vectors */
  double sum_k12[3]; /* sums of vectors */

  double dot_12_sq, last_factor;

  double the_integral;

  const int N_k = TEST_INVAR_N_k;
  int i_1, j_1, k_1;
  int i_2, j_2, k_2;

  the_integral = 0.0;

  for(i_1=0; i_1<N_k; i_1++){
    for(j_1=0; j_1<N_k; j_1++){
      for(k_1=0; k_1<N_k; k_1++){ 

        for(i_2=0; i_2<N_k; i_2++){
          for(j_2=0; j_2<N_k; j_2++){
            for(k_2=0; k_2<N_k; k_2++){ 

              sum_k12[0] = 
                (delta_tilde_struct->i_array)[i_1] +
                (delta_tilde_struct->i_array)[i_2];
              sum_k12[1] = 
                (delta_tilde_struct->j_array)[j_1] +
                (delta_tilde_struct->j_array)[j_2];
              sum_k12[2] = 
                (delta_tilde_struct->k_array)[k_1] +
                (delta_tilde_struct->k_array)[k_2];
                                                          
              W1 = window_R_k(R_domain, flat_length_3(sum_k12));
              /* W1 = 1.0; */

              kmod_1 = flat_length_3_3params
                (delta_tilde_struct->i_array[i_1],
                 delta_tilde_struct->j_array[j_1],
                 delta_tilde_struct->k_array[k_1]);
              kmod_2 = flat_length_3_3params
                (delta_tilde_struct->i_array[i_2],
                 delta_tilde_struct->j_array[j_2],
                 delta_tilde_struct->k_array[k_2]);

              dot_12 = flat_dot_product_3_6params
                ((delta_tilde_struct->i_array)[i_1],
                 (delta_tilde_struct->j_array)[j_1],
                 (delta_tilde_struct->k_array)[k_1],
                 (delta_tilde_struct->i_array)[i_2],
                 (delta_tilde_struct->j_array)[j_2],
                 (delta_tilde_struct->k_array)[k_2]);
              dot_12_sq = dot_12*dot_12;
              last_factor = 1.0 - dot_12_sq / 
                (kmod_1*kmod_1 *kmod_2*kmod_2);
              last_factor = last_factor *last_factor;

              the_integral += 
                power_spec3D( kmod_1 ) *
                power_spec3D( kmod_2 ) *
                W1*W1 *last_factor;
            };
          };
        }; /* for(i_2=0; i_2<N_k; i_2++) */
      };
    };
  }; /* for(i_1=0; i_1<N_k; i_1++) */
  return the_integral;
}




/* 
   integrand of (C9)+(C10) squared, i.e. for independent k_1, k_2, ... , k_6
*/
double C9_sq_integral(struct delta_tilde_struct_s *delta_tilde_struct){

  double kmod_1,kmod_2; /* moduli of k vectors */
  double kmod_3,kmod_4; /* moduli of k vectors */
  double kmod_5,kmod_6; /* moduli of k vectors */
  double dot_12; /* dot product of k_1 and k_2 vectors */
  double dot_23, dot_13;
  double dot_45; /* dot product of k_3 and k_4 vectors */
  double dot_46, dot_56;
  double sum_k123[3]; /* sums of vectors */
  double sum_k456[3];

  double W1,W2;
  const double R_domain = TEST_INVAR_R_domain;

  gsl_complex delta_tilde_1, delta_tilde_2, delta_tilde_3;
  gsl_complex delta_tilde_4, delta_tilde_5, delta_tilde_6;

  gsl_complex delta_tilde_factor;
  double      F_0, F_1, F_2, F_123, F_456; /* terms in (C10) */
  double      real_factor;

  gsl_complex the_integral;

  const int N_k = TEST_INVAR_N_k;
  int i_1, j_1, k_1;
  int i_2, j_2, k_2;
  int i_3, j_3, k_3;
  int i_4, j_4, k_4;
  int i_5, j_5, k_5;
  int i_6, j_6, k_6;

  the_integral = gsl_complex_rect(0.0,0.0);

  for(i_1=0; i_1<N_k; i_1++){
    for(j_1=0; j_1<N_k; j_1++){
      for(k_1=0; k_1<N_k; k_1++){

        for(i_2=0; i_2<N_k; i_2++){
          for(j_2=0; j_2<N_k; j_2++){
            for(k_2=0; k_2<N_k; k_2++){
    
              for(i_3=0; i_3<N_k; i_3++){
                for(j_3=0; j_3<N_k; j_3++){
                  for(k_3=0; k_3<N_k; k_3++){

                    for(i_4=0; i_4<N_k; i_4++){
                      for(j_4=0; j_4<N_k; j_4++){
                        for(k_4=0; k_4<N_k; k_4++){
    
                          for(i_5=0; i_5<N_k; i_5++){
                            for(j_5=0; j_5<N_k; j_5++){
                              for(k_5=0; k_5<N_k; k_5++){
    
                                for(i_6=0; i_6<N_k; i_6++){
                                  for(j_6=0; j_6<N_k; j_6++){
                                    for(k_6=0; k_6<N_k; k_6++){
    
                                      delta_tilde_1 = 
                                        (delta_tilde_struct->delta_tilde)
                                        [ index3D(i_1,j_1,k_1, N_k) ];
                                      delta_tilde_2 = 
                                        (delta_tilde_struct->delta_tilde)
                                        [ index3D(i_2,j_2,k_2, N_k) ];
                                      delta_tilde_3 = 
                                        (delta_tilde_struct->delta_tilde)
                                        [ index3D(i_3,j_3,k_3, N_k) ];
                                      delta_tilde_4 = 
                                        (delta_tilde_struct->delta_tilde)
                                        [ index3D(i_4,j_4,k_4, N_k) ];
                                      delta_tilde_5 = 
                                        (delta_tilde_struct->delta_tilde)
                                        [ index3D(i_5,j_5,k_5, N_k) ];
                                      delta_tilde_6 = 
                                        (delta_tilde_struct->delta_tilde)
                                        [ index3D(i_6,j_6,k_6, N_k) ];

                                      sum_k123[0] = 
                                        (delta_tilde_struct->i_array)[i_1] +
                                        (delta_tilde_struct->i_array)[i_2] +
                                        (delta_tilde_struct->i_array)[i_3];
                                      sum_k123[1] = 
                                        (delta_tilde_struct->j_array)[j_1] +
                                        (delta_tilde_struct->j_array)[j_2] +
                                        (delta_tilde_struct->j_array)[j_3];
                                      sum_k123[2] = 
                                        (delta_tilde_struct->k_array)[k_1] +
                                        (delta_tilde_struct->k_array)[k_2] +
                                        (delta_tilde_struct->k_array)[k_3];

                                      sum_k456[0] = 
                                        (delta_tilde_struct->i_array)[i_4] +
                                        (delta_tilde_struct->i_array)[i_5] +
                                        (delta_tilde_struct->i_array)[i_6];
                                      sum_k456[1] = 
                                        (delta_tilde_struct->j_array)[j_4] +
                                        (delta_tilde_struct->j_array)[j_5] +
                                        (delta_tilde_struct->j_array)[j_6];
                                      sum_k456[2] = 
                                        (delta_tilde_struct->k_array)[k_4] +
                                        (delta_tilde_struct->k_array)[k_5] +
                                        (delta_tilde_struct->k_array)[k_6];
                                                          
                                                          
                                      W1 = window_R_k(R_domain, 
                                                      flat_length_3(sum_k123) );
                                      W2 = window_R_k(R_domain, 
                                                      flat_length_3(sum_k456) );

                                      /* W1= 1.0; W2 = 1.0; */
                                                          
                                      kmod_1 = flat_length_3_3params
                                        ((delta_tilde_struct->i_array)[i_1],
                                         (delta_tilde_struct->j_array)[j_1],
                                         (delta_tilde_struct->k_array)[k_1]);
                                      kmod_2 = flat_length_3_3params
                                        ((delta_tilde_struct->i_array)[i_2],
                                         (delta_tilde_struct->j_array)[j_2],
                                         (delta_tilde_struct->k_array)[k_2]);

                                      kmod_3 = flat_length_3_3params
                                        ((delta_tilde_struct->i_array)[i_3],
                                         (delta_tilde_struct->j_array)[j_3],
                                         (delta_tilde_struct->k_array)[k_3]);
                                      kmod_4 = flat_length_3_3params
                                        ((delta_tilde_struct->i_array)[i_4],
                                         (delta_tilde_struct->j_array)[j_4],
                                         (delta_tilde_struct->k_array)[k_4]);

                                      kmod_5 = flat_length_3_3params
                                        ((delta_tilde_struct->i_array)[i_5],
                                         (delta_tilde_struct->j_array)[j_5],
                                         (delta_tilde_struct->k_array)[k_5]);
                                      kmod_6 = flat_length_3_3params
                                        ((delta_tilde_struct->i_array)[i_6],
                                         (delta_tilde_struct->j_array)[j_6],
                                         (delta_tilde_struct->k_array)[k_6]);

                                      dot_12 = flat_dot_product_3_6params
                                        ((delta_tilde_struct->i_array)[i_1],
                                         (delta_tilde_struct->j_array)[j_1],
                                         (delta_tilde_struct->k_array)[k_1],
                                         (delta_tilde_struct->i_array)[i_2],
                                         (delta_tilde_struct->j_array)[j_2],
                                         (delta_tilde_struct->k_array)[k_2]);
                                      dot_13 = flat_dot_product_3_6params
                                        ((delta_tilde_struct->i_array)[i_1],
                                         (delta_tilde_struct->j_array)[j_1],
                                         (delta_tilde_struct->k_array)[k_1],
                                         (delta_tilde_struct->i_array)[i_3],
                                         (delta_tilde_struct->j_array)[j_3],
                                         (delta_tilde_struct->k_array)[k_3]);
                                      dot_23 = flat_dot_product_3_6params
                                        ((delta_tilde_struct->i_array)[i_2],
                                         (delta_tilde_struct->j_array)[j_2],
                                         (delta_tilde_struct->k_array)[k_2],
                                         (delta_tilde_struct->i_array)[i_3],
                                         (delta_tilde_struct->j_array)[j_3],
                                         (delta_tilde_struct->k_array)[k_3]);

                                      dot_45 = flat_dot_product_3_6params
                                        ((delta_tilde_struct->i_array)[i_4],
                                         (delta_tilde_struct->j_array)[j_4],
                                         (delta_tilde_struct->k_array)[k_4],
                                         (delta_tilde_struct->i_array)[i_5],
                                         (delta_tilde_struct->j_array)[j_5],
                                         (delta_tilde_struct->k_array)[k_5]);
                                      dot_46 = flat_dot_product_3_6params
                                        ((delta_tilde_struct->i_array)[i_4],
                                         (delta_tilde_struct->j_array)[j_4],
                                         (delta_tilde_struct->k_array)[k_4],
                                         (delta_tilde_struct->i_array)[i_6],
                                         (delta_tilde_struct->j_array)[j_6],
                                         (delta_tilde_struct->k_array)[k_6]);
                                      dot_56 = flat_dot_product_3_6params
                                        ((delta_tilde_struct->i_array)[i_5],
                                         (delta_tilde_struct->j_array)[j_5],
                                         (delta_tilde_struct->k_array)[k_5],
                                         (delta_tilde_struct->i_array)[i_6],
                                         (delta_tilde_struct->j_array)[j_6],
                                         (delta_tilde_struct->k_array)[k_6]);
                          
                            
                                      delta_tilde_factor =
                                        gsl_complex_mul
                                        (gsl_complex_mul
                                         (gsl_complex_mul
                                          (gsl_complex_mul
                                           (gsl_complex_mul
                                            (delta_tilde_1, delta_tilde_2),
                                            delta_tilde_3),
                                           delta_tilde_4),
                                          delta_tilde_5),
                                         delta_tilde_6);

                                      F_0 = 1.0/6.0;
                                      F_1 = -0.5 * dot_23*dot_23/
                                        (kmod_2*kmod_2 *kmod_3*kmod_3);
                                      F_2 = (1.0/3.0)* 
                                        dot_12* dot_13* dot_23/
                                        (kmod_1*kmod_1* kmod_2*kmod_2* 
                                         kmod_3*kmod_3);
                                      F_123 = F_0 + F_1 + F_2;

                                      F_0 = 1.0/6.0;
                                      F_1 = -0.5 * dot_56*dot_56/
                                        (kmod_5*kmod_5 *kmod_6*kmod_6);
                                      F_2 = (1.0/3.0)* 
                                        dot_45* dot_46* dot_56/
                                        (kmod_4*kmod_4* kmod_5*kmod_5* 
                                         kmod_6*kmod_6);
                                      F_456 = F_0 + F_1 + F_2;
                          
                                      real_factor = 
                                        W1*W2 * F_123 * F_456;
  

                                      the_integral = 
                                        gsl_complex_add
                                        (the_integral,
                                         gsl_complex_mul_real
                                         ( delta_tilde_factor, 
                                           real_factor ));
                                    };
                                  };
                                }; /* for(i_6=0; i_6<N_k; i_6++) */
                              };
                            };
                          }; /* for(i_5=0; i_5<N_k; i_5++) */
                        };
                      };
                    }; /* for(i_4=0; i_4<N_k; i_4++) */
                  };
                };
              }; /* for(i_3=0; i_3<N_k; i_3++) */
            };
          };
        }; /* for(i_2=0; i_2<N_k; i_2++) */
      };
    };
  };  /* for(i_1=0; i_1<N_k; i_1++) */

  return gsl_complex_abs(the_integral);
}



/* 
   integral of (C20) 
*/
double C20_integral(struct delta_tilde_struct_s * delta_tilde_struct){

  double k_mod_1; /* moduli of k vectors */
  double k_mod_2; /* moduli of k vectors */
  double k_mod_3; /* moduli of k vectors */
  double k_mod_1_sq, k_mod_2_sq, k_mod_3_sq; /* squares */
  double k_mod_1_4th, k_mod_2_4th, k_mod_3_4th; /* 4th power */
  double k_123_sq, k_123_4th;  /* product of all three, ^2 and ^4 */

  double W1, W123;
  const double R_domain = TEST_INVAR_R_domain;

  double dot_12; /* dot product of k_1 and k_2 vectors */
  double dot_13, dot_23;
  double sum_k123[3]; /* sums of vectors */
  double k_sum_mod;

  /* BKS00 (C20)-(C22) */
  double G1_term1, G1_term2, G1;
  double G2_term1, G2_term4, G2_term5, G2;

  /* calculated directly */
  double factor_1, factor_2;

  /* any approach */
  double factor_main;
  const double two_thirds = 2.0/3.0;
  const double one_sixth = 1.0/6.0;


  double the_integral;

  const int N_k = TEST_INVAR_N_k;
  int i_1, j_1, k_1;
  int i_2, j_2, k_2;
  int i_3, j_3, k_3;

  the_integral = 0.0;

  for(i_1=0; i_1<N_k; i_1++){
    for(j_1=0; j_1<N_k; j_1++){
      for(k_1=0; k_1<N_k; k_1++){ 

        for(i_2=0; i_2<N_k; i_2++){
          for(j_2=0; j_2<N_k; j_2++){
            for(k_2=0; k_2<N_k; k_2++){ 

              for(i_3=0; i_3<N_k; i_3++){
                for(j_3=0; j_3<N_k; j_3++){
                  for(k_3=0; k_3<N_k; k_3++){ 

                    k_mod_1 = flat_length_3_3params
                      (delta_tilde_struct->i_array[i_1],
                       delta_tilde_struct->j_array[j_1],
                       delta_tilde_struct->k_array[k_1]);
                    k_mod_2 = flat_length_3_3params
                      (delta_tilde_struct->i_array[i_2],
                       delta_tilde_struct->j_array[j_2],
                       delta_tilde_struct->k_array[k_2]);
                    k_mod_3 = flat_length_3_3params
                      (delta_tilde_struct->i_array[i_3],
                       delta_tilde_struct->j_array[j_3],
                       delta_tilde_struct->k_array[k_3]);


                    sum_k123[0] = 
                      (delta_tilde_struct->i_array)[i_1] +
                      (delta_tilde_struct->i_array)[i_2] +
                      (delta_tilde_struct->i_array)[i_3];
                    sum_k123[1] = 
                      (delta_tilde_struct->j_array)[j_1] +
                      (delta_tilde_struct->j_array)[j_2] +
                      (delta_tilde_struct->j_array)[j_3];
                    sum_k123[2] = 
                      (delta_tilde_struct->k_array)[k_1] +
                      (delta_tilde_struct->k_array)[k_2] +
                      (delta_tilde_struct->k_array)[k_3];
                    k_sum_mod= flat_length_3(sum_k123);

                    if(0==INHOM_INV_III_DEFN){
                      W1 = window_R_k(R_domain, k_mod_1 );
                    }else{
                      W1 = 0.0;
                    };
                    W123 = window_R_k(R_domain, k_sum_mod );

                    k_mod_1_sq = k_mod_1*k_mod_1;
                    k_mod_2_sq = k_mod_2*k_mod_2;
                    k_mod_3_sq = k_mod_3*k_mod_3;

                    k_mod_1_4th = k_mod_1_sq*k_mod_1_sq;
                    k_mod_2_4th = k_mod_2_sq*k_mod_2_sq;
                    k_mod_3_4th = k_mod_3_sq*k_mod_3_sq;
                                                  

                    dot_12 = flat_dot_product_3_6params
                      ((delta_tilde_struct->i_array)[i_1],
                       (delta_tilde_struct->j_array)[j_1],
                       (delta_tilde_struct->k_array)[k_1],
                       (delta_tilde_struct->i_array)[i_2],
                       (delta_tilde_struct->j_array)[j_2],
                       (delta_tilde_struct->k_array)[k_2]);
                    dot_13 = flat_dot_product_3_6params
                      ((delta_tilde_struct->i_array)[i_1],
                       (delta_tilde_struct->j_array)[j_1],
                       (delta_tilde_struct->k_array)[k_1],
                       (delta_tilde_struct->i_array)[i_3],
                       (delta_tilde_struct->j_array)[j_3],
                       (delta_tilde_struct->k_array)[k_3]);
                    dot_23 = flat_dot_product_3_6params
                      ((delta_tilde_struct->i_array)[i_2],
                       (delta_tilde_struct->j_array)[j_2],
                       (delta_tilde_struct->k_array)[k_2],
                       (delta_tilde_struct->i_array)[i_3],
                       (delta_tilde_struct->j_array)[j_3],
                       (delta_tilde_struct->k_array)[k_3]);

                    if(0==INHOM_INV_III_DEFN){
                      /* 
                         This is the peer-reviewed, wrong version of the IIIrd
                         invariant formula. arXiv:astro-ph/9912347v2 gives the
                         identical, wrong formula.
                      */
                      /* (C21); small k_mod_1, k_mod_2, k_mod_3 checked above */
                      G1_term1 = dot_12 * dot_13 /(k_mod_1_sq * k_mod_2 *k_mod_3);
                      G1_term1 = -2.0 *G1_term1 *G1_term1;
    
                      G1_term2 = dot_12 /(k_mod_1 * k_mod_2);
                      G1_term2 = 3.25 *G1_term2 *G1_term2;
    
                      G1 = G1_term1 + G1_term2 - 1.25;
    
                      /* (C22); small k_mod_1, k_mod_2, k_mod_3 checked above */
                      G2_term1 = dot_12 * dot_13 * dot_23 / (k_mod_1_sq * k_mod_2_sq * k_mod_3_sq);
                      G2_term1 = two_thirds *G2_term1 *G2_term1 ;
      
                      G2_term4 = dot_12/(k_mod_1*k_mod_2);
                      G2_term4 *= G2_term4;
                      G2_term5 = - G2_term4;
                      G2_term4 *= G2_term4;
                      G2_term4 *= two_thirds;
      
                      G2 = G2_term1  
                        - 2.0 *dot_12 *dot_12 *dot_12  *dot_13  *dot_23 /
                        (k_mod_1_4th *k_mod_2_4th *k_mod_3_4th)
                        + two_thirds * dot_12 *dot_13  *dot_23 /
                        (k_mod_1_sq *k_mod_2_sq *k_mod_3_sq) 
                        + G2_term4
                        + G2_term5
                        + one_sixth;

                      factor_main = (W1*W1 * G1 +
                                     W123*W123 * G2);  

                    }else if(1==INHOM_INV_III_DEFN){
                      /*!
                        This correct version (1==INHOM_INV_III_DEFN) of the IIIrd
                        invariant formula is to appear soon in a peer-reviewed
                        publication by authors including Roukema and Ostrowski. It
                        can be obtained from BKS00 using (C9), (C10) + octave +
                        maxima for the generalisation of (C17).
                      */

                      k_123_sq = k_mod_1_sq * k_mod_2_sq * k_mod_3_sq;
                      k_123_4th = k_123_sq*k_123_sq;

                      factor_1 = k_123_sq 
                        - 3.0 * dot_23 *dot_23 * k_mod_1_sq 
                        + 2.0 * dot_12 * dot_13 * dot_23;
                      factor_2 = k_123_sq 
                        - dot_12*dot_12 * k_mod_3_sq 
                        - dot_13*dot_13 * k_mod_2_sq 
                        - dot_23*dot_23 * k_mod_1_sq 
                        + 2.0 * dot_12 * dot_13 * dot_23;

                      factor_main = one_sixth * factor_1 * factor_2 
                        * W123*W123 / k_123_4th;
                    };


                    the_integral += 
                      power_spec3D( k_mod_1 ) *
                      power_spec3D( k_mod_2 ) *
                      power_spec3D( k_mod_3 ) *
                      factor_main;
                  };
                };
              }; /* for(i_3=0; i_3<N_k; i_3++) */
            };
          };
        }; /* for(i_2=0; i_2<N_k; i_2++) */
      };
    };
  }; /* for(i_1=0; i_1<N_k; i_1++) */
  return the_integral;
}

