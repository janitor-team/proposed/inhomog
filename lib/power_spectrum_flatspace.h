/*
   power_spectrum_flatspace header file - power spectrum P(k) in FLRW flat models

   Copyright (C) 2013 Jan Ostrowski, Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html

*/

/*! \file power_spectrum_flatspace.h
 *
 * Contains necessary constants for calculation done in
 * power_spectrum_flatspace.c:
 * - \b INHOMOG_TK_BBKS_BKS_R, \b INHOMOG_TK_BBKS_BKS_S,
 * \b INHOMOG_TK_BBKS_BKS_T, \b INHOMOG_TK_BBKS_BKS_U,
 * \b INHOMOG_TK_BBKS_BKS_V - see (C24) in Buchert et al. 2000
 * \latexonly (\href{https://arxiv.org/abs/astro-ph/9912347}
 * {arXiv:astro-ph \textbackslash 9912347v2}) \endlatexonly
 * - \b INHOMOG_PK_BKS_SCALE_FACTOR_RATIO - see
 * \ref power_spectrum_flatspace
 * - \b INHOMOG_PK_BKS_AMPLITUDE_RAW - normalization factor; Buchert
 * et al. 2000 \latexonly (\href{https://arxiv.org/abs/astro-ph/9912347}
 * {arXiv:astro-ph \textbackslash 9912347v2}) \endlatexonly
 * - \b INHOMOG_PK_N and \b INHOMOG_PK_N_0P96 - see Table 2 of
 * Ade et al. 2013 \latexonly (\href{https://arxiv.org/abs/1303.5076}
 * {arXiv:1303.5076v3}) \endlatexonly .
 */

/* "hardwired" version as in BKS00 (C23), (C24) */

/*
The (C24) value
  #define INHOMOG_PK_AMPLITUDE 2.19e4

is arbitrarily decreased by a factor of 8e6, since 2.19e4/201^3 gives
201 * sigma(16) \approx 1.  Hypothesis: factor of a_1^3 accidentally
used in BKS00 calculations?  */

#define INHOMOG_PK_BKS_AMPLITUDE_RAW 2.19e4
/* #define INHOMOG_PK_BKS_AMPLITUDE_RAW (2.19e4*0.5) */

#define INHOMOG_PK_BKS_SCALE_FACTOR_RATIO 201.0

/*
#define INHOMOG_PK_N_1P00 1
#define INHOMOG_PK_N      1.0
*/
/* Ade et al 2013 arXiv:1303.5076v3, Table 2 */
#define INHOMOG_PK_N_0P96 1
#define INHOMOG_PK_N        0.96

#define INHOMOG_TK_BBKS_BKS_R   9.36
#define INHOMOG_TK_BBKS_BKS_S  15.56
#define INHOMOG_TK_BBKS_BKS_T  64.4
#define INHOMOG_TK_BBKS_BKS_U  21.84
#define INHOMOG_TK_BBKS_BKS_V  26.84
