/*
   effective sigma8 calculation

   Copyright (C) 2016, 2017 Boud Roukema, Jan Ostrowski

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html

*/

#include <stdio.h>
#include <sys/types.h>
#include "config.h"
#include <math.h>

#include "lib/inhomog.h"

/** effective sigma8 calculation

    Given sigma_8 inferred from observations under
    FLRW and assuming LCDM, calculate sigma_8
    for an EdS model with the LCDM proxy H_1^bg value
    (see arXiv:1608.06004).
*/

int main(void){
  /* https://arxiv.org/abs/1502.01589 Table 4 column 6 */
  const double sigma_8_LCDM = 0.8159; /* pm 0.0086 */
  double sigma_8_init;
  double sigma_8_EdS_bg; /* EdS background model that matches at high redshifts */
  double sigma_8_EdS_bg_t0eff;

  /* initial redshift, time */
  const double z_init = 1000.0;
  double t_init;
  const double t_0_LCDM = 13.80; /* Gyr */
  double t_0_EdS_bg;

  int want_verbose = 1;

  struct background_cosm_params_s bg_cosm_params_LCDM,
    bg_cosm_params_EdS_bg;

  bg_cosm_params_LCDM.EdS = 0;
  bg_cosm_params_LCDM.flatFLRW = 1;
  bg_cosm_params_LCDM.H_0 = 67.74; /* pm 0.46 */
  bg_cosm_params_LCDM.OmLam_0 = 0.6911; /* pm 0.0062 */
  bg_cosm_params_LCDM.Omm_0 = 1.0 - bg_cosm_params_LCDM.OmLam_0;
  bg_cosm_params_LCDM.recalculate_t_0 = 1;

  bg_cosm_params_LCDM.inhomog_a_scale_factor_initial = 1.0/201.0;
  bg_cosm_params_LCDM.inhomog_a_d_scale_factor_initial = 1.0/201.0;
  bg_cosm_params_LCDM.inhomog_a_scale_factor_now = 1.0;

  bg_cosm_params_EdS_bg.EdS = 1;
  bg_cosm_params_EdS_bg.flatFLRW = 1; /* superfluous */
  bg_cosm_params_EdS_bg.H_0 = 37.7;
  bg_cosm_params_EdS_bg.Omm_0 = 1.0;
  bg_cosm_params_EdS_bg.OmLam_0 = 0.0;
  bg_cosm_params_EdS_bg.recalculate_t_0 = 1;

  bg_cosm_params_EdS_bg.inhomog_a_scale_factor_initial = 1.0/201.0;
  bg_cosm_params_EdS_bg.inhomog_a_d_scale_factor_initial = 1.0/201.0;
  bg_cosm_params_EdS_bg.inhomog_a_scale_factor_now = 1.0;


  t_init = t_flatFLRW(&bg_cosm_params_LCDM,
                      1.0/(z_init+1.0),
                      want_verbose);

  /* Within the LCDM proxy, calculate the initial epoch sigma_8. */
  sigma_8_init = sigma_8_LCDM *
    growth_FLRW(&bg_cosm_params_LCDM,
                t_init,
                want_verbose) /
    growth_FLRW(&bg_cosm_params_LCDM,
                t_0_LCDM,
                want_verbose);

  /* Evolve the initial epoch sigma_8 forward with the EdS background
     model to the *EdS background* unity scale factor epoch t_0_EdS_bg. */
  t_0_EdS_bg = t_EdS(&bg_cosm_params_EdS_bg,
                     1.0,
                     want_verbose);

  sigma_8_EdS_bg = sigma_8_init *
    growth_FLRW(&bg_cosm_params_EdS_bg,
                t_0_EdS_bg,
                want_verbose) /
    growth_FLRW(&bg_cosm_params_EdS_bg,
                t_init,
                want_verbose);

  sigma_8_EdS_bg_t0eff = sigma_8_init *
    growth_FLRW(&bg_cosm_params_EdS_bg,
                t_0_LCDM,
                want_verbose) /
    growth_FLRW(&bg_cosm_params_EdS_bg,
                t_init,
                want_verbose);

  printf("\n");
  printf("z_init = %g\n", z_init);
  printf("t_init,t_0_LCDM,t_0_EdS_bg = %g %g %g\n",
         t_init, t_0_LCDM, t_0_EdS_bg);
  printf("sigma_8_init, sigma_8_LCDM, sigma_8_EdS_bg_t0eff, ");
  printf("sigma_8_EdS_bg = %g %g %g %g\n",
         sigma_8_init, sigma_8_LCDM,
         sigma_8_EdS_bg_t0eff, sigma_8_EdS_bg);
  printf("\n");

  return 0;
}
