/*
   test_power_spectrum_flatspace - power spectrum P(k) in FLRW flat models

   Copyright (C) 2013 Boud Roukema, Jan Ostrowski

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html

*/

/*! \file test_power_spectrum_flatspace.c
 *
 * Allows for an independent test of power_spectrum_flatspace.c.
 * background_cosm_params_s structure is defined and the values are
 * assigned before the main test, but for the power spectrum type 'B'
 * (see: \ref power_spectrum_flatspace) they may be left unevaluated.
 * Checks all \b N_spec_types methods of P(k) evaluation (three by
 * default). Compares results to expected P(k) values using \a tol_Pk
 * parameter value.
 *
 * \b N_K_TEST holds the number of modes for which P(k) will be
 * evaluated.
 *
 * Uses benchmarking to check the execution time before and after each
 * power spectrum evaluation.
 */

#include <stdio.h>
#include <sys/types.h>
#include "config.h"
#include <math.h>
#include <time.h>

#include "lib/inhomog.h"
#include "lib/power_spectrum_flatspace.h"

/* static variables don't make sense in these functions when
   called by multiple threads; thread-local static variables
   would have to be created */
#ifdef _OPENMP
#define POWER_SPECTRUM_STATIC
#else
#define POWER_SPECTRUM_STATIC static
#endif



/* int test_power_spectrum_flatspace(int want_verbose){ */

int main(void){

  /* These parameters should be disabled when running automated
     tests ("make check"). */
  int want_verbose = 0;
  /* Enable this by hand to print the power at a particular k value.
     Linear evolution of the power spectrum is assumed.
   */
  int want_one_k = 0;
  const double sig8 = 0.82; /* sigma_8 normalisation */
  const double one_z = 200; /* redshift */
  double one_L = 1.0; /* input wave length in Mpc (not Mpc/h) */
  double one_k; /* wave number */
  double pp; /* power at one_k */
  double p8; /* power at 2pi/8 Mpc */



  /* #define N_K_TEST 2000 */
#define N_K_TEST 1000000
  double * k_mod; /* [N_K_TEST]; */
#define K_MOD_LOG10_MAX 4.0
  double * P_of_k; /* [N_K_TEST]; */

#define N_spec_types 3
  char pow_spec_types[N_spec_types] = {'B', 'e', 'E'};
  int i_pow_spec_type;
  struct background_cosm_params_s background_cosm_params;

  /* benchmarking */
  clock_t  benchmark[10]; /* num elements must be \ge number of pow spec types */
  int  i_bench=0;

#ifdef INHOMOG_PK_N_1P00
  const double expected_Pk[N_spec_types][4] =
    { {2.693e-07, 2.304e-07, 2.000e-12, 5.599e-18},
      {2.697e-07, 1.369e-07, 1.256e-12, 3.566e-18},
      {2.697e-07, 1.384e-07, 1.281e-12, 3.653e-18} };
#elif INHOMOG_PK_N_0P96
  const double expected_Pk[N_spec_types][4] =
    { {3.892e-07, 2.304e-07, 1.664e-12, 3.874e-18},
      {3.899e-07, 1.369e-07, 1.045e-12, 2.467e-18},
      {3.899e-07, 1.384e-07, 1.066e-12, 2.527e-18} };
#else
#warning "PK index unknown for test_power_spectrum. Expect failure!"
#endif

  const double tol_Pk = 0.01;

  int pass=0;

  /* for pow_spec_type == 'B', background_cosm_params may be left
     unevaluated, since the version used here is
     hardwired for EdS with h=0.5 */

  int i;

  k_mod = malloc(N_K_TEST * sizeof(double));
  P_of_k = malloc(N_K_TEST * sizeof(double));

  /* make check version:  */
  background_cosm_params.Omm_0 = 1.0;
  background_cosm_params.H_0 = 50.0;
  background_cosm_params.Ombary_0 = 0.088;

  background_cosm_params.inhomog_a_scale_factor_initial = 1.0/201.0;
  background_cosm_params.inhomog_a_d_scale_factor_initial = 1.0/201.0;
  background_cosm_params.inhomog_a_scale_factor_now = 1.0;

  background_cosm_params.correct_Pk_norm_known = 0;
  background_cosm_params.correct_Pk_norm = 1.0; /* should be ignored */


  /* LCDM */
  if(want_one_k){
    background_cosm_params.Omm_0 = 0.27;
    background_cosm_params.H_0 = 70.0;
    background_cosm_params.Ombary_0 = 0.045;
  };


  /* testing */
  /*
  background_cosm_params.Omm_0 = 1.0;
  background_cosm_params.H_0 = 38.0;
  background_cosm_params.Ombary_0 = 0.152;
  background_cosm_params.Omm_0 = 0.32;
  background_cosm_params.H_0 = 68.0;
  background_cosm_params.Ombary_0 = 0.048;
  */


  background_cosm_params.Theta2p7 = 2.726/2.7; /* e.g. arXiv:0911.1955 */

  benchmark[i_bench] = clock(); /* before generating delta_tilde */

  for(i_pow_spec_type=0; i_pow_spec_type<N_spec_types; i_pow_spec_type++){

    /*  elementary reminder:
        10^n = exp(log(10^n)) = exp( n* log(10) ) */
    for(i=0; i<N_K_TEST; i++){
      k_mod[i] = exp( (-K_MOD_LOG10_MAX +
                       ((double)i)* (K_MOD_LOG10_MAX - (-K_MOD_LOG10_MAX))/
                       (double)(N_K_TEST-1) ) * log(10.0) );
      P_of_k[i] = power_spectrum_flatspace(k_mod[i],
                                           1.0,  /* scale factor (a(t_0)=1) */
                                           pow_spec_types[i_pow_spec_type],
                                           background_cosm_params
                                           );
    };

    if(want_one_k){
      one_k = 2*M_PI/one_L;
      pp = power_spectrum_flatspace(one_k,
                                    1.0, /* scale factor */
                                    pow_spec_types[i_pow_spec_type],
                                    background_cosm_params
                                    );
      p8 = power_spectrum_flatspace(2.0*M_PI/8.0,
                                    1.0, /* scale factor (a(t_0)=1) */
                                    pow_spec_types[i_pow_spec_type],
                                    background_cosm_params
                                    );

      printf("P(%14.5g), P(2pi/8), pp/p8 *%10.4f, /(1+z): type=%c %14.5g %14.5g %14.5g %14.5g\n",
             one_k,
             sig8,
             pow_spec_types[i_pow_spec_type],
             pp,
             p8,
             pp/p8 * sig8,
             pp/p8 * sig8 /(1.0+one_z)
             );
    };

    /* end hack */



    if(want_verbose){
      for(i=0; i<N_K_TEST; i++){
        printf("test_power_spectrum_flatspace: type=%c %14.5g %14.5g\n",
               pow_spec_types[i_pow_spec_type],
               k_mod[i], P_of_k[i]);
      };
    }else{
      printf("test_power_spectrum_flatspace: %c %8.3e %8.3e %8.3e %8.3e %8.3e %8.3e %8.3e %8.3e\n",
             pow_spec_types[i_pow_spec_type],
             P_of_k[0], P_of_k[1], P_of_k[3], P_of_k[10],
             P_of_k[15],
             P_of_k[N_K_TEST/2-1],
             P_of_k[(int)(0.75*(double)N_K_TEST)],
             P_of_k[N_K_TEST-1]);
    };

    if( !isfinite(fabs(P_of_k[15]-expected_Pk[i_pow_spec_type][0])*2.0/
                  (P_of_k[15]+expected_Pk[i_pow_spec_type][0])) ||
        fabs(P_of_k[15]-expected_Pk[i_pow_spec_type][0])*2.0/
        (P_of_k[15]+expected_Pk[i_pow_spec_type][0]) > tol_Pk){
      pass += 64*i_pow_spec_type + 1;
    };
    if( !isfinite(fabs(P_of_k[N_K_TEST/2-1]-expected_Pk[i_pow_spec_type][1])*2.0/
                  (P_of_k[N_K_TEST/2-1]+expected_Pk[i_pow_spec_type][1])) ||
        fabs(P_of_k[N_K_TEST/2-1]-expected_Pk[i_pow_spec_type][1])*2.0/
        (P_of_k[N_K_TEST/2-1]+expected_Pk[i_pow_spec_type][1]) > tol_Pk){
      pass += 64*i_pow_spec_type + 1;
    };
    if( !isfinite(fabs(P_of_k[(int)(0.75*(double)N_K_TEST)]-expected_Pk[i_pow_spec_type][2])*2.0/
                  (P_of_k[(int)(0.75*(double)N_K_TEST)]+expected_Pk[i_pow_spec_type][2])) ||
        fabs(P_of_k[(int)(0.75*(double)N_K_TEST)]-expected_Pk[i_pow_spec_type][2])*2.0/
        (P_of_k[(int)(0.75*(double)N_K_TEST)]+expected_Pk[i_pow_spec_type][2]) > tol_Pk){
      pass += 64*i_pow_spec_type + 1;
    };
    if( !isfinite(fabs(P_of_k[N_K_TEST-1]-expected_Pk[i_pow_spec_type][3])*2.0/
                  (P_of_k[N_K_TEST-1]+expected_Pk[i_pow_spec_type][3])) ||
        fabs(P_of_k[N_K_TEST-1]-expected_Pk[i_pow_spec_type][3])*2.0/
        (P_of_k[N_K_TEST-1]+expected_Pk[i_pow_spec_type][3]) > tol_Pk){
      pass += 64*i_pow_spec_type + 1;
    };

    i_bench ++;
    benchmark[i_bench]=clock();
    printf("...this test took: ");
    print_benchmark(benchmark[i_bench-1],benchmark[i_bench]);

  }; /* for(i_pow_spec_type=0; i_pow_spec_type<2; i_pow_spec_type++) */

  printf("test_power_spectrum_flatspace: pass = %d\n",pass);

  free(k_mod);
  free(P_of_k);

  return pass;
}
