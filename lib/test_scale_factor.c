/*
   test FLRW background model functions - some may be wrappers

   Copyright (C) 2013,2018 Jan Ostrowski, Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html

*/

#include <stdio.h>
#include <sys/types.h>
#include "config.h"
#include <math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_sort.h>
#include <gsl/gsl_statistics.h>
#include <time.h>

/* for malloc_usable_size if available */
#ifdef __GNUC__
#include <malloc.h>
#endif

#include "lib/inhomog.h"


/* FLRW scale factor */
/* int test_scale_factor(
                      int want_verbose
                      ){
*/
int main(void){

  int want_verbose = 1;

#define N_TEST_T 30
#define N_MODELS 2

  /* These are for benchmarking purposes of e.g. EdS vs LCDM */
  /*
#define HIDE_A 1
#define HIDE_A_DOT 1
#define HIDE_A_DDOT 1
#define HIDE_T 1
  */

  struct background_cosm_params_s background_cosm_params;
  int i_t;
  double delta_t;
  double t_i = 0.004574998; /* for z=200, H_0=50, EdS */
  double t_0 = 13.04;       /* for z=200, H_0=50, EdS */
  double t_background[N_TEST_T];

  /* test parameters */
  double a_test[N_MODELS][N_TEST_T];
  double a_dot_test[N_MODELS][N_TEST_T];
  double a_ddot_test[N_MODELS][N_TEST_T];
  double t_test[N_MODELS][N_TEST_T];
  double a_diff[N_TEST_T];
  double a_dot_diff[N_TEST_T];
  double a_ddot_diff[N_TEST_T];

  /* benchmarking */
  clock_t  benchmark[10]; /* num elements hardwired! */
  int  i_bench=0;


#define TEST_MAX_ERR 1e-4
#define TEST_MEDIAN_ERR 1e-4

  /* The second derivatives have bigger errors when comparing
     nearly-but-not-quite identical EdS and flat FLRW models. */
#define TEST_MAX_ERR_DDOT 2e-3
#define TEST_MEDIAN_ERR_DDOT 2e-3

  int pass = 0; /* default test status = OK = 0 */

  int i_EdS;

  delta_t = (t_0-t_i)/((double)(N_TEST_T-1));

  background_cosm_params.flatFLRW = 1;

  background_cosm_params.inhomog_a_scale_factor_initial = 1.0/201.0;
  background_cosm_params.inhomog_a_d_scale_factor_initial = 1.0/201.0;
  background_cosm_params.inhomog_a_scale_factor_now = 1.0;

  benchmark[i_bench] = clock(); /* initialise timer */

  for(i_EdS=0; i_EdS<2; i_EdS++){
    background_cosm_params.EdS = i_EdS;

    i_bench ++;
    benchmark[i_bench]=clock();
    printf("beginning of i_EdS = %d loop\n",i_EdS);
    print_benchmark(benchmark[i_bench-1],benchmark[i_bench]);

    if(1 == background_cosm_params.EdS){
      background_cosm_params.H_0 = 50.0;
      background_cosm_params.Omm_0 = 1.0;
      background_cosm_params.OmLam_0 = 0.0;
    }else if(1 == background_cosm_params.flatFLRW){
      /* background_cosm_params.H_0 = 70.0;
         background_cosm_params.OmLam_0 = 0.73; */
      background_cosm_params.H_0 = 50.0;
      background_cosm_params.OmLam_0 = 0.001;
      background_cosm_params.Omm_0 = 1.0 - background_cosm_params.OmLam_0;
    };
    background_cosm_params.recalculate_t_0 = 1; /* initially recalculate for this test */


    for(i_t=0; i_t<N_TEST_T; i_t++){
      t_background[i_t] = t_i + (double)i_t * delta_t;

      /* test scale factor and growth function */
      if(1 == background_cosm_params.EdS){
#ifndef HIDE_A
        a_test[i_EdS][i_t] = a_EdS(&background_cosm_params,
                                   t_background[i_t],
                                   want_verbose);
#endif
#ifndef HIDE_A_DOT
        a_dot_test[i_EdS][i_t] =
          a_dot_EdS(&background_cosm_params,
                    t_background[i_t],
                    want_verbose);
#endif
#ifndef HIDE_A_DDOT
        a_ddot_test[i_EdS][i_t] =
          a_ddot_EdS(&background_cosm_params,
                     t_background[i_t],
                     want_verbose);
#endif
#ifndef HIDE_T
        t_test[i_EdS][i_t] = t_EdS(&background_cosm_params,
                                   a_test[i_EdS][i_t],
                                   want_verbose);
#endif
      }else if(1 == background_cosm_params.flatFLRW){
#ifndef HIDE_A
        a_test[i_EdS][i_t] = a_flatFLRW(&background_cosm_params,
                                        t_background[i_t],
                                        want_verbose);
#endif
#ifndef HIDE_A_DOT
        a_dot_test[i_EdS][i_t] =
          a_dot_flatFLRW(&background_cosm_params,
                         t_background[i_t],
                         want_verbose);
#endif
#ifndef HIDE_A_DDOT
        a_ddot_test[i_EdS][i_t] =
          a_ddot_flatFLRW(&background_cosm_params,
                          t_background[i_t],
                          want_verbose);
#endif
#ifndef HIDE_T
        t_test[i_EdS][i_t] = t_flatFLRW(&background_cosm_params,
                                        a_test[i_EdS][i_t],
                                        want_verbose);
#endif
      }; /* if(1 == background_cosm_params.EdS) */
    };  /*     for(i_t=0; i_t<N_TEST_T; i_t++) */

    i_bench ++;
    benchmark[i_bench]=clock();
    printf("...this test took: ");
    print_benchmark(benchmark[i_bench-1],benchmark[i_bench]);

  }; /*   for(i_EdS=0; i_EdS<2; i_EdS++) */

  i_bench ++;
  benchmark[i_bench]=clock();
  printf("...the last few lines took: ");
  print_benchmark(benchmark[i_bench-1],benchmark[i_bench]);

  printf("tcalc_LCDM/tcalc_EdS: %8u %7.2f \n",
         (uint)N_TEST_T,
         (double)(benchmark[2]-benchmark[1])/
         (double)(benchmark[4]-benchmark[3]));


  printf("\ntest_scale_factor:\n");
  printf("test scale factors: t a_LCDM a_EdS adot_LCDM adot_EdS t_LCDM t_EdS\n");
  printf("background_cosm_params.OmLam_0 = %g\n",
         background_cosm_params.OmLam_0);
  for(i_t=0; i_t<N_TEST_T; i_t++){
    printf(":: %g  %g %g  %g %g  %g %g  %g %g\n",
           t_background[i_t],
           a_test[0][i_t],
           a_test[1][i_t],
           a_dot_test[0][i_t],
           a_dot_test[1][i_t],
           a_ddot_test[0][i_t],
           a_ddot_test[1][i_t],
           t_test[0][i_t] ,
           t_test[1][i_t]);

    /* relative errors */
    a_diff[i_t] = fabs(a_test[0][i_t] - a_test[1][i_t]) * 0.5 /
      fabs(a_test[0][i_t] + a_test[1][i_t]);
    a_dot_diff[i_t] = fabs(a_dot_test[0][i_t] - a_dot_test[1][i_t]) * 0.5/
      fabs(a_dot_test[0][i_t] + a_dot_test[1][i_t]);
    a_ddot_diff[i_t] = fabs(a_ddot_test[0][i_t] - a_ddot_test[1][i_t]) * 0.5/
      fabs((a_ddot_test[0][i_t] + a_ddot_test[1][i_t]));
  };

  gsl_sort(a_diff, 1, N_TEST_T);
  gsl_sort(a_dot_diff, 1, N_TEST_T);
  gsl_sort(a_ddot_diff, 1, N_TEST_T);
  printf("median a adot ..., max a adot ... = %g %g %g %g %g %g\n",
           gsl_stats_median_from_sorted_data(a_diff, 1, N_TEST_T),
           gsl_stats_median_from_sorted_data(a_dot_diff, 1, N_TEST_T),
           gsl_stats_median_from_sorted_data(a_ddot_diff, 1, N_TEST_T),
           gsl_stats_max(a_diff, 1, N_TEST_T),
           gsl_stats_max(a_dot_diff, 1, N_TEST_T),
           gsl_stats_max(a_ddot_diff, 1, N_TEST_T));


  /* automated test decision */
  if(gsl_stats_median_from_sorted_data(a_diff, 1, N_TEST_T) > TEST_MEDIAN_ERR) pass += 1;
  if(gsl_stats_median_from_sorted_data(a_dot_diff, 1, N_TEST_T) > TEST_MEDIAN_ERR) pass += 2;
  if(gsl_stats_median_from_sorted_data(a_ddot_diff, 1, N_TEST_T) > TEST_MEDIAN_ERR_DDOT) pass += 4;
  if(gsl_stats_max(a_diff, 1, N_TEST_T) > TEST_MAX_ERR) pass += 8;
  if(gsl_stats_max(a_dot_diff, 1, N_TEST_T) > TEST_MAX_ERR) pass += 16;
  if(gsl_stats_max(a_ddot_diff, 1, N_TEST_T) > TEST_MAX_ERR_DDOT) pass += 32;

  /* check for any infinities or nans */
  if( !(isfinite(gsl_stats_median_from_sorted_data(a_diff, 1, N_TEST_T)) &&
        isfinite(gsl_stats_median_from_sorted_data(a_dot_diff, 1, N_TEST_T)) &&
        isfinite(gsl_stats_median_from_sorted_data(a_ddot_diff, 1, N_TEST_T)) &&
        isfinite(gsl_stats_max(a_diff, 1, N_TEST_T)) &&
        isfinite(gsl_stats_max(a_dot_diff, 1, N_TEST_T)) &&
        isfinite(gsl_stats_max(a_ddot_diff, 1, N_TEST_T))
        ) ) pass += 64;

  printf("pass = %d\n",pass);
  return pass;
}
