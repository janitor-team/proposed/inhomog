/*
   test_gsl_multifit

   Copyright (C) 2015 Boud Roukema, Jan Ostrowski

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html

*/

#include <stdio.h>
#include <sys/types.h>
#include "config.h"
#include <math.h>
#include <time.h>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_statistics.h>

#include <gsl/gsl_multifit.h>

int main(void){
  const gsl_rng_type * T_gsl;
  gsl_rng * r_gsl;

  /* input "observations" */
#define N_OBS 20000
  double xx; /* one x value */
  gsl_matrix *X_obs;
  gsl_vector *y_obs;

  int i;

  /* order of polynomial */
#define DIM_MODEL 3
  double quad_min = 3.1416; /* pi is input; this is not a way to calculate pi ;-) */
  double offset = -3.0;
  double amplitude = 0.1;
  double noise = 0.1;
  gsl_vector *polynom;
  gsl_matrix *cov;
  double chisq;

  double best_estimate_quad_min;

  /*
    -- Function: gsl_multifit_linear_workspace * gsl_multifit_linear_alloc
    (size_t N, size_t P)
    This function allocates a workspace for fitting a model to N
    observations using P parameters.
  */
  gsl_multifit_linear_workspace *multifit_workspace =
    gsl_multifit_linear_alloc(N_OBS, DIM_MODEL);

  gsl_rng_env_setup();
  /*  T_gsl = gsl_rng_default; */
  T_gsl = gsl_rng_ranlxd2; /* L"uscher's RANLUX algorithm, mathematically best, but 10x slower */
  gsl_rng_default_seed += 56453;
  r_gsl = gsl_rng_alloc (T_gsl); /* this gets reallocated each time the
                                    function gets called */

  /* allocate vectors and matrix */
  X_obs = gsl_matrix_alloc(N_OBS,DIM_MODEL);
  y_obs = gsl_vector_alloc(N_OBS);

  polynom = gsl_vector_alloc(DIM_MODEL);
  cov = gsl_matrix_alloc(DIM_MODEL, DIM_MODEL);

  /* generate data */
  for (i=0; i< N_OBS; i++){
    xx = quad_min + gsl_ran_gaussian_ziggurat(r_gsl, quad_min);
    gsl_matrix_set(X_obs, (size_t)i, 0, 1.0);
    gsl_matrix_set(X_obs, (size_t)i, 1, xx);
    gsl_matrix_set(X_obs, (size_t)i, 2, xx*xx);

    gsl_vector_set(y_obs, (size_t)i,
                   amplitude * (xx-quad_min)*(xx-quad_min) + offset
                   + gsl_ran_gaussian_ziggurat(r_gsl, noise) );
  };

  gsl_multifit_linear (X_obs,
                       y_obs,
                       polynom,
                       cov,
                       &chisq,
                       multifit_workspace);

  best_estimate_quad_min =
    -0.5 *gsl_vector_get(polynom,1)/
    gsl_vector_get(polynom,2);
  printf("quad_min = %g  and the best fit estimate is %g\n",
         quad_min,
         best_estimate_quad_min);

  gsl_matrix_free(cov);
  gsl_vector_free(polynom);
  gsl_vector_free(y_obs);
  gsl_matrix_free(X_obs);

  gsl_multifit_linear_free(multifit_workspace);

  gsl_rng_free(r_gsl);

  return 0;
}
