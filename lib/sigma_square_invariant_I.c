/*
   sigma_square_invariant_I - functions for BKS00 (C14)

   Copyright (C) 2013 Jan Ostrowski, Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html

*/

/*! \file sigma_square_invariant_I.c */

#include <stdio.h>
#include <sys/types.h>
#include "config.h"
#include <math.h>
#include <gsl/gsl_rng.h>
/* #include <gsl/gsl_math.h> */
#include <gsl/gsl_monte.h>
#include <gsl/gsl_monte_plain.h>
#include <gsl/gsl_monte_vegas.h>

#include "lib/inhomog.h"
#include "lib/power_spectrum_flatspace.h"

/*! \brief Calculates the square root of a given length scale.
 *
 * Function prevents the situation where the square root of three
 * parameters ("coordinates") passed into the function is too small
 * (the control parameter \b TOL_LENGTH_SQUARED is defined in
 * inhomog.h).
 *
 * \param [in] k_array0
 * \param [in] k_array1
 * \param [in] k_array2
 */
double flat_length_3_3params(double k_array0,
                             double k_array1,
                             double k_array2){
  double dd;
  dd = k_array0*k_array0 +
    k_array1*k_array1 +
    k_array2*k_array2 ;
  dd = sqrt(fmax(TOL_LENGTH_SQUARED,dd));

  return dd;
}

/*! \brief Calculates the square root of a given length scale.
 *
 * Function prevents the situation where the square root of three
 * values ("coordinates") stored in the \a k_array passed into the
 * function is too small (the control parameter \b TOL_LENGTH_SQUARED is
 * defined in inhomog.h).
 *
 * \param [in] k_array
 */
double flat_length_3(double k_array[3]){
  double dd;
  dd = k_array[0]*k_array[0] +
    k_array[1]*k_array[1] +
    k_array[2]*k_array[2] ;
  dd = sqrt(fmax(TOL_LENGTH_SQUARED,dd));

  return dd;
}

#define DEBUG 1

#undef DEBUG

/*! \brief Calculates the integrand function for \f$ \sigma_I^2 (R) \f$
 * calculation.
 *
 * Prepares the integrand function by calling \ref window_R_k (or
 * \ref window_R1_R2_k, depending on \a w_type value) and
 * \ref power_spectrum_flatspace functions. For necessary parameters,
 * such as the window function type or the power spectrum calculation
 * method, uses rza_integrand_params_s structure.
 *
 * The equation used comes from (C14) in Buchert et al. 2000
 * \latexonly (\href{https://arxiv.org/abs/astro-ph/9912347}
 * {arXiv:astro-ph \textbackslash 9912347v2}) \endlatexonly .
 *
 * The \b DEBUG macro allows for checking the mode \f$ k \f$, domain
 * radius/radii \f$ R \f$ and window and integrand functions throughout
 * the calculation.
 *
 * \param [in] k_array pointer to the array of possible mode values
 * \param [in] dim [TODO]
 * \param [in] params a pointer to the void parameter
 */
double sigma_sq_invariant_I_integrand(double * k_array,
                                      size_t dim,
                                      void * params){

  struct rza_integrand_params_s rza_integrand_params;
  double k_mod;
  double window;
  /*   double R_domain; */
  double the_integrand;

  if(dim!=3){
    printf("sigma_sq_invariant_I_integrand: dim = %d != 3\n",
           (int)dim);
    exit(1);
  };
  k_mod = flat_length_3(k_array);
#ifdef DEBUG
  printf("sigma_sq_invariant_I_integrand: k_mod = %g\n",k_mod);
#endif

  if(k_mod < TOL_LENGTH){
    the_integrand = 0.0; /* set to zero if k too close to negative */
  }else{
    /* R_domain = (double)*((double *)params); */
    /* one-parameter version */

    rza_integrand_params = *((struct rza_integrand_params_s *)params);

    switch(rza_integrand_params.w_type)
      {
      case 1:
      default:
#ifdef DEBUG
        printf("sigma_sq_invariant_I_integrand: *R_domain = %g\n",
               rza_integrand_params.R_domain);
#endif

        /* window function: use physical units at initial epoch directly */
        window = window_R_k(rza_integrand_params.R_domain, k_mod);
        break;

      case 2:
#ifdef DEBUG
        if(rza_integrand_params.R_domain_2 <= rza_integrand_params.R_domain_1){
          printf("sigma_sq_invariant_I_integrand: R_domain_2 <= R_domain_1: %g %g\n",
                 rza_integrand_params.R_domain_2, rza_integrand_params.R_domain_1);
        };
#endif
        window = window_R1_R2_k(rza_integrand_params.R_domain_1,
                                rza_integrand_params.R_domain_2,
                                k_mod);
        break;
      }; /* switch(rza_integrand_params.w_type) */

    /* power spectrum: both physical units and initial epoch are needed */
    the_integrand =  power_spectrum_flatspace
      (k_mod,
       rza_integrand_params.background_cosm_params.inhomog_a_scale_factor_initial,
       rza_integrand_params.pow_spec_type,
       rza_integrand_params.background_cosm_params
       )
      * window * window;

#ifdef DEBUG
  printf("sigma_sq_invariant_I_integrand: window, the_integrand = %g\n",
         window,the_integrand);
#endif

  };

  return the_integrand;
}

/*! \brief Calculates \f$ \sigma_I^2 (R) \f$  and its error.
 *
 * Uses Monte Carlo alogrithm through GSL libraries to find the value of
 * the first invariant together with its error. It's possible to use
 * 'plain' version of the algorithm, but the VEGAS method is used by
 * default to reduce errors.
 *
 * Before integration, checks if the caller copied contents of the
 * background_cosm_params_s structure to the rza_integrand_params_s
 * structure through \b BOUNDS_CHECK parameters for \f$ H_0 \f$ and
 * \f$ \Omega_{mm} \f$ (defined in inhomog.h).
 *
 * If \a want_verbose is specified, prints out the domain radius R
 * together with the results (integral value and its error).
 *
 * \param [in] rza_integrand_params_s structure containing parameters
 * necessary for the ODE integration
 * \param [in] n_calls number of times to evaluate the function
 * \param [in] want_verbose control parameter; defined in
 * biscale_partition.c
 * \param [out] the_integral pointer to the result
 * \param [out] integ_error pointer to the result error
 */
int sigma_sq_invariant_I(/* INPUTS; */
                         struct rza_integrand_params_s rza_integrand_params,
                         long   n_calls,  /* number of times to evaluate the function */
                         int want_verbose,
                         double *the_integral, /* OUTPUTS: */
                         double *integ_error
                         )
{
  const gsl_rng_type * T_gsl;
  gsl_rng * r_gsl;

  static unsigned long int local_gsl_seed=0;

  /*  gsl_monte_plain_state * working_space; */
  gsl_monte_vegas_state * working_space;
#define INHOMOG_VEGAS_SMALL ((size_t)10000)
#define INHOMOG_TRY_VEGAS_MAX 10
  int i_try_vegas=0;

  /* a meta-function as per  gsl_monte_function_struct  in gsl_monte.h */
  gsl_monte_function our_meta_function;

  double x_lower[DIM_BKS00];
  double x_upper[DIM_BKS00];
  int i;

  gsl_rng_env_setup();
  T_gsl = gsl_rng_default;
  gsl_rng_default_seed += 3119 + local_gsl_seed;
  local_gsl_seed = gsl_rng_default_seed;
  r_gsl = gsl_rng_alloc (T_gsl); /* this gets reallocated each time the
                                    function gets called */

  /* bounds checks, in case the caller forgot to copy
     the background cosm params into rza_integrands. */
  if(rza_integrand_params.background_cosm_params.H_0 <
     BOUNDS_CHECK_H_0_MIN ||
     rza_integrand_params.background_cosm_params.H_0 >
     BOUNDS_CHECK_H_0_MAX ||
     rza_integrand_params.background_cosm_params.Omm_0 <
     BOUNDS_CHECK_Omm_0_MIN ||
     rza_integrand_params.background_cosm_params.Omm_0 >
     BOUNDS_CHECK_Omm_0_MAX){
    printf("sigma_sq_invariant_I WARNING: ");
    printf("rza_integrand_params.background_cosm_params.H_0 =%g\n",
           rza_integrand_params.background_cosm_params.H_0);
    printf("rza_integrand_params.background_cosm_params.Omm_0 =%g\n",
           rza_integrand_params.background_cosm_params.Omm_0);
  };


  /* meta set up our function (defined in detail above) */
  our_meta_function.f = &sigma_sq_invariant_I_integrand;
  our_meta_function.dim = DIM_BKS00;
  our_meta_function.params = &rza_integrand_params;


  /* gsl_monte_plain_state * gsl_monte_plain_alloc (size_t DIM) */
  /*  working_space =  gsl_monte_plain_alloc (DIM_BKS00); */
  working_space =  gsl_monte_vegas_alloc (DIM_BKS00);

  /*  int gsl_monte_plain_integrate (gsl_monte_function * F,
      const double XL[], const double XU[], size_t DIM, size_t
      CALLS, gsl_rng * R, gsl_monte_plain_state * S, double *
      RESULT, double * ABSERR);

      VEGAS is a more intelligent algorithm.
  */

  /* define the cartesian region of integration */
  for (i=0; (unsigned)i<DIM_BKS00; i++){
    x_lower[i] = K_LOWER_BKS00;
    x_upper[i] = K_UPPER_BKS00;
  };

  /*
  gsl_monte_plain_integrate ( &our_meta_function,
                              x_lower,
                              x_upper,
                              DIM_BKS00,
                              n_calls,
                              r_gsl,
                              working_space,
                              the_integral,
                              integ_error
                              );
  */

  /* warm up: */
  gsl_monte_vegas_integrate ( &our_meta_function,
                              x_lower,
                              x_upper,
                              DIM_BKS00,
                              INHOMOG_VEGAS_SMALL,
                              r_gsl,
                              working_space,
                              the_integral,
                              integ_error
                              );

  /* try 5-tuple repetitions at least a third of the maximum n tries,
     and keep going if the accuracy is still low, but not infinitely */
  while(i_try_vegas < INHOMOG_TRY_VEGAS_MAX &&
        (i_try_vegas < INHOMOG_TRY_VEGAS_MAX/3 ||
         fabs(gsl_monte_vegas_chisq(working_space) -1.0) > 0.5) ) {
    gsl_monte_vegas_integrate ( &our_meta_function,
                                x_lower,
                                x_upper,
                                DIM_BKS00,
                                (size_t)((long)(n_calls/5)),
                                r_gsl,
                                working_space,
                                the_integral,  /* already a pointer */
                                integ_error    /* already a pointer */
                                );
    i_try_vegas++;  /* don't risk an unending bad run at VEGAS */
    /* printf("debug vegas: i_try_VEGAS = %d\n",i_try_vegas); */
  };

  *the_integral *= 64.0 *exp(6.0*log(M_PI));
  *integ_error *= 64.0 *exp(6.0*log(M_PI));

  /* DISABLED:
     multiply by the 8 octants if symmetry assumed and k_j=0 2-planes
     avoided */
  /*
  if(0 < K_LOWER_BKS00 && K_LOWER_BKS00 < K_UPPER_BKS00){
    *the_integral *= 8.0;
    *integ_error *= 8.0;
  };
  */

  if(want_verbose){
    printf("sigma_square_invariant_I: R_domain");
      switch(rza_integrand_params.w_type)
      {
      case 1:
      default:
        printf("= %g, the_integral = %g, err = %g\n",
               rza_integrand_params.R_domain, *the_integral, *integ_error);
        break;

      case 2:
        printf("= %g, the_integral = %g, err = %g\n",
               rza_integrand_params.R_domain_1, *the_integral, *integ_error);
        break;
      };
  };

  gsl_rng_free(r_gsl);
  /*  gsl_monte_plain_free (working_space); */
  gsl_monte_vegas_free (working_space);

  return 0;
}

/*! \brief Performs a test of \f$ \sigma_{I}^2 (R) \f$,
 * \f$ \sigma_{II}^2 (R) \f$ and \f$ \sigma_{III}^2 (R) \f$ calculation.
 *
 * Before calculation, estimates the domain radii values. Calculation
 * depends on the window function type \a w_type.
 *
 * Prints out the calculated values. The number of the values calculated
 * depends either on the \b N_PLOT variable or the
 * \b N_PLOT_BKS00_Table1 (the choice is controlled by \a w_type).
 *
 * \param [in] background_cosm_params pointer to the
 * background_cosm_params_s containing relevant cosmological parameters
 */
int test_sigma_sq_invariants(/* INPUTS: */
                             struct rza_integrand_params_s rza_integrand_params
                             )
{
  double R_domain_lower_BKS00;
  double R_domain_upper_BKS00;
#define N_PLOT 16
#define N_PLOT_BKS00_Table1 7
  int  n_plot; /* value to use depending on chosen option */
  /* #define N_PLOT 2 */
  double delta_ln_k;
  double R_domain_mod[N_PLOT];
  double R_domain_BKS00_Table1[N_PLOT_BKS00_Table1] =
    {0.025, 0.08, 0.25, 0.5, 1.25, 2.5, 5.0};

  double sig2I[N_PLOT];
  double sig2I_err[N_PLOT]; /* estimated standard errors */
  double sig2II[N_PLOT];
  double sig2II_err[N_PLOT]; /* estimated standard errors */
  double sig2III[N_PLOT];
  double sig2III_err[N_PLOT]; /* estimated standard errors */

  long n_calls_I = 300000;

  int integrator_verbose = 0;

  int i;

  switch(rza_integrand_params.w_type)
    {
    case 0:
      n_plot = N_PLOT_BKS00_Table1;
      break;
    default:
      n_plot = N_PLOT;
      break;
    };

  R_domain_lower_BKS00 = 0.02; /* BKS00 Fig 13  0.02Mpc, physical units */
  R_domain_upper_BKS00 = 5.0;  /* BKS00 Fig 13  5Mpc, physical units    */
  delta_ln_k = (log(R_domain_upper_BKS00) - log(R_domain_lower_BKS00))/(double)N_PLOT;



#pragma omp parallel                            \
  default(shared)                               \
  private(i)                                    \
  firstprivate(rza_integrand_params)
  {
#pragma omp for schedule(dynamic)
    for(i=0; i<n_plot; i++){
      switch(rza_integrand_params.w_type)
        {
        case 0:
          R_domain_mod[i] = R_domain_BKS00_Table1[i];
          rza_integrand_params.R_domain = R_domain_mod[i];
          break;

        case 1:
        default:
          R_domain_mod[i] = exp( log(R_domain_lower_BKS00) + ((double)i+0.5)* delta_ln_k );
          rza_integrand_params.R_domain = R_domain_mod[i];
          break;

        case 2:
          R_domain_mod[i] = exp( log(R_domain_lower_BKS00) + ((double)i+0.5)* delta_ln_k );
          rza_integrand_params.R_domain_1 = R_domain_mod[i] -
            0.5*rza_integrand_params.delta_R;
          rza_integrand_params.R_domain_2 = R_domain_mod[i] +
            0.5*rza_integrand_params.delta_R;
          break;
        };

      sigma_sq_invariant_I( rza_integrand_params, n_calls_I,
                            integrator_verbose,
                            &(sig2I[i]), &(sig2I_err[i]) );

      sigma_sq_invariant_II( rza_integrand_params, 3*n_calls_I,
                             integrator_verbose,
                             &(sig2II[i]), &(sig2II_err[i]) );

      sigma_sq_invariant_III( rza_integrand_params, 20*n_calls_I,
                              integrator_verbose,
                              &(sig2III[i]), &(sig2III_err[i]) );
    }; /* for(i=0; i<n_plot; i++)  calculation loop */
  }; /* #pragma omp parallel                          */
  for(i=0; i<n_plot; i++){
    if(rza_integrand_params.w_type >0){
      printf("test_sigma_sq_invariants: %g %g %g %g %g %g %g %g %g %g %g | %g\n",
             R_domain_mod[i],
             sqrt(sig2I[i]), sig2I[i], sig2I_err[i],
             sqrt(sig2II[i]), sig2II[i], sig2II_err[i],
             sqrt(sig2III[i]), sig2III[i], sig2III_err[i],
             3.0*exp(log(fabs(sig2III[i]))/3.0), /* (27 |sig_III|)^{1/3} */
             sig2I[i]*sqrt(sig2I[i]) /27.0    /* BKS00 estimate from sig_I */
             );
    }else{
      printf("test_sigma_sq_invariants: %g  %g %g  %g %g  %g %g \n",
             R_domain_mod[i],
             1e3*sqrt(sig2I[i]), 1e3* 0.5*sig2I_err[i]/sqrt(sig2I[i]),
             1e6*sqrt(sig2II[i]), 1e6* 0.5*sig2II_err[i]/sqrt(sig2II[i]),
             1e12*sqrt(sig2III[i]), 1e12* 0.5*sig2III_err[i]/sqrt(sig2III[i])
             );
    };

  }; /* for(i=0; i<n_plot; i++)   print loop */


  return 0;
}
