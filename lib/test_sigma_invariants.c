/*
   test_sigma_invariants - brute force test of BKS 2000 Appendix C formulae

   Copyright (C) 2013 Boud Roukema, Jan Ostrowski

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html

*/

#include <stdio.h>
#include <sys/types.h>
#include "config.h"
#include <math.h>
#include <time.h>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_sort.h>
#include <gsl/gsl_statistics.h>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_complex_math.h>

/* #include <fftw3.h> */

#ifdef _OPENMP
#include <omp.h>
#endif


#include "lib/inhomog.h"
#include "lib/power_spectrum_flatspace.h"
#include "lib/test_sigma_invariants.h"


long index3D(int i,
             int j,
             int k,
             int N){
  long index_3D;
#ifndef TEST_INVAR_TEST1D
  index_3D = (long)(i*N*N + j*N + k);
#else
  index_3D = (long)k; /* i, j totally ignored in 1D case */
#endif
  return index_3D;
}



double power_spec3D(/* INPUTS */
                 double k_signed
                 /* OUTPUTS */
                 ){

  const double kpeak = TEST_INVAR_kpeak;
  const double Pk_norm = TEST_INVAR_Pk_norm;
  const double n_HarrZel = TEST_INVAR_n_HarrZel;
  const double n_small_L_scale = -3.0;

  double k_wave;
  /*   double k_sigma, k_diff_sig; */

  k_wave = k_signed * k_signed;
  if(k_wave > 1e-200){
    k_wave = sqrt(k_wave);
  }else{
    k_wave = 1e-100;
  };

  /* Gaussian  P(k) */
  /*
  k_sigma = kpeak;
  k_diff_sig = fabs(k_wave - kpeak)/k_sigma;
  return (Pk_norm * exp(-0.5 *k_diff_sig*k_diff_sig));
  */

  /*  CDM-like P(k) */
  if( k_wave < kpeak){
    return (Pk_norm * exp( n_HarrZel *log( k_wave/kpeak ) ));
  }else{
    return (Pk_norm * exp( n_small_L_scale *log( k_wave/kpeak ) ));
  };
}


/* randomly generate a delta realisation from the power spectrum */
int get_delta_tilde_realisation(/* INPUTS */
                                unsigned long int rng_seed,
                                /* OUTPUTS */
                                double i_array[TEST_INVAR_N_k],
                                double j_array[TEST_INVAR_N_k],
                                double k_array[TEST_INVAR_N_k],
                                gsl_complex *delta_tilde){
  /*                            double *d_k){ */


  double *A_k_array;
  double phi_k;

  const int N_k = TEST_INVAR_N_k;
  const double Ak_min = TEST_INVAR_Ak_min;

  const double kmax = TEST_INVAR_kmax;
  double d_i, d_j, d_k; /* intervals in each direction */
  double kmin, kmin_i, kmin_j, kmin_k;

  const gsl_rng_type * T_gsl = TEST_INVAR_GSL_RNG_TYPE;
  gsl_rng * r_gsl;

  double k_wave;
  double A_k;
  int ii=0, jj=0, kk;


  r_gsl = gsl_rng_alloc (T_gsl); /* this gets reallocated each time the
                                    function gets called */
  gsl_rng_set(r_gsl, rng_seed); /* seed given by main routine */


  if( 2*(int)(floor(N_k/2) +0.01) != N_k ){
    /* Odd N_k is in principle doable with fftw3, but it's not
       implemented here. */
    printf("get_delta_tilde_realisation ERROR: N_k = %d not even!\n",
           N_k);
    exit(1);
  };


  /* set wave numbers */
  /* easiest approach is to avoid k=0: */
  kmin= -kmax;
  /* avoid possible cancels related to equi-scaled grid */
  kmin_i = kmin *
    gsl_ran_flat(r_gsl,
                 1.0 - TEST_INVAR_kmax_fraction_vary,
                 1.0 + TEST_INVAR_kmax_fraction_vary);
  d_i = -2.0*kmin_i/(double)N_k;
  for(ii=0; ii<N_k; ii++){
    i_array[ii] =
      kmin_i + (0.5+(double)ii)*(d_i);
  };

  kmin_j = kmin *
    gsl_ran_flat(r_gsl,
                 1.0 - TEST_INVAR_kmax_fraction_vary,
                 1.0 + TEST_INVAR_kmax_fraction_vary);
  d_j = -2.0*kmin_j/(double)N_k;
  /* set wave numbers */
  for(ii=0; ii<N_k; ii++){
    j_array[ii] =
      kmin_j + (0.5+(double)ii)*(d_j);
  };

  kmin_k = kmin *
    gsl_ran_flat(r_gsl,
                 1.0 - TEST_INVAR_kmax_fraction_vary,
                 1.0 + TEST_INVAR_kmax_fraction_vary);
  d_k = -2.0*kmin_k/(double)N_k;
  /* set wave numbers */
  for(ii=0; ii<N_k; ii++){
    k_array[ii] =
      kmin_k + (0.5+(double)ii)*(d_k);
  };


  /* allocate  big array */
  if((A_k_array = malloc((size_t)(N_k*N_k*N_k) * sizeof(double)))){
        /* allocation successful */
  }else{
    printf("Out of memory for A_k_array.\n");
    exit(1);
  };


  /* generate Gaussian random amplitudes and uniform random phases */
#ifndef TEST_INVAR_TEST1D
  for(ii=0; ii<N_k; ii++){
    for(jj=0; jj<N_k; jj++){
#endif
      for(kk=0; kk<N_k; kk++){

        /* beyond the monopole */
#ifndef TEST_INVAR_TEST1D
        k_wave = flat_length_3_3params
          (i_array[ii],
           j_array[jj],
           k_array[kk]);
#else
        k_wave = fabs(k_array[kk]);
#endif

          /* draw an amplitude from a Gaussian of width sqrt(P(k)) and zero mean */
          /* GSL method: Marsaglia-Tsang ziggurat - for speed */
          A_k = power_spec3D(k_wave);
          if(A_k < Ak_min){
            A_k = Ak_min; /* avoid sqrt(0.0) width of Gaussian */
          }else{
            A_k = sqrt(A_k);
          };
          A_k_array[ index3D(ii,jj,kk,N_k) ] =
            gsl_ran_gaussian(r_gsl, A_k);
      }; /*     for(kk=0; kk<N_k; kk++) */
#ifndef TEST_INVAR_TEST1D
    };  /* for(jj=0; jj<N_k; jj++) */
  };  /*  for(ii=0; ii<N_k; ii++) */
#endif

#ifndef TEST_INVAR_TEST1D
  for(ii=0; ii<N_k; ii++){
    for(jj=0; jj<N_k; jj++){
#endif
      for(kk=0; kk<N_k; kk++){
        phi_k =
          gsl_ran_flat(r_gsl, 0.0, 2.0*M_PI);

        delta_tilde[ index3D(ii,jj,kk,N_k) ] =
          gsl_complex_rect
          ( /* A_k e^{i phi_k} real part */
           A_k_array[ index3D(ii,jj,kk, N_k) ] *
           cos(phi_k),
           /* A_k e^{i phi_k} imag part */
           A_k_array[ index3D(ii,jj,kk, N_k) ] *
           sin(phi_k)
            );
      }; /*     for(kk=0; kk<N_k; kk++) */
#ifndef TEST_INVAR_TEST1D
    };  /* for(jj=0; jj<N_k; jj++) */
  };  /*  for(ii=0; ii<N_k; ii++) */
#endif

  free(A_k_array); /* internal to this routine only */

  gsl_rng_free(r_gsl);
  return 0;
}


int check_delta_tilde1D(struct delta_tilde_struct_s * delta_tilde_struct){

  const int N_k = TEST_INVAR_N_k;
  int k1;

  printf("check_delta_tilde1D ");
  for(k1=0; k1<N_k; k1++){
    printf("delta_tilde[%d]: abs, arg = %g %g ",
           k1,
           gsl_complex_abs
           (delta_tilde_struct->delta_tilde[ index3D(0,0, k1, N_k) ]),
           gsl_complex_arg
           (delta_tilde_struct->delta_tilde[ index3D(0,0, k1, N_k) ]));
  };
  printf("\n");
  return 0;
}



int main(void){

  const int N_k = TEST_INVAR_N_k;
  double i_array[TEST_INVAR_N_k];
  double j_array[TEST_INVAR_N_k];
  double k_array[TEST_INVAR_N_k];
  gsl_complex *delta_tilde; /* delta tilde */
#ifdef TEST_INVAR_TEST1D
  gsl_complex *expectation_delta_tilde_square = NULL;
#endif
  struct delta_tilde_struct_s delta_tilde_struct;
  /*  double d_k; */

  const int N_realisations = TEST_INVAR_N_realisations;
  const unsigned int N_realisations_u = TEST_INVAR_N_realisations;
  int i_realise;

  double *C7;
  double *C14;
  double *C7_C14_ratio;
  double C7_C14_ratio_mean; /* mean */
  double C7_C14_ratio_serr; /* standard error (in the mean) */
  double C7_C14_ratio_median; /* median */

  double *C8;
  double *C18;
  double *C8_C18_ratio;
  double C8_C18_ratio_mean; /* mean */
  double C8_C18_ratio_serr; /* standard error (in the mean) */
  double C8_C18_ratio_median; /* median */

  double *C9;
  double *C20;
  double *C9_C20_ratio;
  double C9_C20_ratio_mean; /* mean */
  double C9_C20_ratio_serr; /* standard error (in the mean) */
  double C9_C20_ratio_median; /* median */

  const gsl_rng_type * T_gsl = TEST_INVAR_GSL_RNG_TYPE;
  gsl_rng * r_gsl;
  unsigned long int *rng_seed; /* seeds for individual realisations */


  /* benchmarking */
  clock_t  benchmark[10];
  double overall_bench_s;
  int i_thread;
  int iverbose=0;

  /* debug */
  int k1,k2;
#ifdef TEST_INVAR_TEST1D
  double *delta_tilde_abs;
  double *delta_tilde_arg;
#endif

  gsl_rng_env_setup();
  r_gsl = gsl_rng_alloc (T_gsl); /* this gets reallocated each time the
                                    function gets called */

  benchmark[8] = clock(); /* global: before generating delta_tilde */



#ifdef TEST_INVAR_TEST1D
  expectation_delta_tilde_square =
    malloc((size_t)(N_k*N_k) * sizeof(gsl_complex));
  for(k1=0; k1<N_k; k1++){
      for(k2=0; k2<N_k; k2++){
        expectation_delta_tilde_square[k1*N_k + k2] =
          gsl_complex_rect(0.0,0.0);
      };
  };
  delta_tilde_abs = malloc((size_t)(N_k*N_realisations) * sizeof(double));
  delta_tilde_arg = malloc((size_t)(N_k*N_realisations) * sizeof(double));
#endif

  C7 = malloc(N_realisations_u *sizeof(double));
  C14 = malloc(N_realisations_u *sizeof(double));
  C7_C14_ratio = malloc(N_realisations_u *sizeof(double));

  C8 = malloc(N_realisations_u *sizeof(double));
  C18 = malloc(N_realisations_u *sizeof(double));
  C8_C18_ratio = malloc(N_realisations_u *sizeof(double));

  C9 = malloc(N_realisations_u *sizeof(double));
  C20 = malloc(N_realisations_u *sizeof(double));
  C9_C20_ratio = malloc(N_realisations_u *sizeof(double));

  rng_seed = malloc(N_realisations_u *sizeof(unsigned long int));
  for (i_realise=0; i_realise<N_realisations; i_realise++){
    rng_seed[i_realise] = gsl_rng_get(r_gsl);
  };

  /* d_k, */
#pragma omp parallel for                \
  default(shared)                       \
  schedule(dynamic)                     \
  private(i_realise,i_thread,           \
          delta_tilde,                  \
          i_array,j_array,              \
          k_array,                      \
          delta_tilde_struct,           \
          k1,k2                         \
          )                             \
  firstprivate(benchmark,               \
               rng_seed,                \
               iverbose)
    for (i_realise=0; i_realise<N_realisations; i_realise++){

#ifdef _OPENMP
      i_thread= omp_get_thread_num();
#else
      i_thread=0;
#endif

      /* allocate  big array */
      if((delta_tilde = malloc((size_t)(N_k*N_k*N_k) * sizeof(gsl_complex)))){
        /* allocation successful */
      }else{
        printf("Out of memory for delta_tilde.\n");
        exit(1);
      };


      benchmark[0] = clock(); /* before generating delta_tilde */

      iverbose= (i_realise < 3 && i_thread < 2);

      if(iverbose){
        printf("i_realise = %d\n",i_realise);
      };
      get_delta_tilde_realisation(/* INPUTS */
                                  rng_seed[i_realise],
                                  /* OUTPUTS */
                                  i_array,
                                  j_array,
                                  k_array,
                                  delta_tilde);
      /*                                  &d_k); */

      delta_tilde_struct.delta_tilde = delta_tilde;
      delta_tilde_struct.i_array = i_array;
      delta_tilde_struct.j_array = j_array;
      delta_tilde_struct.k_array = k_array;
      /*      delta_tilde_struct.d_k = d_k; */

      benchmark[1] = clock();
      if(iverbose){
        printf("i_thread=%d; generating delta_tilde took: ",i_thread);
        print_benchmark(benchmark[0],benchmark[1]);
        for(k1=0; k1<N_k; k1++){
          printf("i_array[%d] = %g\n",
                 k1,i_array[k1]);
        };
        for(k1=0; k1<N_k; k1++){
          printf("j_array[%d] = %g\n",
                 k1,j_array[k1]);
        };
        for(k2=0; k2<N_k; k2++){
          printf("k_array[%d] = %g\n",
                 k2,k_array[k2]);
        };
      };

#ifdef TEST_INVAR_TEST1D
      if(i_realise < 100)check_delta_tilde1D(&delta_tilde_struct);
#endif

      C7[i_realise] = C7_sq_integral(&delta_tilde_struct);
      C14[i_realise] = C14_integral(&delta_tilde_struct);
      C7_C14_ratio[i_realise] = C7[i_realise]/C14[i_realise];

      benchmark[2] = clock();
      if(iverbose){
        printf("C7, C14 = %g %g\n",
               C7[i_realise],
               C14[i_realise]);
      printf("i_thread=%d; inv I  took: ",i_thread);
      print_benchmark(benchmark[1],benchmark[2]);
      };


      C8[i_realise] = C8_sq_integral(&delta_tilde_struct);
      C18[i_realise] = C18_integral(&delta_tilde_struct);
      C8_C18_ratio[i_realise] = C8[i_realise]/C18[i_realise];

      benchmark[2] = clock();
      if(iverbose){
        printf("C8, C18 = %g %g\n",
               C8[i_realise],
               C18[i_realise]);
      printf("i_thread=%d; inv I  took: ",i_thread);
      print_benchmark(benchmark[1],benchmark[2]);
      };


      C9[i_realise] = C9_sq_integral(&delta_tilde_struct);
      C20[i_realise] = C20_integral(&delta_tilde_struct);
      C9_C20_ratio[i_realise] = C9[i_realise]/C20[i_realise];

      benchmark[2] = clock();
      if(iverbose){
        printf("C9, C20 = %g %g\n",
               C9[i_realise],
               C20[i_realise]);
      printf("i_thread=%d; inv I  took: ",i_thread);
      print_benchmark(benchmark[1],benchmark[2]);
      };


#ifdef TEST_INVAR_TEST1D
      for(k1=0; k1<N_k; k1++){
        delta_tilde_abs[k1*N_realisations + i_realise] = gsl_complex_abs2
          (delta_tilde[ index3D(0,0, k1, N_k) ]);
        delta_tilde_arg[k1*N_realisations + i_realise] = gsl_complex_arg
          (delta_tilde[ index3D(0,0, k1, N_k) ]);

        for(k2=0; k2<N_k; k2++){
          expectation_delta_tilde_square[k1*N_k + k2] =
            gsl_complex_add
            (expectation_delta_tilde_square[k1*N_k + k2],
             gsl_complex_mul
             (delta_tilde[ index3D(0,0, k1, N_k) ] ,
              delta_tilde[ index3D(0,0, k2, N_k) ] )
             );
        };
      };
#endif


      free(delta_tilde);
    }; /*  for (i_realise=0; i_realise<N_realisations; i_realise++) */
    /*  #pragma omp parallel for */

  free(rng_seed);
  gsl_rng_free(r_gsl);


  C7_C14_ratio_mean = gsl_stats_mean(C7_C14_ratio, 1, N_realisations_u);
  C7_C14_ratio_serr = gsl_stats_sd(C7_C14_ratio, 1, N_realisations_u)/
    sqrt((double)N_realisations);
  gsl_sort(C7_C14_ratio, 1, N_realisations_u);
  C7_C14_ratio_median = gsl_stats_median_from_sorted_data(C7_C14_ratio, 1, N_realisations_u);

  printf("N_realisations = %d\n",N_realisations);
  printf("inv I C7/C14 stats: %g; %g \\pm %g\n",
         C7_C14_ratio_median,
         C7_C14_ratio_mean,
         C7_C14_ratio_serr); /* Warning: stand error in mean, not median */

  C8_C18_ratio_mean = gsl_stats_mean(C8_C18_ratio, 1, N_realisations_u);
  C8_C18_ratio_serr = gsl_stats_sd(C8_C18_ratio, 1, N_realisations_u)/
    sqrt((double)N_realisations);
  gsl_sort(C8_C18_ratio, 1, N_realisations_u);
  C8_C18_ratio_median = gsl_stats_median_from_sorted_data(C8_C18_ratio, 1, N_realisations_u);

  printf("N_realisations = %d\n",N_realisations);
  printf("inv II C8/C18 stats: %g; %g \\pm %g\n",
         C8_C18_ratio_median,
         C8_C18_ratio_mean,
         C8_C18_ratio_serr); /* Warning: stand error in mean, not median */


  C9_C20_ratio_mean = gsl_stats_mean(C9_C20_ratio, 1, N_realisations_u);
  C9_C20_ratio_serr = gsl_stats_sd(C9_C20_ratio, 1, N_realisations_u)/
    sqrt((double)N_realisations);
  gsl_sort(C9_C20_ratio, 1, N_realisations_u);
  C9_C20_ratio_median = gsl_stats_median_from_sorted_data(C9_C20_ratio, 1, N_realisations_u);

  printf("N_realisations = %d\n",N_realisations);
  printf("inv III C9/C20 stats: %g; %g \\pm %g\n",
         C9_C20_ratio_median,
         C9_C20_ratio_mean,
         C9_C20_ratio_serr); /* Warning: stand error in mean, not median */


  free(C7);
  free(C14);
  free(C7_C14_ratio);
  free(C8);
  free(C18);
  free(C8_C18_ratio);
  free(C9);
  free(C20);
  free(C9_C20_ratio);

  benchmark[9] = clock();
  printf("global timing for N_realisations = %d ",N_realisations);
#ifdef _OPENMP
  printf("num threads available was %d ",
         omp_get_max_threads());
  overall_bench_s = print_benchmark(benchmark[8],benchmark[9]);
  printf("mean time per thread was %g\n",
         overall_bench_s /(double)omp_get_max_threads());
#else
  printf(": ");
  print_benchmark(benchmark[8],benchmark[9]);
#endif



#ifdef TEST_INVAR_TEST1D
  for(k1=0; k1<N_k; k1++){
    printf("delta_tilde mean abs^2 = %g\n",
           gsl_stats_mean
           ((double*)&(delta_tilde_abs[k1*N_realisations]), 1,
            N_realisations_u));
  };
  for(k1=0; k1<N_k; k1++){
    printf("delta_tilde mean arg = %g\n",
           gsl_stats_mean
           ((double*)&(delta_tilde_arg[k1*N_realisations]), 1,
            N_realisations_u));
  };
  for(k1=0; k1<N_k; k1++){
    printf("delta_tilde min, max arg = %g %g\n",
           gsl_stats_min
           ((double*)&(delta_tilde_arg[k1*N_realisations]), 1,
            N_realisations_u),
           gsl_stats_max
           ((double*)&(delta_tilde_arg[k1*N_realisations]), 1,
            N_realisations_u));
  };
  for(k1=0; k1<N_k; k1++){
    printf("delta_tilde var arg = %g\n",
           gsl_stats_variance
           ((double*)&(delta_tilde_arg[k1*N_realisations]), 1,
            N_realisations_u));
  };


  printf("expectation_delta_tilde_square:\n");
  for(k1=0; k1<N_k; k1++){
    for(k2=0; k2<N_k; k2++){
      expectation_delta_tilde_square[k1*N_k + k2] =
        gsl_complex_div_real
        (expectation_delta_tilde_square[k1*N_k + k2],
         (double)N_realisations);
      printf("%g ",
             gsl_complex_abs
             (expectation_delta_tilde_square[k1*N_k + k2]));
    };
    printf("\n");
  };
  printf("power_spec: %g %g\n",
         power_spec3D(0.5),power_spec3D(-0.5));
  free(expectation_delta_tilde_square);
  free(delta_tilde_abs);
  free(delta_tilde_arg);
#endif

  return 0;
}
