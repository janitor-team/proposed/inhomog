lib_LTLIBRARIES = libinhomog.la

libinhomog_la_SOURCES=power_spectrum_flatspace.c \
	FLRW_background.c \
	sigma_square_invariant_I.c \
	sigma_square_invariant_II.c \
	sigma_square_invariant_III.c \
	kinematical_backreaction.c \
	curvature_backreaction.c \
        scale_factor_D.c \
        scale_factor_D_Ray.c \
        scale_factor_D_Ham.c \
	growth_function.c \
	Omega_D.c \
	Omega_D_precalc.c \
	biscale_partition.c \
	parse_noempty_strtox.c \
	print_benchmark.c \
	alloc_big_array.c \
	c_gsl_wrap.c \
	backward_compatibility.c

libinhomog_la_LDFLAGS = -version-info $(INHOMOG_LT_VERSION) \
	-export-symbols-regex "^([OTa-qs-z]|report_out)"


pkginclude_HEADERS = ../config.h inhomog.h power_spectrum_flatspace.h \
	biscale_partition.h \
	parse_noempty_strtox.h \
	alloc_big_array.h

noinst_PROGRAMS= \
	test_power_spectrum_flatspace \
	test_scale_factor test_growth_function \
	test_ode test_gsl_monte_carlo \
	test_gsl_uniform \
	test_parse_noempty_strtox \
	test_kinematical_backreaction \
	test_scale_factor_D \
	test_scale_factor_D_Ray test_scale_factor_D_Ham test_Omega_D \
	test_biscale_partition \
	test_sigma_invariants \
	test_gsl_multifit \
	test_gsl_ieee_mode \
	test_alloc_big_array \
	Omega_D_BAO \
	Omega_D_VAv2 \
	Omega_D_effWEC \
	effective_sigma8

# not presently used - OK to keep source code, but don't force the dependence
# test_fftw3

test_power_spectrum_flatspace_SOURCES = test_power_spectrum_flatspace.c
test_power_spectrum_flatspace_LDADD=libinhomog.la

test_scale_factor_SOURCES = test_scale_factor.c
test_scale_factor_LDADD=libinhomog.la

test_growth_function_SOURCES = test_growth_function.c
test_growth_function_LDADD=libinhomog.la

test_ode_SOURCES = test_ode.c
test_ode_LDADD=libinhomog.la

test_gsl_monte_carlo_SOURCES = test_gsl_monte_carlo.c
test_gsl_monte_carlo_LDADD=libinhomog.la

test_gsl_uniform_SOURCES = test_gsl_uniform.c
test_gsl_uniform_LDADD=libinhomog.la

test_parse_noempty_strtox_SOURCES = test_parse_noempty_strtox.c
test_parse_noempty_strtox_LDADD=libinhomog.la

test_alloc_big_array_SOURCES = test_alloc_big_array.c
test_alloc_big_array_LDADD=libinhomog.la

test_kinematical_backreaction_SOURCES = test_kinematical_backreaction.c
test_kinematical_backreaction_LDADD=libinhomog.la

test_scale_factor_D_SOURCES = test_scale_factor_D.c
test_scale_factor_D_LDADD=libinhomog.la

test_scale_factor_D_Ray_SOURCES = test_scale_factor_D_Ray.c
test_scale_factor_D_Ray_LDADD=libinhomog.la

test_scale_factor_D_Ham_SOURCES = test_scale_factor_D_Ham.c
test_scale_factor_D_Ham_LDADD=libinhomog.la

test_Omega_D_SOURCES = test_Omega_D.c
test_Omega_D_LDADD=libinhomog.la

test_biscale_partition_SOURCES = test_biscale_partition.c
test_biscale_partition_LDADD=libinhomog.la

Omega_D_BAO_SOURCES = Omega_D_BAO.c
Omega_D_BAO_LDADD=libinhomog.la

Omega_D_VAv2_SOURCES = Omega_D_VAv2.c
Omega_D_VAv2_LDADD=libinhomog.la

Omega_D_effWEC_SOURCES = Omega_D_effWEC.c
Omega_D_effWEC_LDADD=libinhomog.la

effective_sigma8_SOURCES = effective_sigma8.c
effective_sigma8_LDADD=libinhomog.la

EXTRA_DIST = test_sigma_invariants.h test_ode.h

TESTS_FAST=test_gsl_monte_carlo test_ode \
	test_power_spectrum_flatspace test_scale_factor \
	test_growth_function \
	test_biscale_partition \
	test_alloc_big_array
TESTS_SLOW=test_Omega_D \
	test_kinematical_backreaction test_scale_factor_D

## Only run the full set of tests in development mode. Automated build
## software should be given smaller scale tests only.
if DEV_MODE
  TESTS=$(TESTS_FAST) $(TESTS_SLOW)
else
  TESTS=$(TESTS_FAST)
endif

## tests that are for development only, not for a standard make check
test_sigma_invariants_SOURCES = test_sigma_invariants.c \
	delta_tilde_integrals.c
test_sigma_invariants_LDADD=libinhomog.la

#test_fftw3_SOURCES = test_fftw3.c
#test_fftw3_LDADD=libinhomog.la

test_gsl_multifit_SOURCES = test_gsl_multifit.c
test_gsl_multifit_LDADD=libinhomog.la

test_gsl_ieee_mode_SOURCES = test_gsl_ieee_mode.c
test_gsl_ieee_mode_LDADD=libinhomog.la

TESTS_DEV=test_sigma_invariants tests_gsl_multifit \
	test_gsl_ieee_mode
# test_fftw3

#  uncomment the following if inhomog requires the math library
#inhomog_LDADD=-lm

## -ansi ## causes M_PI to fail
## -Werror  error: ignoring #pragma omp parallel
if DEV_MODE
  GSL_RECOMMENDED=-pedantic -Wall -W -Wmissing-prototypes -Wstrict-prototypes -Wwrite-strings -Wconversion -Wshadow -Wpointer-arith -Wcast-qual -Wcast-align -Wnested-externs -fshort-enums -fno-common -Dinline= -g -O3
  GSL_RECOMMENDED += -fdiagnostics-color=always -fsanitize=address
else
  GSL_RECOMMENDED=-O3
endif

ACLOCAL_AMFLAGS = -I m4
AM_CFLAGS = -Wall -Wextra $(OPENMP_CFLAGS) $(GSL_RECOMMENDED)
AM_CPPFLAGS = $(OPENMP_CFLAGS) $(GSL_RECOMMENDED)


#  if you write a self-test script named `chk', uncomment the
#  following and add `chk' to the EXTRA_DIST list
#TESTS=chk

# Variables for creating a pkg-config file
# ( https://en.wikipedia.org/wiki/pkg-config )
# inspired from GSL and gnuastro's Makefile.am and lib/Makefile.am,
# respectively.
pkgconfig_DATA = inhomog.pc
pkgconfigdir = $(libdir)/pkgconfig
CLEANFILES = inhomog.pc



inhomog.pc: Makefile
	rm -f $@ $@.tmp
	$(SED)                                      \
	-e 's|@prefix[@]|$(prefix)|g'               \
	-e 's|@exec_prefix[@]|$(exec_prefix)|g'     \
	-e 's|@libdir[@]|$(libdir)|g'               \
	-e 's|@includedir[@]|$(includedir)|g'       \
	-e 's|@LIBS[@]|$(LIBS)|g'                   \
	-e 's|@VERSION[@]|$(VERSION)|g'             \
	'$(srcdir)/$@.in' >> $@.tmp
	chmod a-w $@.tmp
	mv $@.tmp $@
