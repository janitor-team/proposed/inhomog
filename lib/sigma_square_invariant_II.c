/*
   sigma_square_invariant_II - functions for BKS00 (C17)

   Copyright (C) 2013 Jan Ostrowski, Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html

*/

/*! \file sigma_square_invariant_II.c */

#include <stdio.h>
#include <sys/types.h>
#include "config.h"
#include <math.h>
#include <gsl/gsl_rng.h>
/* #include <gsl/gsl_math.h> */
#include <gsl/gsl_monte.h>
#include <gsl/gsl_monte_plain.h>
#include <gsl/gsl_monte_vegas.h>

#include "lib/inhomog.h"
#include "lib/power_spectrum_flatspace.h"

/*
double flat_dot_product_3(double k_array[6]){
  double dd;
  dd = k_array[0]*k_array[3] +
    k_array[1]*k_array[4] +
    k_array[2]*k_array[5] ;

  return dd;
};
*/

#define DEBUG 1

#undef DEBUG


/*! \brief Calculates the integrand function for \f$ \sigma_{II}^2 (R)
 * \f$ calculation.
 *
 * Prepares the integrand function by calling \ref window_R_k (or
 * \ref window_R1_R2_k, depending on \a w_type value) and
 * \ref power_spectrum_flatspace functions. For necessary parameters,
 * such as the window function type or the power spectrum calculation
 * method, uses rza_integrand_params_s structure.
 *
 * The equation used comes from (C18) in Buchert et al. 2000
 * \latexonly (\href{https://arxiv.org/abs/astro-ph/9912347}
 * {arXiv:astro-ph \textbackslash 9912347v2}) \endlatexonly .
 *
 * The \b DEBUG macro allows for checking the mode \f$ k \f$, domain
 * radius/radii \f$ R \f$ and window and integrand functions throughout
 * the calculation.
 *
 * \param [in] k_array pointer to the array of possible mode values
 * \param [in] dim [TODO]
 * \param [in] params a pointer to the void parameter
 */
double sigma_sq_invariant_II_integrand(double * k_array,
                                      size_t dim,
                                      void * params){

  struct rza_integrand_params_s rza_integrand_params;
  double k_mod_1, k_mod_2; /* 3D mod, 3D mod, respectively */
  double k_sum[DIM_BKS00];          /* vec(k_1) + vec(k_2) */
  double k_sum_mod;
  double window;
  /* double R_domain; */
  double third_factor; /* third factor in BKS00 (C18) */
  double the_integrand;

  if(dim!=6){
    printf("sigma_sq_invariant_II_integrand: dim = %d != 6\n",
           (int)dim);
    exit(1);
  };

  k_mod_1 = flat_length_3(k_array);
  k_mod_2 = flat_length_3( &(k_array[3]) );
  k_sum[0]= k_array[0] + k_array[3];
  k_sum[1]= k_array[1] + k_array[4];
  k_sum[2]= k_array[2] + k_array[5];
  k_sum_mod = flat_length_3(k_sum);
#ifdef DEBUG
  printf("sigma_sq_invariant_I_integrand: k_mod_1 = %g\n",k_mod_1);
#endif

  if(k_mod_1 < TOL_LENGTH || k_mod_2 < TOL_LENGTH){
    the_integrand = 0.0; /* set to zero if k too close to negative */
  }else{

    rza_integrand_params = *((struct rza_integrand_params_s *)params);

    /* window function: use physical units at initial epoch directly */
    switch(rza_integrand_params.w_type)
      {
      case 1:
      default:
        /* window function: use physical units at initial epoch directly */
        window = window_R_k(rza_integrand_params.R_domain, k_sum_mod);
        break;

      case 2:
        window = window_R1_R2_k(rza_integrand_params.R_domain_1,
                                rza_integrand_params.R_domain_2,
                                k_sum_mod);
        break;
      }; /* switch(rza_integrand_params.w_type) */


    /* third factor of BKS00 (C18); small k_mod_1, k_mod_2 checked above */
    third_factor = flat_dot_product_3(k_array,1,2) / (k_mod_1 * k_mod_2);
    third_factor = 1.0 - third_factor*third_factor;
    third_factor *= third_factor;

    /* power spectrum: both physical units and initial epoch are needed */
    the_integrand =  power_spectrum_flatspace
      (k_mod_1,
       rza_integrand_params.background_cosm_params.inhomog_a_scale_factor_initial,
       rza_integrand_params.pow_spec_type,
       rza_integrand_params.background_cosm_params
       ) *
      power_spectrum_flatspace
      (k_mod_2,
       rza_integrand_params.background_cosm_params.inhomog_a_scale_factor_initial,
       rza_integrand_params.pow_spec_type,
       rza_integrand_params.background_cosm_params
       ) *
      window * window *
      third_factor ;

#ifdef DEBUG
  printf("sigma_sq_invariant_I_integrand: window, the_integrand = %g\n",
         window,the_integrand);
#endif

  };

  return the_integrand;
}

/*! \brief Calculates \f$ \sigma_{II}^2 (R) \f$  and its error.
 *
 * Uses Monte Carlo alogrithm through GSL libraries to find the value of
 * the first invariant together with its error. It's possible to use
 * 'plain' version of the algorithm, but the VEGAS method is used by
 * default to reduce errors.
 *
 * If \a want_verbose is specified, prints out the domain radius R
 * together with the results (integral value and its error).
 *
 * \param [in] rza_integrand_params_s structure containing parameters
 * necessary for the ODE integration
 * \param [in] n_calls number of times to evaluate the function
 * \param [in] want_verbose control parameter; defined in
 * biscale_partition.c
 * \param [out] the_integral pointer to the result
 * \param [out] integ_error pointer to the result error
 */
int sigma_sq_invariant_II(/* INPUTS; */
                          struct rza_integrand_params_s rza_integrand_params,
                          long    n_calls,  /* number of times to evaluate the function */
                          int want_verbose,
                          double *the_integral, /* OUTPUTS: */
                          double *integ_error
                          )
{
  const gsl_rng_type * T_gsl;
  gsl_rng * r_gsl;

  static unsigned long int local_gsl_seed=0;

  /*  gsl_monte_plain_state * working_space; */
  gsl_monte_vegas_state * working_space;
#define INHOMOG_VEGAS_SMALL ((size_t)10000)
#define INHOMOG_TRY_VEGAS_MAX 10
  int i_try_vegas=0;

  /* a meta-function as per  gsl_monte_function_struct  in gsl_monte.h */
  gsl_monte_function our_meta_function;

  double x_lower[2*DIM_BKS00];
  double x_upper[2*DIM_BKS00];
  int i;

  gsl_rng_env_setup();
  T_gsl = gsl_rng_default;
  gsl_rng_default_seed += 3847 + local_gsl_seed;
  local_gsl_seed = gsl_rng_default_seed;
  r_gsl = gsl_rng_alloc (T_gsl); /* this gets reallocated each time the
                                    function gets called */

  /* meta set up our function (defined in detail above) */
  our_meta_function.f = &sigma_sq_invariant_II_integrand;
  our_meta_function.dim = 2*DIM_BKS00;
  our_meta_function.params = &rza_integrand_params;

  /* gsl_monte_plain_state * gsl_monte_plain_alloc (size_t DIM) */
  /*  working_space =  gsl_monte_plain_alloc (DIM_BKS00); */
  working_space =  gsl_monte_vegas_alloc (2*DIM_BKS00);

  /*  int gsl_monte_plain_integrate (gsl_monte_function * F,
      const double XL[], const double XU[], size_t DIM, size_t
      CALLS, gsl_rng * R, gsl_monte_plain_state * S, double *
      RESULT, double * ABSERR);

      VEGAS is a more intelligent algorithm.
  */

  /* define the cartesian region of integration */
  for (i=0; (unsigned)i<2*DIM_BKS00; i++){
    x_lower[i] = K_LOWER_BKS00;
    x_upper[i] = K_UPPER_BKS00;
  };

  /*
  gsl_monte_plain_integrate ( &our_meta_function,
                              x_lower,
                              x_upper,
                              (2*DIM_BKS00),
                              n_calls,
                              r_gsl,
                              working_space,
                              the_integral,
                              integ_error
                              );
  */

  /* warm up: */
  gsl_monte_vegas_integrate ( &our_meta_function,
                              x_lower,
                              x_upper,
                              (2*DIM_BKS00),
                              INHOMOG_VEGAS_SMALL,
                              r_gsl,
                              working_space,
                              the_integral,
                              integ_error
                              );

  /* try 5-tuple repetitions at least a third of the maximum n tries,
     and keep going if the accuracy is still low, but not infinitely */
  while(i_try_vegas < INHOMOG_TRY_VEGAS_MAX &&
        (i_try_vegas < INHOMOG_TRY_VEGAS_MAX/3 ||
         fabs(gsl_monte_vegas_chisq(working_space) -1.0) > 0.5) ) {
    gsl_monte_vegas_integrate ( &our_meta_function,
                                x_lower,
                                x_upper,
                                (2*DIM_BKS00),
                                (size_t)((long)(n_calls/5)),
                                r_gsl,
                                working_space,
                                the_integral,  /* already a pointer */
                                integ_error    /* already a pointer */
                                );
    i_try_vegas++;  /* don't risk an unending bad run at VEGAS */
    /* printf("debug vegas: i_try_VEGAS = %d\n",i_try_vegas); */
  };

  *the_integral *= 32.0 *exp(6.0*log(M_PI));
  *integ_error *= 32.0 *exp(6.0*log(M_PI));

  if(want_verbose){
    printf("sigma_square_invariant_I: R_domain");
      switch(rza_integrand_params.w_type)
      {
      case 1:
      default:
        printf("= %g, the_integral = %g, err = %g\n",
               rza_integrand_params.R_domain, *the_integral, *integ_error);
        break;

      case 2:
        printf("= %g, the_integral = %g, err = %g\n",
               rza_integrand_params.R_domain_1, *the_integral, *integ_error);
        break;
      };
  };

  gsl_rng_free(r_gsl);
  /*  gsl_monte_plain_free (working_space); */
  gsl_monte_vegas_free (working_space);

  return 0;
}
