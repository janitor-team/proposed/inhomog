/*
   power_spectrum_flatspace - power spectrum P(k) in FLRW flat models

   Copyright (C) 2013 Boud Roukema, Jan Ostrowski

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html

*/

/*! \file power_spectrum_flatspace.c */

#include <stdio.h>
#include <sys/types.h>
#include "config.h"
#include <math.h>
#include <gsl/gsl_sf_bessel.h>

#include "lib/inhomog.h"
#include "lib/power_spectrum_flatspace.h"

/* static variables don't make sense in these functions when
   called by multiple threads; thread-local static variables
   would have to be created */
#ifdef _OPENMP
#define POWER_SPECTRUM_STATIC
#else
#define POWER_SPECTRUM_STATIC static
#endif

/* BKS00 (C5) */
/*! \brief Calculates \f$ \widetilde{W}_{R} \f$
 *
 * Calculates the value of the Fourier transform of the window function
 * \f$ \widetilde{W}_{R} \f$ according to (C5) of Buchert et al. 2000
 * \latexonly (\href{https://arxiv.org/abs/astro-ph/9912347}
 * {arXiv:astro-ph \textbackslash 9912347v2}) \endlatexonly .
 *
 * If either \a k_mod or \a R_domain are less than \b TOL_LENGTH (see
 * inhomog.h for the tolerance parameters values), sets the window to 0.
 *
 * \param [in] R_domain radius of the considered spherical domain
 * \param [in] k_mod sine/cosine function number
 */
double window_R_k(double R_domain,    /* INPUTS: */
                  double k_mod
                  ){
  double window;
  POWER_SPECTRUM_STATIC double R_domain_local = -9e9;
  POWER_SPECTRUM_STATIC double B_R, B_R_factor = 0.0;
  double k_R;

  if(k_mod < TOL_LENGTH || R_domain < TOL_LENGTH){
    window = 0.0;
  }else{
    /* Some things are enough to be calculated once for a given
       domain size R_domain; so variables for these are static -
       remembered between each call to the function. */
    if(R_domain_local != R_domain){
      R_domain_local = R_domain;
      B_R = 4.0/3.0 *M_PI *R_domain *R_domain *R_domain;
      B_R_factor = 1.0/( B_R *2.0 *M_PI *M_PI );
      /* TODO: tolerance test for zero B_R */
    };

    k_R = k_mod*R_domain_local;

    window = B_R_factor /(k_mod*k_mod*k_mod) /* positiveness checked above */
      *(sin(k_R) - k_R * cos(k_R));
  };
  return window;
}


double window_R1_R2_k(double R_domain_1,    /* INPUTS: */
                      double R_domain_2,
                      double k_mod
                      ){

  double window;
  POWER_SPECTRUM_STATIC double R_domain_1_local = -9e9;
  POWER_SPECTRUM_STATIC double R_domain_2_local = -9e9;
  POWER_SPECTRUM_STATIC double R1_3, R2_3;               /* volumes */
  POWER_SPECTRUM_STATIC double B_R1_R2, B_R1_R2_factor = 0.0;
  double k_R1, k_R2;

  if(k_mod < TOL_LENGTH ||
     R_domain_1 < TOL_LENGTH || R_domain_2 < TOL_LENGTH ||
     R_domain_2 - R_domain_1 < TOL_LENGTH
     ){
    window = 0.0;
  }else{
    /* Some things are enough to be calculated once for a given
       domain size R_domain; so variables for these are static -
       remembered between each call to the function. */
    if(R_domain_1_local != R_domain_1 || R_domain_2_local != R_domain_2 ){
      R_domain_1_local = R_domain_1;
      R_domain_2_local = R_domain_2;

      R1_3 = R_domain_1 * R_domain_1 *R_domain_1;
      R2_3 = R_domain_2 * R_domain_2 *R_domain_2;

      B_R1_R2 = 4.0/3.0 *M_PI * (R2_3 - R1_3);
      B_R1_R2_factor = 1.0/( B_R1_R2 *2.0 *M_PI *M_PI );
    };

    k_R1 = k_mod*R_domain_1_local;
    k_R2 = k_mod*R_domain_2_local;

    window = B_R1_R2_factor /(k_mod*k_mod*k_mod) /* positiveness checked above */
      *( (sin(k_R2) - k_R2 * cos(k_R2)) -
         (sin(k_R1) - k_R1 * cos(k_R1))
         );
  };
  return window;
}

/* front end */
/*! \brief Calculates the power spectrum distribution for a given
 * domain.
 *
 * The maximal ratio of initial scale factor to its current value is
 * controlled by \b INHOMOG_PK_BKS_SCALE_FACTOR_RATIO value
 * (power_spectrum_flatspace.h). If it's too big, prints out an error
 * with the values that produced it and stops the calculations.
 *
 * Calculates power spectrum depending on the chosen type. Currently
 * \latexonly (\today) \endlatexonly the possible options are:
 * - e - \ref power_spectrum_flatspace_EisHu_short (default, if the type
 * is not specified)
 * - E - \ref power_spectrum_flatspace_EisHu_full
 * - B - \ref power_spectrum_flatspace_BBKS
 *
 * Returns the power spectrum \a Pofk.
 *
 * \param [in] k_mod sine/cosine function number
 * \param [in] a_scale_factor_initial the initial scale factor value
 * \param [in] pow_spec_type determines how the power spectrum should
 * be calculated
 * \param [in] background_cosm_params a structure of
 * background_cosm_params_s type
 */
double power_spectrum_flatspace(double k_mod, /* INPUTS */
                                double a_scale_factor_initial,
                                char   pow_spec_type,
                                struct background_cosm_params_s background_cosm_params
                                ){
  double Pofk;

  /* Check consistency of scale factor convention
     and Pofk normalisation; otherwise exit. */
  if(fabs((background_cosm_params.inhomog_a_scale_factor_now /
           background_cosm_params.inhomog_a_scale_factor_initial - INHOMOG_PK_BKS_SCALE_FACTOR_RATIO)/
          INHOMOG_PK_BKS_SCALE_FACTOR_RATIO
          ) > 0.01){
    printf("power_spectrum_flatspace ERROR: inconsistent ");
    printf("conventions: %g / %g != %g.\n",
           background_cosm_params.inhomog_a_scale_factor_now,
           background_cosm_params.inhomog_a_scale_factor_initial,
           INHOMOG_PK_BKS_SCALE_FACTOR_RATIO);
    exit(1);
  };

  switch(pow_spec_type)
    {
    case 'B':
      Pofk = power_spectrum_flatspace_BBKS
        (k_mod, /* INPUTS */
         a_scale_factor_initial,
         background_cosm_params);
      break;

    case 'e':
      Pofk = power_spectrum_flatspace_EisHu_short
        (k_mod, /* INPUTS */
         a_scale_factor_initial,
         background_cosm_params);
      break;

    case 'E':
      Pofk = power_spectrum_flatspace_EisHu_full
        (k_mod, /* INPUTS */
         a_scale_factor_initial,
         background_cosm_params);
      break;

    default :
      printf("power_spectrum_flatspace: Warning: invalid power spectrum type %c. Will default to 'e' = EisHu_short.\n",
             pow_spec_type);
      Pofk = power_spectrum_flatspace_EisHu_short
        (k_mod, /* INPUTS */
         a_scale_factor_initial,
         background_cosm_params);
    };
  return Pofk;
}

/* "hardwired" version as in BKS00 (C23), (C24) */
/*! \brief Calculates the power spectrum.
 *
 * Uses formulas (C23), (C24) and (C25) from Buchert et al. 2000
 * \latexonly (\href{https://arxiv.org/abs/astro-ph/9912347}
 * {arXiv:astro-ph \textbackslash 9912347v2}) \endlatexonly .
 *
 * Uses constants \b INHOMOG_TK_BBKS_BKS_T (t), \b INHOMOG_TK_BBKS_BKS_U
 * (u) and \b INHOMOG_TK_BBKS_BKS_V (v) defined in
 * power_spectrum_flatspace.h.
 *
 * If the normalization factor for the power spectrum is known
 * ( \a correct_Pk_norm_known = 1 in background_cosm_params_s), the
 * function automatically corrects the result.
 *
 * \param [in] k_mod sine/cosine function number
 * \param [in] a_scale_factor_initial the initial scale factor value
 * \param [in] background_cosm_params a structure of
 * background_cosm_params_s type
 */
double power_spectrum_flatspace_BBKS(double k_mod, /* INPUTS */
                                     double a_scale_factor_initial,
                                     struct background_cosm_params_s background_cosm_params
                                     ){


  /*  return 1.0; */ /* to start with :P */

  double k_mod_t0; /* scaled to epoch t_0 (missing from (C25)) */
  double T_k_t0;
  double work1;
  double t_term,u_term,v_term;
  double P_of_k;

  /* local copies to (slightly) reduce calculation time if possible */
  POWER_SPECTRUM_STATIC double a_scale_factor_initial_local=0.0;
  POWER_SPECTRUM_STATIC double A_all_factors=0.0;

  /* BKS00 (C25) should obviously read:

       P_i(k/a_1, t_1) = P(k/a_0, t_0) * (a_1/a_0)^2,

     BKS00 set a_1=1, a_0=201.
     Here we set a_1= (e.g.) 1/201,  a_0 = 1, per the common convention.
     Thus,

       P_i(k, t_1) = P(k*a_1/1, t_0) * (a_1/1)^2
                   = P(k*a_1, t_0) * a_1^2 .

  */

  k_mod_t0 = k_mod *(a_scale_factor_initial/background_cosm_params.inhomog_a_scale_factor_now);

  T_k_t0 = log(1.0 + ((double)INHOMOG_TK_BBKS_BKS_R) * k_mod_t0) /
    ( (double)INHOMOG_TK_BBKS_BKS_R * k_mod_t0 );

  t_term = ((double)INHOMOG_TK_BBKS_BKS_T) * k_mod_t0;
  u_term = ((double)INHOMOG_TK_BBKS_BKS_U) * k_mod_t0;
  v_term = ((double)INHOMOG_TK_BBKS_BKS_V) * k_mod_t0;
  v_term *= v_term;
  work1 = 1.0 +
    (double)INHOMOG_TK_BBKS_BKS_S * k_mod_t0 +
    t_term* t_term +
    u_term* u_term* u_term +
    v_term * v_term;  /* v_term already squared once */

  if(work1 < TOL_TK_FACTOR || fabs(k_mod_t0) < TOL_LENGTH_SQUARED ){
    P_of_k= 0.0; /* set to zero if factor too close to negative */
  }else{
    /* Warning: error introduced by proofreaders in official PRD
       version of BKS00 for (C24) - the minus sign was removed. */
    T_k_t0 *= exp(-0.25*log(work1)); /* multiply by work1^{-1/4} */

    if(a_scale_factor_initial_local != a_scale_factor_initial){
      a_scale_factor_initial_local = a_scale_factor_initial;

      /* BKS00 (C25) coefficient, modified by a_i^3 to match
         Fig. 13 */
      A_all_factors= ( (double)INHOMOG_PK_BKS_AMPLITUDE_RAW *
                       exp(3.0*log
                           (background_cosm_params.inhomog_a_scale_factor_initial/
                            background_cosm_params.inhomog_a_scale_factor_now)))*
        (a_scale_factor_initial*a_scale_factor_initial)/
        (background_cosm_params.inhomog_a_scale_factor_now*background_cosm_params.inhomog_a_scale_factor_now);

      if(1==background_cosm_params.correct_Pk_norm_known){
        A_all_factors *= background_cosm_params.correct_Pk_norm;
      };

    };
    P_of_k = A_all_factors *
      exp((double)INHOMOG_PK_N*log(k_mod_t0)) * T_k_t0 * T_k_t0;
  }; /* ! if(work1 < TOL_TK_DENOMINATOR) */
  return P_of_k;
}


/* P(k) from (26), (28)--(31), Eisenstein & Hu 1998 = EH98
   http://cdsads.u-strasbg.fr/abs/1998ApJ...496..605E
*/
/*! \brief Calculates the power spectrum.
 *
 * Uses formulas (26) and (28) to (31) from Eisenstein & Hu, 1998
 * \latexonly (\href{http://cdsads.u-strasbg.fr/abs/1998ApJ...496..605E}
 * {1998ApJ...496..605E}) \endlatexonly .
 *
 * If the normalization factor for the power spectrum is known
 * ( \a correct_Pk_norm_known = 1 in background_cosm_params_s), the
 * function automatically corrects the result.
 *
 * \param [in] k_mod sine/cosine function number
 * \param [in] a_scale_factor_initial the initial scale factor value
 * \param [in] background_cosm_params a structure of
 * background_cosm_params_s type
 */
double power_spectrum_flatspace_EisHu_short
(/* INPUTS */
 double k_mod,  /* comoving Mpc, no "h^-1" */
 double a_scale_factor_initial,
 struct background_cosm_params_s background_cosm_params
 ){
  double k_mod_t0;
  double T_k_t0;
  double L0_Tk,C0_Tk; /* EH98 (29) working variables */
  double A_all_factors;
  double P_of_k;

  double s_EH98; /* sound horizon at drag epoch EH98 (26) */
  double q_EH98; /* k scaled by k at equality epoch EH98 (28) */
  double alpha_Gamma; /* EH98 (30) */
  double Gamma_eff;   /* EH98 (31) */

  k_mod_t0 = k_mod *(a_scale_factor_initial/background_cosm_params.inhomog_a_scale_factor_now);

  if(fabs(k_mod_t0) < TOL_LENGTH_SQUARED ){
    P_of_k= 0.0; /* set to zero if factor too close to negative */
    return P_of_k;
  };


  s_EH98 = 44.5 * log
    (9.83 / (background_cosm_params.Omm_0 *
             background_cosm_params.H_0 *
             background_cosm_params.H_0 * 1e-4)) /
    sqrt( 1.0 + 10.0 * exp
          (0.75* log
           (background_cosm_params.Ombary_0 *
            background_cosm_params.H_0 *
            background_cosm_params.H_0 * 1e-4)) );

  alpha_Gamma =
    1.0 - 0.328 * log
    (431.0 * background_cosm_params.Omm_0 *
             background_cosm_params.H_0 *
     background_cosm_params.H_0 * 1e-4) *
    background_cosm_params.Ombary_0 /
    background_cosm_params.Omm_0 +
    0.38 * log
    (22.3 * background_cosm_params.Omm_0 *
             background_cosm_params.H_0 *
     background_cosm_params.H_0 * 1e-4) *
    background_cosm_params.Ombary_0 *
    background_cosm_params.Ombary_0
    / (background_cosm_params.Omm_0  *
       background_cosm_params.Omm_0);

  Gamma_eff = background_cosm_params.Omm_0 *
             background_cosm_params.H_0 * 0.01 *
    (alpha_Gamma +
     (1.0 - alpha_Gamma) /
     (1.0 +
      exp(4.0* log(0.43 * k_mod_t0 * s_EH98)))
     );

  q_EH98 = k_mod_t0 /
    (background_cosm_params.H_0 * 0.01) *
    background_cosm_params.Theta2p7 *
    background_cosm_params.Theta2p7 /
    Gamma_eff;

  L0_Tk = log(2*M_E + 1.8 * q_EH98);

  C0_Tk = 14.2 + 731.0/(1.0 + 62.5 * q_EH98);

  T_k_t0 = L0_Tk / (L0_Tk + C0_Tk * q_EH98 * q_EH98);

  /* TODO: check sigma_8 normalisation */
  /* BKS00 (C25) coefficient, modified by a_i^3 to match
     Fig. 13 */
  A_all_factors= ( (double)INHOMOG_PK_BKS_AMPLITUDE_RAW *
                   exp(3.0*log
                       (background_cosm_params.inhomog_a_scale_factor_initial/
                        background_cosm_params.inhomog_a_scale_factor_now)))*
    (a_scale_factor_initial*a_scale_factor_initial)/
    (background_cosm_params.inhomog_a_scale_factor_now*background_cosm_params.inhomog_a_scale_factor_now);

  if(1==background_cosm_params.correct_Pk_norm_known){
    A_all_factors *= background_cosm_params.correct_Pk_norm;
  };

  P_of_k = A_all_factors *
    exp((double)INHOMOG_PK_N*log(k_mod_t0)) * T_k_t0 * T_k_t0;

  return P_of_k;
}


/*
   the GCC compile option -finline-functions (included in -O3)
   should inline the following function
 */

double Transfer0tilde_EisHu(/* double k_mod_t0, -> passed via q_EH98 */
                     double alpha_c,
                     double beta_c,
                     double q_EH98);

double Transfer0tilde_EisHu(/* double k_mod_t0, -> passed via q_EH98 */
                     double alpha_c,
                     double beta_c,
                     double q_EH98){
  double C;
  double Transfer0tilde;

  C = 14.2/alpha_c +
    386.0/ (1.0 + 69.9 * exp(1.08 *log(q_EH98)));

  Transfer0tilde = log(M_E + 1.8 *beta_c *q_EH98) /
    ( log(M_E + 1.8 *beta_c *q_EH98) + C *q_EH98 *q_EH98 );

  return Transfer0tilde;
}

/*! \brief Calculates the power spectrum.
 *
 * Longer version of \ref power_spectrum_flatspace_EisHu_short.
 * Uses formulas and methods from Eisenstein & Hu, 1998
 * \latexonly (\href{http://cdsads.u-strasbg.fr/abs/1998ApJ...496..605E}
 * {1998ApJ...496..605E}) \endlatexonly .
 *
 * If the normalization factor for the power spectrum is known
 * ( \a correct_Pk_norm_known = 1 in background_cosm_params_s), the
 * function automatically corrects the result.
 *
 * \param [in] k_mod sine/cosine function number
 * \param [in] a_scale_factor_initial the initial scale factor value
 * \param [in] background_cosm_params a structure of
 * background_cosm_params_s type
 */
double power_spectrum_flatspace_EisHu_full
(double k_mod, /* INPUTS */
 double a_scale_factor_initial,
 struct background_cosm_params_s background_cosm_params
 ){

  double k_mod_t0;
  double T_k_t0;
  double A_all_factors;
  double P_of_k;

  /* used almost everywhere */
  double Omm0_hsq; /* Omega_{m0} h^2 */
  double Ombary_hsq; /* Omega_{b0} h^2 */
  double Omb_frac, Omc_frac; /* Omega_{b0}/Omega_{m0}, 1 - that */

  /* eqs (2)-(7) */
  double z_eq, k_eq, z_drag;
  double b1_drag, b2_drag;
  double R_eq, R_drag; /* baryon to photon momentum density ratio */
  double s_EH98; /* sound horizon at drag epoch EH98 (26) */
  double k_Silk;

  /* eqs (10)-(12) */
  double q_EH98; /* k scaled by k at equality epoch EH98 (28) */
  double a1c,a2c, b1c,b2c; /* auxiliaries for CDM part */
  double alpha_c, beta_c;

  /* (14)-(24) */
  double yEq15, GEq15;
  double alpha_b, beta_b;
  double fEq18;
  double Transfer_b;
  double k_times_s; /* used in (22), (21) */
  double beta_node, s_tilde;
  double Transfer_c;


  k_mod_t0 = k_mod *(a_scale_factor_initial/background_cosm_params.inhomog_a_scale_factor_now);

  if(fabs(k_mod_t0) < TOL_LENGTH_SQUARED ){
    P_of_k= 0.0; /* set to zero if factor too close to negative */
    return P_of_k;
  };


  /* used almost everywhere */
  Omm0_hsq =  background_cosm_params.Omm_0 *
    background_cosm_params.H_0 *
    background_cosm_params.H_0 * 1e-4;
  Ombary_hsq =  background_cosm_params.Ombary_0 *
    background_cosm_params.H_0 *
    background_cosm_params.H_0 * 1e-4;

  if(background_cosm_params.Omm_0 > TOL_PK_EISHU){
    Omb_frac = background_cosm_params.Ombary_0/
      background_cosm_params.Omm_0;
  }else{
    Omb_frac = 0.0;
  };
  Omc_frac = 1.0 - Omb_frac;

  /* eqs (2)-(7) */
  z_eq = 2.5e4 * Omm0_hsq *
    exp(-4.0*log(background_cosm_params.Theta2p7));

  k_eq = 7.46e-2 * Omm0_hsq *
    exp(-2.0*log(background_cosm_params.Theta2p7));

  b1_drag = 0.313 *  exp(-0.419*log(Omm0_hsq)) *
    (1.0 + 0.607 *  exp(0.674*log(Omm0_hsq)));
  b2_drag = 0.238 *  exp(0.223*log(Omm0_hsq));

  z_drag = 1291.0 *
    exp(0.251*log(Omm0_hsq)) /
    fmax(TOL_PK_EISHU, (1.0 + 0.659 * exp(0.828*log(Omm0_hsq)))) *
    (1.0 + b1_drag *
     exp(b2_drag * log(Ombary_hsq))
     );

  R_eq = 31.5 * Ombary_hsq
    *exp(-4.0*log(background_cosm_params.Theta2p7)) /
    fmax(TOL_PK_EISHU, (0.001*z_eq));
  R_drag = 31.5 * Ombary_hsq
    *exp(-4.0*log(background_cosm_params.Theta2p7)) /
    fmax(TOL_PK_EISHU, (0.001*z_drag));

  s_EH98 = 2.0/fmax(TOL_PK_EISHU, (3.0*k_eq)) *sqrt(fmax(TOL_PK_EISHU, 6.0/R_eq)) *
    log
    ( (sqrt(1.0+R_drag) + sqrt(fmax(TOL_PK_EISHU, R_drag + R_eq)))/
      (1.0 + sqrt(R_eq))
      );
  /*
    printf("power_spectrum_flatspace_EisHu_full: s_EH98 = %g\n",
         s_EH98);
  */

  k_Silk = 1.6 * exp(0.52*log(Ombary_hsq)) *
    exp(0.73 * log(Omm0_hsq)) *
    ( 1.0 + exp(-0.95 * log(10.4 * Omm0_hsq)) );


  /* eqs (10)-(12) */
  q_EH98 = k_mod_t0 / fmax(TOL_PK_EISHU, (13.41 * k_eq));

  a1c = exp(0.670* log(46.9*Omm0_hsq)) *
    (1.0 + exp(-0.532* log(32.1*Omm0_hsq))
     );
  a2c = exp(0.424* log(12.0*Omm0_hsq)) *
    (1.0 + exp(-0.582* log(45.0*Omm0_hsq))
     );

  alpha_c = exp(- (Omb_frac) *log(a1c)) *
    exp( - exp(3.0*log(Omb_frac)) *log(a2c) );

  b1c = 0.944 / (1.0 + exp(-0.708* log(458.0*Omm0_hsq)));
  b2c = exp(-0.0266* log(0.395*Omm0_hsq));

  beta_c = 1.0 / fmax(TOL_PK_EISHU,
                      (1.0 + b1c * ( exp(b2c*log(Omc_frac)) -1.0 )));


  /* (14)-(24) */
  yEq15 = (1.0 + z_eq)/fmax(TOL_PK_EISHU, (1.0 + z_drag));
  GEq15 = yEq15 *
    (-6.0 * sqrt(fmax(TOL_PK_EISHU, 1.0 + yEq15)) +
     (2.0 + 3.0*yEq15) *
     log( (sqrt(1.0+yEq15) + 1.0)/
          fmax(TOL_PK_EISHU, (sqrt(1.0+yEq15) - 1.0) ))
     );
  alpha_b = 2.07 * k_eq * s_EH98 *
    exp(-0.75 * log(1.0 + R_drag)) * GEq15;


  fEq18 = 1.0 /
    ( 1.0 + exp(4.0* log(k_mod_t0 * s_EH98 /5.4)) );  /* (18) */

  /* (20) */
  Transfer_c = fEq18 *
    Transfer0tilde_EisHu( /* k_mod_t0, */
                         1.0,
                         beta_c,
                         q_EH98) +
    (1.0 - fEq18) *
    Transfer0tilde_EisHu( /* k_mod_t0, */
                         alpha_c,
                         beta_c,
                         q_EH98);

  beta_b = 0.5 + Omb_frac + (3.0 - 2.0*Omb_frac) *
    sqrt( exp(2.0*log(17.2 * Omm0_hsq)) + 1.0 );  /* (24) */

  beta_node = 8.41 * exp(0.435 *
                         log(fmax(TOL_PK_EISHU, Omm0_hsq)));   /* (23) */

  k_times_s = k_mod_t0*s_EH98;

  s_tilde = s_EH98 /
    exp(1.0/3.0 *log( 1.0 +
                      exp(3.0*log(fmax(TOL_PK_EISHU,
                                       beta_node/k_times_s))) ));

  Transfer_b =
    (Transfer0tilde_EisHu( /* k_mod_t0, */
                          1.0, 1.0, q_EH98) /
     (1.0 +  k_times_s/5.2 * k_times_s/5.2 ) +
     alpha_b / (1.0 + exp(3.0*log(fmax(TOL_PK_EISHU, beta_b/k_times_s)))) *
     exp(fmax(TOL_MIN_LN_EISHU,
              - (exp(1.4 *log(fmax(TOL_PK_EISHU, k_mod_t0/k_Silk)))) ))
     ) *
    gsl_sf_bessel_j0( k_mod_t0 * s_tilde ); /* (21) */


  /* 16 */
  T_k_t0 = Omb_frac * Transfer_b + Omc_frac * Transfer_c;

  /* TODO: check sigma_8 normalisation */
  /* BKS00 (C25) coefficient, modified by a_i^3 to match
     Fig. 13 */
  A_all_factors= ( (double)INHOMOG_PK_BKS_AMPLITUDE_RAW *
                   exp(3.0*log
                       (background_cosm_params.inhomog_a_scale_factor_initial/
                        background_cosm_params.inhomog_a_scale_factor_now)))*
    (a_scale_factor_initial*a_scale_factor_initial)/
    (background_cosm_params.inhomog_a_scale_factor_now*background_cosm_params.inhomog_a_scale_factor_now);

  if(1==background_cosm_params.correct_Pk_norm_known){
    A_all_factors *= background_cosm_params.correct_Pk_norm;
  };

  P_of_k = A_all_factors *
    exp((double)INHOMOG_PK_N*log(k_mod_t0)) * T_k_t0 * T_k_t0;

  return P_of_k;
}
