/*
   alloc_big_array - allocate or free big multi-dimensional arrays

   Copyright (C) 2014 Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html
*/

#ifndef _ALLOC_BIG_ARRAY_H
#define _ALLOC_BIG_ARRAY_H 1

#include <stdint.h>

int report_out_of_mem(uint64_t available,
                      uint64_t needed,
                      const char  alloc_type[5]);

int alloc_big_array_1D_d(uint64_t p,
                         double ** array_1D,
                         int initialise_to_zero);

int free_big_array_1D_d(double * array_1D);

int alloc_big_array_2D_d(uint64_t n,
                         uint64_t p,
                         double *** array_2D,
                         int initialise_to_zero);

int free_big_array_2D_d(uint64_t n,
                        double ** array_2D);

int alloc_big_array_3D_d(uint64_t m,
                         uint64_t n,
                         uint64_t p,
                         double **** array_3D,
                         int initialise_to_zero);

int free_big_array_3D_d(uint64_t m,
                        uint64_t n,
                        double *** array_3D);

int alloc_big_array_4D_d(uint64_t m,
                         uint64_t n,
                         uint64_t p,
                         uint64_t q,
                         double ***** array_4D,
                         int initialise_to_zero);

int free_big_array_4D_d(uint64_t m,
                        uint64_t n,
                        uint64_t p,
                        double **** array_4D);

int alloc_big_array_2D_i(uint64_t n,
                         uint64_t p,
                         int *** array_2D,
                         int initialise_to_zero);

int free_big_array_2D_i(uint64_t n,
                        int ** array_2D);

int alloc_big_array_3D_i(uint64_t m,
                         uint64_t n,
                         uint64_t p,
                         int **** array_3D,
                         int initialise_to_zero);

int free_big_array_3D_i(uint64_t m,
                        uint64_t n,
                        int *** array_3D);

int alloc_big_array_4D_i(uint64_t m,
                         uint64_t n,
                         uint64_t p,
                         uint64_t q,
                         int ***** array_4D,
                         int initialise_to_zero);

int free_big_array_4D_i(uint64_t m,
                        uint64_t n,
                        uint64_t p,
                        int **** array_4D);

#endif /* #ifndef HAVE_INCLUDED_ALLOC_BIG_ARRAY */
