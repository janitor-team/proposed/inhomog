/*
   kinematic backreaction - (50) of Buchert et al RZA2 arXiv:1303.6193v2

   Copyright (C) 2013 Boud Roukema, Jan Ostrowski

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html

*/

/*! \file kinematical_backreaction.c */

#include <stdio.h>
#include <sys/types.h>
#include "config.h"
#include <math.h>
#include <gsl/gsl_rng.h>

/* for malloc_usable_size if available */
#ifdef __GNUC__
#include <malloc.h>
#endif

#include "lib/inhomog.h"

/* static variables don't make sense in these functions when
   called by multiple threads; thread-local static variables
   would have to be created */
#ifdef _OPENMP
#define BACKREACTION_STATIC
#else
#define BACKREACTION_STATIC static
#endif

#define DEBUG 1

/* #undef DEBUG */



/* See (50) of Buchert et al RZA2 arXiv:1303.6193v2 for the formula used
   here. */
/*! \brief Calculates kinematic backreaction \f$ Q_D \f$.
 *
 * Initial time depends on the chosen model. The choice is controlled by
 * the \a t_EdS and \a t_flatFLRW parameters (for a more detailed
 * description see biscale_partition.c).
 *
 * \f$ q(t) \f$, the initial growth function which appears during
 * \f$ \xi \f$ calculation), is calculated according to the \ref
 * growth_FLRW function (for a more detailed description see
 * growth_function.c).
 *
 * If the I, II and III invariants are not calculated (i.e. \a
 * precalculated_invariants.enabled in rza_integrand_params_s is set to
 * 0) calculates them before the final calculation.
 *
 * \f$ Q_D \f$ is calculated according to equation (50) (Buchert et al.
 * 2013; \latexonly \href{https://arxiv.org/abs/1303.6193}{arXiv:
 * 1303.6193} \endlatexonly ) :
 *
 * \f$ ^{RZA}Q_{D} = \frac{\dot{\xi}^2
 * (\gamma_1~+~\xi \gamma_2~+~\xi^2 \gamma_3)}
 * {(1~+~\xi <I_i>_I~+~\xi^2 <II_i>_I~+~\xi^3 <III_i>_I)^2} \f$
 *
 * \b DEBUG macro allows for checking invariant parameters after their
 * calculation, and prints out an eventual error message if \a
 * t_background (time values matrix) has wrong values.
 *
 * To print out values before, during and after calculation,
 * \a want_verbose must be set to 1.
 *
 * \param [in] rza_integrand_params_s pointer to the structure
 * containing parameters necessary for the ODE integration
 * \param [in] background_cosm_params structure containing necessary
 * cosmological parameters (Omega, Hubble constant etc.)
 * \param [in] t_background pointer to the matrix of time values
 * \param [in] n_t_background size of the \a t_background_in matrix
 * \param [in] n_calls_invariants for invariant I integration
 * \param [in] want_planar control parameter; defined in
 * biscale_partition.c
 * \param [in] want_spherical control parameter; defined in
 * biscale_partition.c
 * \param [in] want_verbose control parameter; defined in
 * biscale_partition.c
 * \param [out] rza_Q_D pointer to the RZA kinematical backreaction
 * parameter
 */
int kinematical_backreaction(/* INPUTS: */
                              struct rza_integrand_params_s *rza_integrand_params,
                              struct background_cosm_params_s background_cosm_params,
                              double *t_background,
                              int    n_t_background,
                              double n_sigma[3],
                              long   n_calls_invariants,  /* for invariant I integration */
                              int want_planar, /* cf RZA2 V.A */
                              int want_spherical, /* cf RZA2 V.B.3 */
                              int want_verbose,
                              /* OUTPUTS: */
                              double *rza_Q_D
                              )
{
  double q_growth, q_growth_dot; /* growth parameter and derivative in
                        background (bg) model */
  double t_initial;  /* initial epoch */
  double q_growth_i, q_growth_dot_i;  /* initial epoch growth parameter */
  double xi, xi_dot; /* growth parameter and derivative in background
                        (bg) model */
  double inv_I, inv_II, inv_III; /* velocity gradient tensor mean invariants */
  double inv_I_err, inv_II_err=0.0, inv_III_err=0.0;
  double gamma_1, gamma_2, gamma_3;

  int   i_t; /* iterate over times */
  const int integrator_verbose = 0;

  const double two_thirds = 2.0/3.0;

  double denom;


  /* TODO: set this at a higher level, not here; this will only
     influence sublevels in the scope. Or calculate separately and set
     to zero, to avoid one unnecessary repeated calculation.
  */
  /*
     initial conditions:
  */
  background_cosm_params.recalculate_t_0 = 1;

  if(1==background_cosm_params.EdS){
    t_initial = t_EdS(&background_cosm_params,
                      background_cosm_params.inhomog_a_scale_factor_initial,
                      want_verbose);
  }else if(1==background_cosm_params.flatFLRW &&
           background_cosm_params.Omm_0 < 1.0){
    t_initial = t_flatFLRW(&background_cosm_params,
                           background_cosm_params.inhomog_a_scale_factor_initial,
                           want_verbose);
  }else{
    printf("kinematical_backreaction ERROR: ");
    printf("No other options for background_cosm_params so far in program.\n");
    exit(1);
  };
  if(want_verbose){
    printf("kinematical_backreaction: t_initial = %g\n",
           t_initial);
  };

  q_growth_i = growth_FLRW(&background_cosm_params,
                           t_initial, want_verbose);
  q_growth_dot_i = dot_growth_FLRW(&background_cosm_params,
                                   t_initial, want_verbose);

  if(want_verbose){
    printf("kinematical_backreaction: q_growth_i = %g\n",
           q_growth_i);
    printf("kinematical_backreaction: q_growth_dot_i = %g\n",
           q_growth_dot_i);
  };

  rza_integrand_params->background_cosm_params =
    background_cosm_params; /* needed by the invariant integrators for P(k) */

  /* initial conditions of invariants */
  /* #pragma omp parallel                                       \
     default(shared)                                            \
     firstprivate(rza_integrand_params, n_calls_invariants)
  */
  {
    {

      if(rza_integrand_params->precalculated_invariants.enabled){
        inv_I = rza_integrand_params->precalculated_invariants.inv_I;
        inv_II = rza_integrand_params->precalculated_invariants.inv_II;
        inv_III = rza_integrand_params->precalculated_invariants.inv_III;
      }else /* new calculation of invariants */ {


        if(rza_integrand_params->sigma_sq_inv_triple.I_known){
          /* set the scale of inv_I */
          inv_I = n_sigma[0] *
            rza_integrand_params->sigma_sq_inv_triple.sqrt_E_sigma_sq_I;
        }else{
          sigma_sq_invariant_I( *rza_integrand_params,
                                n_calls_invariants,
                                integrator_verbose,
                                &inv_I, &inv_I_err );
          /* set the scale of inv_I */
          rza_integrand_params->sigma_sq_inv_triple.sqrt_E_sigma_sq_I =
            sqrt(inv_I);
          rza_integrand_params->sigma_sq_inv_triple.I_known = 1;
          inv_I = n_sigma[0] *
            rza_integrand_params->sigma_sq_inv_triple.sqrt_E_sigma_sq_I;
        };


        if(want_planar){
          inv_II = 0.0;
          inv_III = 0.0;
        }else if(want_spherical){
          inv_II = inv_I*inv_I/3.0;
          inv_III = inv_I*inv_I*inv_I/27.0;
        }else /* generic case */ {
          {
            if(fabs(n_sigma[1]) > 0.0){

              if(rza_integrand_params->sigma_sq_inv_triple.II_known){
                /* set the scale of inv_II */
                inv_II = n_sigma[1] *
                  rza_integrand_params->sigma_sq_inv_triple.sqrt_E_sigma_sq_II;
              }else{
                sigma_sq_invariant_II( *rza_integrand_params,
                                       3*n_calls_invariants,
                                       integrator_verbose,
                                       &inv_II, &inv_II_err );

                /* set the scale of inv_II */
                rza_integrand_params->sigma_sq_inv_triple.sqrt_E_sigma_sq_II =
                  sqrt(inv_II);
                rza_integrand_params->sigma_sq_inv_triple.II_known = 1;
                inv_II = n_sigma[1] *
                  rza_integrand_params->sigma_sq_inv_triple.sqrt_E_sigma_sq_II;
              };

            }else{
              inv_II =0.0;
            }
          }

          {
            if(fabs(n_sigma[2]) > 0.0){

              if(rza_integrand_params->sigma_sq_inv_triple.III_known){
                /* set the scale of inv_III */
                inv_III = n_sigma[2] *
                  rza_integrand_params->sigma_sq_inv_triple.sqrt_E_sigma_sq_III;
              }else{
                sigma_sq_invariant_III( *rza_integrand_params,
                                        20*n_calls_invariants,
                                        integrator_verbose,
                                        &inv_III, &inv_III_err );

                /* set the scale of inv_III */
                rza_integrand_params->sigma_sq_inv_triple.sqrt_E_sigma_sq_III =
                  sqrt(inv_III);
                rza_integrand_params->sigma_sq_inv_triple.III_known = 1;
                inv_III = n_sigma[2] *
                  rza_integrand_params->sigma_sq_inv_triple.sqrt_E_sigma_sq_III;
              };

            }else{
              inv_III =0.0;
            };
          };
        }; /* if: planar/spherical/generic/ */
      }; /* if: precalculated vs new calculation */
    }; /* #pragma omp sections */
  }; /* #pragma omp parallel  */

#ifdef DEBUG
  if(want_verbose && 1==rza_integrand_params->w_type){
    printf("kinematical_backreaction.invs(k,M,T)= %g  %g %g  %g %g  %g %g \n",
           rza_integrand_params->R_domain,
           1e3*(inv_I), 1e3* 0.5*inv_I_err/(inv_I),
           1e6*(inv_II), 1e6* 0.5*inv_II_err/(inv_II),
           1e12*(inv_III), 1e12* 0.5*inv_III_err/(inv_III)
           );
  };
#endif

  gamma_1 = 2.0 * inv_II - two_thirds *inv_I*inv_I;
  gamma_2 = 6.0 * inv_III - two_thirds *inv_I*inv_II;
  gamma_3 = 2.0 * inv_I *inv_III - two_thirds *inv_II*inv_II;

  if(!(background_cosm_params.EdS) &&
     !(background_cosm_params.flatFLRW &&
       background_cosm_params.Omm_0 < 1.0)){
    printf("kinematical_backreaction ERROR: ");
    printf("No other options for background_cosm_params so far in program.\n");
    exit(1);
  };

  /* new values */
  for(i_t=0; i_t<n_t_background; i_t++){
#ifdef DEBUG
    if(!(t_background[i_t] > 1e-8 && t_background[i_t] < 100.0) ){
      printf("pre-a_EdS ERROR: i_t, t_background, background_cosm_params.t_0 = %d %g %g\n",
             i_t, t_background[i_t], background_cosm_params.t_0);
    };
#endif
    q_growth =
      growth_FLRW(&background_cosm_params,
                  t_background[i_t], want_verbose);
    q_growth_dot =
      dot_growth_FLRW(&background_cosm_params,
                      t_background[i_t], want_verbose);

    if(want_verbose){
      printf("kinematical_backreaction: q_growth = %g\n",
             q_growth);
      printf("kinematical_backreaction: q_growth_dot = %g\n",
             q_growth_dot);
    };
    /* 28.09.2013: TODO: dimensionless \xi convention; the
       dimensional/dimensionless conventions varies internally
       within BKS00 and RZA2

       2017-07-16: WARNING: RZA2(32) states q_growth_dot_i in the
       denominator of the definition of xi, but that is wrong; the
       denominator should be q_growth_i .
    */
    /* dimensional [T] */
    /*
      xi = (q_growth -q_growth_i) / q_growth_i;
      xi_dot = q_growth_dot / q_growth_i;
    */

    /* dimensionless */

    xi = (q_growth -q_growth_i) / q_growth_i;
    xi_dot = q_growth_dot / q_growth_i;

    if(want_verbose){
      printf("kinematical_backreaction: xi = %g\n",
             xi);
      printf("kinematical_backreaction: xi_dot = %g\n",
             xi_dot);
    };

    denom = 1.0 + xi*inv_I + xi*xi*inv_II + xi*xi*xi*inv_III;
    rza_Q_D[i_t] = xi_dot * xi_dot *
      (gamma_1 + xi*gamma_2 + xi*xi*gamma_3) / (denom*denom);
  }; /* for(i_t=0; i_t<n_t_background; i_t++) */

  return 0;
}
