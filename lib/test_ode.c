/*
   test GSL ODE integrator

   Copyright (C) 2013,2018 Boud Roukema, Jan Ostrowski

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html

*/

#include <stdio.h>
#include <sys/types.h>
#include "config.h"
#include <math.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv.h>
#include <gsl/gsl_sort.h>
#include <gsl/gsl_statistics.h>

/* for malloc_usable_size if available */
#ifdef __GNUC__
#include <malloc.h>
#endif

#include "lib/inhomog.h"
#include "lib/test_ode.h"

/* static variables don't make sense in these functions when
   called by multiple threads; thread-local static variables
   would have to be created */
#ifdef _OPENMP
#define TEST_ODE_STATIC
#else
#define TEST_ODE_STATIC static
#endif

#define DEBUG 1

/* #undef DEBUG */


/* GSL example: solve \ddot{x}(t) + \mu \dot{x}(t) (x(t)^2 - 1) + x(t)
   = 0 */
/* our example:
   solve \ddot{x}(t) = -x + t^2 + 2 + \mu
   exact solution: x = sin(t) + t^2 + \mu
 */
int test_inhomog_ODE_func(double t,
                          const double func_first_order[],
                          double func_first_order_deriv[],
                          void *params){
  double mu = *(double *)params;
  func_first_order_deriv[0] = func_first_order[1];
  /*  func_first_order_deriv[1] = -func_first_order[0] + mu * func_first_order[1] *
      (1.0 - func_first_order[0]*func_first_order[0]); */
  func_first_order_deriv[1] = -func_first_order[0] + t*t + 2.0 + mu;
  return GSL_SUCCESS;
}

int test_inhomog_ODE_jacob(double t,
                           const double func_first_order[],
                           double *dfunc1D_partials,
                           double dfunc1D_dt[],
                           void *params){
  double mu = *(double *)params;
  gsl_matrix_view dfunc1D_partials_mat =
    gsl_matrix_view_array(dfunc1D_partials,2,2);
  gsl_matrix * m = &dfunc1D_partials_mat.matrix;
  /* 0 = \dot{original x}; 1 = \dot{\dot{x}} */
  gsl_matrix_set (m, 0, 0, 0.0); /* \partial\dot{x}/\partial x */
  gsl_matrix_set (m, 0, 1, 1.0); /* \partial\dot{x}/\partial \dot{x} */
  /* \partial\dot{\dot{x}}/\partial x : */
  /* gsl_matrix_set (m, 1, 0, -1.0 - 2.0 *mu *func_first_order[0]*func_first_order[1]);  */
  gsl_matrix_set (m, 1, 0, -1.0);
  /* \partial\dot{\dot{x}}/\partial \dot{x} : */
  /* gsl_matrix_set (m, 1, 1, mu *(1.0 - func_first_order[0]*func_first_order[0]));  */
  gsl_matrix_set (m, 1, 1, 0.0);
  /* \partial\dot{x}/\partial t */
  dfunc1D_dt[0] = 0.0;
  /* \partial\dot{\dot{x}}/\partial t */
  /* dfunc1D_dt[1] = 0.0; */
  dfunc1D_dt[1] = 2.0*t
    + 0.0*(mu + func_first_order[0]); /* follow ISO 90 C standard - unused variables not allowed */
  return GSL_SUCCESS;
}


/* int test_inhomog_ODE(int want_verbose){ */

int main(void){

  int want_verbose = 0;
  int i_integrator;

  /* tolerances for rkf45 test = 1th test; 2013-09-30 */
#define TEST_INTEGRATOR 1
#define TEST_MEDIAN_ERR 1.5e-14
#define TEST_1PERC_ERR 1e-15
#define TEST_99PERC_ERR 1.5e-13
  int pass = 0; /* default test status = OK = 0 */


  double mu = 7.7; /* 10.0; */

  const gsl_odeiv_step_type *T [N_INTEGRATORS];
#define TEST_ODE_N_TIMES 3000
  const int N_t = TEST_ODE_N_TIMES;
  double t_array[TEST_ODE_N_TIMES];
  double x_array[N_INTEGRATORS][TEST_ODE_N_TIMES];
  double t_min=0.0, t_max = 10.0; /* 100.0; */  /* fixed */
  double t; /* varies */
  double dt; /* fixed */
  int i;
  double h;
  double func_first_order[2]; /* IC = initial conditions */
  /*  double check[N_INTEGRATORS][N_t]; */
  double check[TEST_ODE_N_TIMES];

  gsl_odeiv_system sys;

  gsl_odeiv_step *s;
  gsl_odeiv_control *c;
  gsl_odeiv_evolve *e;


  /*
    func_first_order[0] = 1.0;
    func_first_order[1] = 0.0;
  */


  T[0] = gsl_odeiv_step_rk2;
  T[1] = gsl_odeiv_step_rkf45;
  T[2] = gsl_odeiv_step_rk8pd;
  T[3] = gsl_odeiv_step_rk2imp;
  T[4] = gsl_odeiv_step_rk4imp;
  T[5] = gsl_odeiv_step_bsimp;
  T[6] = gsl_odeiv_step_gear2;

  sys.function = test_inhomog_ODE_func;
  sys.jacobian = test_inhomog_ODE_jacob;
  sys.dimension = 2;
  sys.params = &mu;


  dt=(t_max-t_min)/((double)N_t-1.0);
  for(i=0;i<N_t;i++){
    t_array[i] = t_min + (double)i * dt;
  };

  printf("\ntest_ode:\n");
  for(i_integrator=0; i_integrator<N_INTEGRATORS; i_integrator++){

    s = gsl_odeiv_step_alloc(T[i_integrator],2);
    c = gsl_odeiv_control_y_new(0.0,1e-6);;
    e = gsl_odeiv_evolve_alloc(2);

    h = 1e-6;
    func_first_order[0] = mu;
    func_first_order[1] = 1.0;
    printf("GSL ODE step type = integrator: %s\n",
            gsl_odeiv_step_name (s));

    /* main integration */
    t = t_min;
    for (i=0;i<N_t;i++){
      while (t < t_array[i]){
        gsl_odeiv_evolve_apply(e, c, s,
                               &sys,
                               &t, t_array[i], &h,
                               func_first_order);
        x_array[i_integrator][i] = func_first_order[0];
      }
      if(want_verbose) printf("ode: %d %.5e %.5e %.5e %.5e\n",
             i, t_array[i], t,
             func_first_order[0], func_first_order[1]);
    };


    for (i=1;i<N_t-1;i++){
      /* naive rough check */
      /*
      check[i_integrator][i] =
        (x_array[i+1][i_integrator] +
         x_array[i-1][i_integrator] - 2*x_array[i_integrator][i])
        /dt/dt   # \ddot{x}
        + x_array[i_integrator][i]
        - t_array[i]*t_array[i]
        - 2.0 - mu;
      */
        /*
        (x_array[i+1][i_integrator] +
               x_array[i-1][i_integrator] - 2*x_array[i_integrator][i])
               /dt/dt   # \ddot{x}
        + x_array[i_integrator][i]
        - mu * 0.5 *
        (x_array[i+1][i_integrator] - x_array[i-1][i_integrator])/dt # \dot{x}
        * (1.0 - x_array[i_integrator][i] * x_array[i_integrator][i]);
        */

    /* check using known analytical solution */

      check[i] =
        x_array[i_integrator][i]
        - sin(t_array[i]) - t_array[i]*t_array[i]
        - mu;

      /* absolute errors */
      if(want_verbose) printf("ode_check: %.5e %.5e\n",
             t_array[i], fabs(check[i]));

    };

    if(want_verbose) printf("ode_check:\n"); /* useful for graph -TX */

    /* median error - changes data in place */
    for(i=0;i<N_t;i++){
      check[i] = fabs(check[i]);
    };
    gsl_sort(check, 1, (size_t)N_t);
    printf("ode_check_median_1percent_99percent: %.5e %.5e %.5e\n\n",
           gsl_stats_median_from_sorted_data(check, 1, (size_t)N_t),
           gsl_stats_quantile_from_sorted_data(check, 1, (size_t)N_t, 0.01),
           gsl_stats_quantile_from_sorted_data(check, 1, (size_t)N_t, 0.99)
           );
    if(TEST_INTEGRATOR==i_integrator){
      if(gsl_stats_median_from_sorted_data(check, 1, (size_t)N_t) >
         TEST_MEDIAN_ERR) pass = 1;
      if(gsl_stats_quantile_from_sorted_data(check, 1, (size_t)N_t, 0.01) >
         TEST_1PERC_ERR) pass = 2;
      if(gsl_stats_quantile_from_sorted_data(check, 1, (size_t)N_t, 0.99) >
         TEST_99PERC_ERR) pass = 3;

      if(!(isfinite(gsl_stats_median_from_sorted_data(check, 1, (size_t)N_t)) &&
           isfinite(gsl_stats_quantile_from_sorted_data(check, 1, (size_t)N_t, 0.01)) &&
           isfinite(gsl_stats_quantile_from_sorted_data(check, 1, (size_t)N_t, 0.99)))){
        pass = 4;
      };

      if(pass != 0){
        printf("An integration error is too high; pass = %d.\n",pass);
      };
    };

    gsl_odeiv_step_free (s);
    gsl_odeiv_evolve_free (e);
    gsl_odeiv_control_free (c);

  };  /* for(i_integrator=0; i_integrator<N_INTEGRATORS; i_integrator++) */

  /*
    for (i=1;i<N_t-1;i++){
      printf("ode_compare: %.5e ",
             t_array[i]);
      for(i_integrator=0; i_integrator<N_INTEGRATORS; i_integrator++){
        printf(" %.5e", fabs(check[i_integrator][i]));
      }
      printf("\n");
    };
  */

  printf("pass = %d\n",pass);
  return pass;
}
